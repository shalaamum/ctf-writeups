## Summary

This is a writeup for the "SafeBridge" challenge at [Real World CTF](https://realworldctf.com/) 2024. This was a Solidity challenge involving a bridge for ERC20 tokens between two blockchains. In total 17 teams solved this challenge.

Solution by: Fr3d, shalaamum, treesparrow

Writeup by: shalaamum


## Challenge

This is a challenge with a remote, but a setup to test locally was also provided.


### Launcher

Connecting to the remote we get a menu to launch or kill our instance, as well as an option to get the flag after solving the challenge. Starting an instance yields output like this:
```
1 - launch new instance
2 - kill instance
3 - get flag
action? 1
creating private blockchain...
deploying challenge...

your private blockchain has been set up
it will automatically terminate in 1440 minutes
---
rpc endpoints:
    - http://127.0.0.1:8545/dFTCyHHGbLnZoHUDiHvPFoda/l1
    - http://127.0.0.1:8545/dFTCyHHGbLnZoHUDiHvPFoda/l2
private key:        0x8b9954b13aca7b0ae59a59c9bf343b4173cb205c48eee5a1e9f8b23ae6556bcc
challenge contract: 0x220f6d4f2231bc050Cbf51828f9ad2dA110Ea984
```
The two rpc endpoints are for two Ethereum compatible blockchains (which we will refer to as L1 and L2), and we are provided a private key for an account that has 1000 ETH on both L1 and L2.

We find the code that checks whether we solved the challenge and if so hands out the flag in `ctf_launchers/pwn_launcher.py`:
```python
    def get_flag(self) -> int:
        instance_body = requests.get(
            f"{ORCHESTRATOR_HOST}/instances/{self.get_instance_id()}"
        ).json()
        if not instance_body["ok"]:
            print(instance_body["message"])
            return 1

        user_data = instance_body["data"]

        if not self.is_solved(user_data, user_data["metadata"]["challenge_address"]):
            print("are you sure you solved it?")
            return 1

        print(FLAG)
        return 0

    def is_solved(self, user_data: UserData, addr: str) -> bool:
        web3 = get_privileged_web3(user_data, "l1")

        (result,) = abi.decode(
            ["bool"],
            web3.eth.call(
                {
                    "to": addr,
                    "data": web3.keccak(text="isSolved()")[:4],
                }
            ),
        )
        return result
```

So we somehow have to make the `isSolved()` function of the challenge contract return `True`.


### Setup

The most relevant files for the setup are `challenge.py` and `project/script/Deploy.s.sol`.
From them we can see that the `Challenge` contract is deployed on L1, together with a `WETH`, `L1CrossDomainMessenger`, and `L1ERC20Bridge` contract. A deposit of 2 WETH is also made to the L1 bridge contract. On L2, a `L2WETH`, `L2CrossDomainMessenger`, and `L2ERC20Bridge` contract is deployed. On L2 those contracts are deployed to fixed predetermined addresses that can be found in `project/src/libraries/constants/Lib_PredeployAddresses.sol`.


### Challenge contract

The challenge contract (`project/src/Challenge.sol`) is very short:
```solidity
contract Challenge {
    address public immutable BRIDGE;
    address public immutable MESSENGER;
    address public immutable WETH;

    constructor(address bridge, address messenger, address weth) {
        BRIDGE = bridge;
        MESSENGER = messenger;
        WETH = weth;
    }

    function isSolved() external view returns (bool) {
        return IERC20(WETH).balanceOf(BRIDGE) == 0;
    }
}
```
So our task is to reduce the WETH balance of the L1 bridge contract to zero, from the balance of 2 WETH it has originally.


### WETH and L2WETH

The WETH contract on L1 acts like expected, it is an ERC20 and additionally we can wrap and unwrap the native currency. L2 WETH however does not allow to wrap or unwrap, it acts like a standard ERC20, with the addition that the L2 bridge contract is allowed to mint and burn.


### L1 Bridge -> L2 Bridge

The functionality the two bridge contracts offer is to transfer ERC20 tokens across the bridge to a counterpart on the other blockchain. Let us look at the two directions, starting with an L1 to L2 transfer.

Such a transfer is initiated on the L1 side with this function (this function is internal, but the wrapper around this merely sets `_from` to `msg.sender`).
```solidity
    function _initiateERC20Deposit(address _l1Token, address _l2Token, address _from, address _to, uint256 _amount)
        internal
    {
        IERC20(_l1Token).safeTransferFrom(_from, address(this), _amount);

        bytes memory message;
        if (_l1Token == weth) {
            message = abi.encodeWithSelector(
                IL2ERC20Bridge.finalizeDeposit.selector, address(0), Lib_PredeployAddresses.L2_WETH, _from, _to, _amount
            );
        } else {
            message =
                abi.encodeWithSelector(IL2ERC20Bridge.finalizeDeposit.selector, _l1Token, _l2Token, _from, _to, _amount);
        }

        sendCrossDomainMessage(l2TokenBridge, message);
        deposits[_l1Token][_l2Token] = deposits[_l1Token][_l2Token] + _amount;

        emit ERC20DepositInitiated(_l1Token, _l2Token, _from, _to, _amount);
    }
```
There are three things happening here:
1. The amount of L1 token that we wish to transfer to L2 get transferred from us to the L1 bridge.
2. `deposits[_l1Token][_l2Token]` is increased by that amount.
3. A cross-chain function call is made for the `finalizeDeposit` function of the L2 bridge contract. The `_from`, `_to`, and `_amount` arguments are just as expected, but the token address arguments have two cases:
  - If the L1 token is WETH, then as `_l1Token` address `0` is used, and as `_l2Token` address the fixed address of L2 WETH
  - Otherwise, the values we passed are used.

On L2 side, this is the function called:
```solidity
    function finalizeDeposit(address _l1Token, address _l2Token, address _from, address _to, uint256 _amount)
        external
        virtual
        onlyFromCrossDomainAccount(l1TokenBridge)
    {
        // Check the target token is compliant and
        // verify the deposited token on L1 matches the L2 deposited token representation here
        if (ERC165Checker.supportsInterface(_l2Token, 0x1d1d8b63) && _l1Token == IL2StandardERC20(_l2Token).l1Token()) {
            IL2StandardERC20(_l2Token).mint(_to, _amount);
            emit DepositFinalized(_l1Token, _l2Token, _from, _to, _amount);
        } else {
            emit DepositFailed(_l1Token, _l2Token, _from, _to, _amount);
        }
    }
```
So this first checks that the L2 token claims to be a counterpart to the L1 token, by checking the `l1Token()` function. If this check passes, then the token is minted for the receiver.

What is interesting in this flow is that there are no checks on the L1 side that the L2 token is a valid counterpart to it. This means anyone can deploy a contract to L2 that claims to be a counterpart to any L1 token, such as WETH. Furthermore, the function `l1Token()` could even return different values at different times.

What is also interesting is that if `_l1Token` happened to be WETH, then the `_l2Token` we provided on the L1 side is ignored and replaced with regards to the call to the L2 bridge and eventual minting, but is *not* replaced with regards to the bookkeeping in `deposits`. This breaks `deposits[A][B]` validly bookkeeping how much of A has been bridged to B, as initiating a transfer to L2 of WETH to some token B will increase `deposits[WETH][B]`, but in fact we will not obtain B, but L2 WETH on the L2 side.


### L2 bridge -> L1 bridge

Transfers back from L2 to L1 are initiated from the L2 side. This is the relevant function:
```solidity
    function _initiateWithdrawal(address _l2Token, address _from, address _to, uint256 _amount) internal {
        IL2StandardERC20(_l2Token).burn(msg.sender, _amount);

        address l1Token = IL2StandardERC20(_l2Token).l1Token();
        bytes memory message;
        if (_l2Token == Lib_PredeployAddresses.L2_WETH) {
            message = abi.encodeWithSelector(IL1ERC20Bridge.finalizeWethWithdrawal.selector, _from, _to, _amount);
        } else {
            message = abi.encodeWithSelector(
                IL1ERC20Bridge.finalizeERC20Withdrawal.selector, l1Token, _l2Token, _from, _to, _amount
            );
        }

        sendCrossDomainMessage(l1TokenBridge, message);

        emit WithdrawalInitiated(l1Token, _l2Token, msg.sender, _to, _amount);
    }
```
So in this direction we can not specify what the L1 token should be, this is always taken from the L2 token's `l1Token()` function. A cross-chain function call is made to the L1 bridge for the withdrawal, with the function being `finalizeWethWithdrawal` or `finalizeERC20Withdrawal`, depending on whether the L2 token we want to transfer back to L1 is L2 WETH or not.

On the L1 side, here is the relevant code.
```solidity
    function finalizeERC20Withdrawal(address _l1Token, address _l2Token, address _from, address _to, uint256 _amount)
        public
        onlyFromCrossDomainAccount(l2TokenBridge)
    {
        deposits[_l1Token][_l2Token] = deposits[_l1Token][_l2Token] - _amount;
        IERC20(_l1Token).safeTransfer(_to, _amount);
        emit ERC20WithdrawalFinalized(_l1Token, _l2Token, _from, _to, _amount);
    }

    function finalizeWethWithdrawal(address _from, address _to, uint256 _amount)
        external
        onlyFromCrossDomainAccount(l2TokenBridge)
    {
        finalizeERC20Withdrawal(weth, Lib_PredeployAddresses.L2_WETH, _from, _to, _amount);
    }
```
This transfers the right amount of tokens to the receiver and updates `deposits`. Note that the subtraction will revert if `deposits[_l1Token][_l2Token]` is less than `_amount`.
This means to withdraw WETH from the L1 bridge, we must own some of a token B on L2 so that `B.l1Token() == WETH`, and `deposits[WETH][B]` must be large enough for the withdrawal.


## Solution

Originally all `deposit[A][B]` are zero, except for `deposit[WETH][L2 WETH]`, which has a value of `2*10^18` (2 WETH), which is also how much WETH the L1 bridge holds. If we hold B on L2 and want to withdraw A, then `deposit[A][B]` must already be sufficiently large. If we ensure this by using the bridge to deposit A for B, then this won't help us, as we are just withdrawing the amount of A we put in before. So our solution will have to use that `deposit[WETH][L2 WETH]` is already nonzero. So the solution will require us to somehow obtain L2 WETH.

As explained, we can obtain L2 WETH by initiating a deposit from L1 WETH to any L2 token. If the L2 token is L2 WETH, then this will again not help us, because we again will only be able withdraw what we put in, with the original amount held by the bridge left over. So we should deposit 2 WETH on L1 for a different token, say the KALMAR token, on L2. We will still get 2 L2 WETH from this, but now both `deposit[WETH][L2 WETH]` and `deposit[WETH][KALMAR]` will be `2*10^18`. We can then drain `deposit[WETH][L2 WETH]` because we own 2 L2 WETH. But we can also drain `deposit[WETH][KALMAR]`, because we control KALMAR and can just mint ourselves some.


## Attachments

- [KalmarToken.sol](./KalmarToken.sol), the contract for the KALMAR token used on L2.
- [solve.py](./solve.py), the solve script.
