#!/usr/bin/env python3

# solve script by shalaamum (kalmarunionen) for "anti Kerckhoffs" challenge at Codegate 2023 Preliminaries

# Can write the first polynomials as a*(x-x0)^2 + c.
# Afterwards we apply matrix A, but then we can absorb the factor a into the matrix by
# multiplying the respective column with a.
# Similarly, can push the affine part c through (first make the linear part affine linear with A*c, then push
# the affine part into the quadratic polynomials at the end), so we get:
# 1. Apply componentwise (x - x0)^2
# 2. Multiply from left with matrix A
# 3. Apply componentwise quadratic polynomials
# Finally, if A = Diag(d_0, d_1, ...) * A', then can push the d_i into the quadratic polynomials at the end.
# Thus, we can assume that (as we know that A has no zero entries) the first column consists only of 1.

import random
import functools
import itertools
import collections
import time
import pwnlib.tubes.process
import pwnlib.tubes.remote
from tqdm import tqdm
import sage.all

from problib import *

INPUTS_BY_NUM_CORRECT = {i: set() for i in range(m + 1)}
POLY_1_ZEROS = [None]*m
MATRIX = [[None]*m for _ in range(m)]
for i in range(m):
    MATRIX[i][0] = 1
QUERIES_DONE = 0

@functools.cache
def invert_poly_1(index, value):
    for x in range(MOD):
        if ((x - POLY_1_ZEROS[index])**2) % MOD == value:
            return x
    return None

def apply_poly_1(values):
    assert len(values) == m
    return [((x - POLY_1_ZEROS[index])**2) % MOD for index, x in enumerate(values)]

def inputs_outputs_by_most_correct():
    result = []
    for c in range(m, -1, -1):
        result.extend(list(INPUTS_BY_NUM_CORRECT[c]))
    return result

def bin_int_to_list(x):
    assert 0 <= x < 2 ** m, "Invalid int format"
    L = [0] * m
    for i in range(m):
        L[i] = x % 2
        x //= 2
    return L

def get_outputs_from_io(io, input_lists):
    global QUERIES_DONE
    inputs = ' '.join([str(list_to_int(inp)) for inp in input_lists])
    io.recvuntil(b'> ')
    io.sendline(inputs.encode())
    io.recvuntil(b'Outputs = ')
    data = io.recvline().strip().decode().split(' ')
    outputs = [bin_int_to_list(int(x)) for x in data]
    for inp, outp in zip(input_lists, outputs):
        INPUTS_BY_NUM_CORRECT[sum(outp)].add((tuple(inp), tuple(outp)))
    QUERIES_DONE += len(input_lists)
    return outputs

def get_some_inputs(oracle, n):
    inp = [[random.randrange(0, MOD) for __ in range(m)] for _ in range(n)]
    oracle(inp)

def recover_poly_1_zeros(oracle):
    print('Recovering double zeros of the first polynomial step')
    for index in range(m):
        examples = inputs_outputs_by_most_correct()
        for inp, outp in examples:
            example = list(inp)
            example_output = list(outp)
            #print(f'{example=}')
            #print(f'{example_output=}')
            test_inputs = []
            for v in range(MOD):
                test_ = list(example)
                test_[index] = v
                test_inputs.append(test_)
            test_outputs = oracle(test_inputs)
            possible_double_zero = set(range(MOD))
            for v in range(MOD):
                #print(f'{v=} --> {test_outputs[v]} : {test_outputs.count(test_outputs[v])}')
                if test_outputs.count(test_outputs[v]) == 2:
                    possible_double_zero.remove(v)
            possible_double_zero = list(possible_double_zero)
            if len(possible_double_zero) == 1:
                double_zero = possible_double_zero[0]
                print(f'Double zero at index {index} is {double_zero}')
                POLY_1_ZEROS[index] = double_zero
                break

def test_poly_1(oracle, n):
    print('Testing poly 1 recovery...')
    for _ in tqdm(range(n)):
        inputs = [[(POLY_1_ZEROS[index] + sign) % MOD for index in range(m)] for sign in (-1, 1)]
        outputs = oracle(inputs)
        assert outputs[0] == outputs[1]

def recover_matrix_column(oracle, column):
    print(f'Trying to recover matrix column and {column}...', end='')
    examples = inputs_outputs_by_most_correct()
    #linear_equations = [set() for row in range(m)]
    column_possibilities = [set(range(MOD)) for _ in range(m)]
    column_1 = 0
    column_2 = column
    for inp, outp in examples:
        coeffs_to_output = dict()
        example_input = list(inp)
        example_output = list(outp)
        #print(f'Using example {example_input}')
        test_coeffs_1 = []
        test_coeffs_2 = []
        test_inputs = []
        for coeff_1, coeff_2 in itertools.product(range(MOD), repeat=2):
            inp_1 = invert_poly_1(column_1, coeff_1)
            inp_2 = invert_poly_1(column_2, coeff_2)
            if inp_1 is None or inp_2 is None:
                continue
            test_input = list(example_input)
            test_input[column_1] = inp_1
            test_input[column_2] = inp_2
            test_coeffs_1.append(coeff_1)
            test_coeffs_2.append(coeff_2)
            test_inputs.append(test_input)
        test_outputs = oracle(test_inputs)

        for i in range(len(test_inputs)):
            coeffs_to_output[(test_coeffs_1[i], test_coeffs_2[i])] = test_outputs[i]

        for row in range(m):
            for entry in list(column_possibilities[row]):
                linear_comb_to_output = [[] for c in range(MOD)]
                for coeff_1, coeff_2 in coeffs_to_output:
                    linear_comb_to_output[(coeff_1 + coeff_2 * entry) % MOD].append(coeffs_to_output[(coeff_1, coeff_2)][row])
                #print(f'entry {entry} --> {linear_comb_to_output}')
                if any((out != [0]*len(out)) and (out != [1]*len(out)) for out in linear_comb_to_output):
                    #print(f'Ruling out {entry}')
                    column_possibilities[row].remove(entry)
        #print(f'\rColumn possibilities: {column_possibilities}', end=' '*20)
        if all(len(column_possibilities[row]) == 1 for row in range(m)):
            break

    assert all(len(column_possibilities[row]) == 1 for row in range(m)), "Exhausted examples!"
    #print()
    print(f'\rColumn {column:02}:   [ ', end='')
    for row in range(m):
        MATRIX[row][column] = list(column_possibilities[row])[0]
        print(f'{MATRIX[row][column]:02} ', end='')
    print(']')

def recover_matrix(oracle):
    for column in range(1, m):
        recover_matrix_column(oracle, column)

def apply_matrix(matrix, values):
    assert len(values) == m
    return [sum(matrix[row][column] * values[column] for column in range(m)) % MOD for row in range(m)]

def apply_first_two_steps(values):
    return apply_matrix(MATRIX, apply_poly_1(values))

def recover_pre_poly_target():
    print('Recovering pre-poly target')
    pre_poly_target = [set() for _ in range(m)]
    examples = inputs_outputs_by_most_correct()
    for inp, outp in tqdm(examples):
        example_input = list(inp)
        example_output = list(outp)
        example_pre_poly = apply_first_two_steps(example_input)
        for row in range(m):
            if example_output[row] == 1:
                pre_poly_target[row].add(example_pre_poly[row])
    for row in range(m):
        assert 1 <= len(pre_poly_target[row]) <= 2
    return pre_poly_target

def invert_first_two_steps(pre_poly_target):
    F = sage.all.GF(MOD)
    M = sage.all.matrix(F, MATRIX)
    Minv = [[int(entry) for entry in row] for row in M.inverse()]
    for target in tqdm(itertools.product(*pre_poly_target)):
        post_first_poly_target = apply_matrix(Minv, target)
        result = [invert_poly_1(index, value) for index, value in enumerate(post_first_poly_target)]
        if all(x is not None for x in result):
            return result
    raise ValueError('Could not find solution!')


def main():
    io = pwnlib.tubes.process.process(['python3', 'prob.py'])
    #io = pwnlib.tubes.remote.remote('13.124.51.204', 6238)
    TARGET = int_to_list(int(io.recvline().strip().decode().split(' = ')[1]))
    oracle = (lambda _io: (lambda inp: get_outputs_from_io(_io, inp)))(io)
    
    print(f'{TARGET=}')
    get_some_inputs(oracle, 1000)
    print(f'{QUERIES_DONE=}')
    recover_poly_1_zeros(oracle)
    print(f'{QUERIES_DONE=}')
    #test_poly_1(oracle, 1000)
    recover_matrix(oracle)
    print(f'{QUERIES_DONE=}')
    pre_poly_target = recover_pre_poly_target()
    print(f'{QUERIES_DONE=}')
    print(f'Number of candidates: 2^{sum([len(x) for x in pre_poly_target])}')
    secret = invert_first_two_steps(pre_poly_target)
    print(f'{secret=}')
    oracle([secret])
    io.interactive()

if __name__ == '__main__':
    main()

# codegate2023{f0c709007b63fb8ef00663ada0f171ff5d0769907db58ddc59f63ab13ce711f79f9dc0f58cd0af2055d392e159edd7e6d8a5f984f9}
