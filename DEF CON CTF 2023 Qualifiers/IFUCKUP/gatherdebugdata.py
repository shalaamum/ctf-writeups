#!/usr/bin/env python3

# "ifuckup" challenge at DEF CON CTF 2023 Qualifiers
# Script to gather data to verify understanding of the binary
# By shalaamum

import pwnlib.context
import pwnlib.gdb
from pwnlib.elf.elf import ELF


elf = ELF('./ifuckup')

terminalSetting = ["tmux", "new-window"]
pwnlib.context.context.clear(terminal=terminalSetting, binary=elf)

run_args = [elf.path]
gdbscript = '''
set $generate_debug_data = 1
source ./unsetenv.gdb
source ./ifuckupgdb.py
continue
'''
io = pwnlib.gdb.debug(run_args, gdbscript=gdbscript)
io.recvuntil(b'0. Quit\n')
io.sendline(b'5')
io.recvuntil(b'0. Quit\n')
io.sendline(b'0')
#io.interactive()
