#!/usr/bin/env python3

# "ifuckup" challenge at DEF CON CTF 2023 Qualifiers
# Script to debug the "unknown command" bug
# By shalaamum

import sys
import json
import pwnlib.context
import pwnlib.gdb
import pwnlib.tubes.process
import pwnlib.tubes.remote
from pwnlib.elf.elf import ELF

elf = ELF('./ifuckup')

terminalSetting = ["tmux", "new-window"]
pwnlib.context.context.clear(terminal=terminalSetting, binary=elf)

run_args = [elf.path]
gdbscript = '''
set $debug_unknown_command = 1
source ./unsetenv.gdb
source ./ifuckupgdb.py
continue
'''

io = pwnlib.gdb.debug(run_args, gdbscript=gdbscript)

def menu():
    return io.recvuntil(b'0. Quit\n')

def get_random():
    io.sendline(b'5')
    data = menu()
    if b"known" in data:
        print(data)
        io.interactive()
        quit()
    print(data.decode())
    return data


menu()
num_sent = 0
while True:
    new_data = get_random()
    num_sent += 1
    print(f'Number of get_random calls: {num_sent}')
