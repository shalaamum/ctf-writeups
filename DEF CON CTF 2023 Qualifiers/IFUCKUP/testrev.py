#!/usr/bin/env python3

# "ifuckup" challenge at DEF CON CTF 2023 Qualifiers
# Script checking that the debug data gathered from the binary fits with expectations, thus verifying the understanding gained from reversing
# By shalaamum


from prng import PRNG

def testrev():
    import debugdata
    prng0 = PRNG(debugdata.initial_prng0_state[1][debugdata.initial_prng0_state[0]:]
                 + debugdata.initial_prng0_state[1][:debugdata.initial_prng0_state[0]])
    prng1 = PRNG(debugdata.initial_prng1_state[1][debugdata.initial_prng1_state[0]:]
                 + debugdata.initial_prng1_state[1][:debugdata.initial_prng1_state[0]])

    # First we test that the raw outputs themselves are correct
    for prng0_raw in debugdata.prng0_outputs_raw:
        assert prng0_raw == prng0.step_raw()
    for prng1_raw in debugdata.prng1_outputs_raw:
        assert prng1_raw == prng1.step_raw()
    prng0 = PRNG(debugdata.initial_prng0_state[1][debugdata.initial_prng0_state[0]:]
                 + debugdata.initial_prng0_state[1][:debugdata.initial_prng0_state[0]])
    prng1 = PRNG(debugdata.initial_prng1_state[1][debugdata.initial_prng1_state[0]:]
                 + debugdata.initial_prng1_state[1][:debugdata.initial_prng1_state[0]])

    # Next we test whether we understand how the raw outputs lead to the actual values printed
    index_0 = 0
    index_1 = 0
    for count in range(1, 0x41):
        random_value = prng0.step_0()
        assert random_value == debugdata.prng0_outputs[index_0]
        index_0 += 1
        if (count & 7) != 0 and (count & 3) == 0:
            random_value = prng1.step_1()
            assert random_value == debugdata.prng1_outputs[index_1]
            index_1 += 1
            #print(f'Stepping PRNG0 {random_value} times')
            for _ in range(random_value):
                prng0.step_raw()
    print('Success!')

if __name__ == '__main__':
    testrev()
