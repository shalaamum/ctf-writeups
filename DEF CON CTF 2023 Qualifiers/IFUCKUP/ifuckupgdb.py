
# "ifuckup" challenge at DEF CON CTF 2023 Qualifiers
# Script for GDB to handle relocations of the binary, allowing debugging across relocation, as well as handlers to gather debug data and debug the "unknown command" bug
# By shalaamum



import gdb
import struct


binary_base_address = 0x08048000

def is_gdb_var_void(name):
    return gdb.parse_and_eval(name).type.code == gdb.TYPE_CODE_VOID


def to_uint(x):
    return int(x) % (1<<32)


def continue_event():
    gdb.execute('continue')


class BreakpointHandler:
    def __init__(self, offset, should_continue=False):
        global binary_base_address
        self._breakpoint = gdb.Breakpoint(f'*{binary_base_address + offset}')
        gdb.events.stop.connect(self._handle_stop_event)
        self.offset = offset
        self.should_continue = should_continue

    def _handle_stop_event(self, event):
        global binary_base_address
        if not isinstance(event, gdb.BreakpointEvent):
            return

        # Check if the breakpoint is valid. It might become invalid due to being
        # an event from an old, already deleted, breakpoint, and we get the event
        # only now because the RelocationHandler "continue"ed.
        if not event.breakpoints[0].is_valid():
            return

        bp_address = to_uint(event.breakpoints[0].locations[0].address)
        if bp_address - binary_base_address == self.offset:
            self.handle_stop_event(event)
            if self.should_continue:
                gdb.post_event(continue_event)


class RelocationHandler(BreakpointHandler):
    def __init__(self):
        super().__init__(0x120a, should_continue=True)
        self.other_breakpoints = []

    def handle_stop_event(self, event):
        global binary_base_address
        inferior = gdb.selected_inferior()

        old_base = int(gdb.parse_and_eval("*$edi")) % (1<<32)
        new_base = int(gdb.parse_and_eval("$esi")) % (1<<32)
        binary_base_address = new_base
    
        print(f'Relocating from 0x{old_base:08x} to 0x{new_base:08x}')
        
        my_address = binary_base_address + self.offset
        self.other_breakpoints = []

        for bp in gdb.breakpoints():
            bp_address = bp.locations[0].address
            #print(f'Breakpoint at {bp_address:08x}')
            # We delete the breakpoint in the old copy of the binary
            bp.delete()
            
            new_bp_address = bp_address - old_base + new_base
            new_bp_address_str = f'*{new_bp_address}'
            # The old version of the binary had an int3 at bp_address instead of the correct byte from the binary.
            # This got copied over to the new version. When stopping, GDB recovers the previous byte
            # at the old version of the binary, but does not do so for the new, copied version.
            # We thus need to not only delete the breakpoint and add a new one, we also need to
            # fix this byte in the copied version.
            inferior.write_memory(new_bp_address, inferior.read_memory(bp_address, 1))

            # Finally, add new breakpoint.
            if bp_address == my_address:
                self.breakpoint = gdb.Breakpoint(new_bp_address_str)
            else:
                self.other_breakpoints.append(gdb.Breakpoint(new_bp_address_str))


def read_uint(address):
    inferior = gdb.selected_inferior()
    return struct.unpack('<I', inferior.read_memory(address, 4).tobytes())[0]


def get_prng_states():
    inferior = gdb.selected_inferior()
    prngs_global = read_uint(binary_base_address + 0x36c4)
    result = []
    for num in (0,1):
        state_start = prngs_global + 0x44*num + 0x4
        rotation = read_uint(state_start - 4)
        state = []
        for i in range(0x10):
            state.append(read_uint(state_start + 4*i))
        result.append((rotation, tuple(state)))
    return tuple(result)


initial_prng0_state = None
initial_prng1_state = None
prng0_outputs_raw = []
prng0_outputs = []
prng1_outputs_raw = []
prng1_outputs = []


class GetRandomHandlerStart(BreakpointHandler):
    def __init__(self):
        super().__init__(0x1813, should_continue=True)

    def handle_stop_event(self, event):
        print('Handler for get_random start')
        global initial_prng0_state, initial_prng1_state
        global step_prng_handler
        step_prng_handler = PRNGHandlerResult()
        initial_prng0_state, initial_prng1_state = get_prng_states()


class GetRandomHandlerPRNG0(BreakpointHandler):
    def __init__(self):
        super().__init__(0x1881, should_continue=True)

    def handle_stop_event(self, event):
        print('Handler for get_random PRNG0')
        global prng0_outputs
        value = to_uint(gdb.parse_and_eval("*(unsigned int *)($esp)"))
        prng0_outputs.append(value)


class GetRandomHandlerPRNG1(BreakpointHandler):
    def __init__(self):
        super().__init__(0x18f1, should_continue=True)

    def handle_stop_event(self, event):
        print('Handler for get_random PRNG1')
        global prng1_outputs
        value = to_uint(gdb.parse_and_eval("*(unsigned int *)($ebp-0x38)"))
        prng1_outputs.append(value)


class GetRandomHandlerEnd(BreakpointHandler):
    def __init__(self):
        super().__init__(0x1925, should_continue=True)

    def handle_stop_event(self, event):
        print('Writing out collected data...')
        with open('./debugdata.py', 'w') as fh:
            fh.write(f'initial_prng0_state = {initial_prng0_state}\n')
            fh.write(f'initial_prng1_state = {initial_prng1_state}\n')
            fh.write(f'prng0_outputs_raw = {prng0_outputs_raw}\n')
            fh.write(f'prng0_outputs = {prng0_outputs}\n')
            fh.write(f'prng1_outputs_raw = {prng1_outputs_raw}\n')
            fh.write(f'prng1_outputs = {prng1_outputs}\n')


class PRNGHandlerResult(BreakpointHandler):
    def __init__(self):
        super().__init__(0x1ac9, should_continue=True)

    def handle_stop_event(self, event):
        print('Handler for step_prng')
        # result_uint is in EDI
        value = to_uint(gdb.parse_and_eval("(unsigned int)($edi)"))
        prng_address = to_uint(gdb.parse_and_eval("(unsigned int)($eax)"))
        prngs_global = read_uint(binary_base_address + 0x36c4)
        if prng_address == prngs_global:
            prng0_outputs_raw.append(value)
        elif prng_address == prngs_global + 0x44:
            prng1_outputs_raw.append(value)
        else:
            print(f'{prng_address:08x}')
            print(f'{prngs_global:08x}')
            raise Exception("PRNG that was argument to step_prng is not one of the two expected ones!")


class ReadBeforeRelocateHandler(BreakpointHandler):
    def __init__(self):
        super().__init__(0x14ac, should_continue=True)

    def handle_stop_event(self, event):
        print('Handler for read, before relocation')
        value = to_uint(gdb.parse_and_eval("*(*(unsigned int **)($ebp) - 4)"))
        stack_pointer = to_uint(gdb.parse_and_eval("$esp"))
        print(f'Base address: 0x{binary_base_address:08x}, stack: 0x{stack_pointer:08x}, value: 0x{value:08x}')


class ReadAfterRelocateHandler(BreakpointHandler):
    def __init__(self):
        super().__init__(0x14b1, should_continue=True)

    def handle_stop_event(self, event):
        print('Handler for read, after relocation')
        value = to_uint(gdb.parse_and_eval("*(*(unsigned int **)($ebp) - 4)"))
        stack_pointer = to_uint(gdb.parse_and_eval("$esp"))
        print(f'Base address: 0x{binary_base_address:08x}, stack: 0x{stack_pointer:08x}, value: 0x{value:08x}')

 


relocation_handler = RelocationHandler()
if not is_gdb_var_void('$generate_debug_data'):
    print('Setting breakpoints to obtain PRNG debug data')
    get_random_start_handler = GetRandomHandlerStart()
    get_random_prng0_handler = GetRandomHandlerPRNG0()
    get_random_prng1_handler = GetRandomHandlerPRNG1()
    get_random_end_handler = GetRandomHandlerEnd()
    step_prng_handler = None
if not is_gdb_var_void('$debug_unknown_command'):
    print('Setting breakpoints for debugging the unknown command bug')
    read_before_relocate_handler = ReadBeforeRelocateHandler()
    read_after_relocate_handler = ReadAfterRelocateHandler()
