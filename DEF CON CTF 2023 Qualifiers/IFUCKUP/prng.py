
# "ifuckup" challenge at DEF CON CTF 2023 Qualifiers
# Python implementation of the PRNG 
# By shalaamum


import math

class PRNG:
    def __init__(self, state):
        # State is state' from the writeup, stored as 16 integers, each encoding 32 bits
        self.state = list(state)

    def step_raw(self):
        #print(f'State before step: {self.state}')
        tmp1 = (self.state[-1]) % (1<<32)
        tmp2 = ((self.state[0] << 0x10) ^ self.state[0] ^ self.state[0xd]) % (1<<32)
        tmp3 = ((self.state[9] >> 0xb) ^ self.state[9]) % (1<<32)
        tmp4 = (tmp2 ^ (self.state[0xd] << 0xf) ^ tmp3) % (1<<32)
        result_uint = ((tmp2 << 0x12) ^ (tmp1 << 2) ^ tmp1 ^ tmp3 ^ (tmp3 << 0x1c) ^ ((tmp4 & 0x6d22169) << 5)) % (1<<32)
        self.state[0] = tmp4
        self.state[-1] = result_uint
        self.state = self.state[-1:] + self.state[:-1]
        return result_uint

    def step_0(self):
        raw = self.step_raw()
        if raw == 0:
            return 0
        else:
            return raw - 1

    def step_1(self):
        return math.floor(self.step_raw() * (2**(-24) - 2**(-32)))

def fast_forward(prng0, prng1):
    for _ in range(prng1.step_1()):
        prng0.step_raw()


def relocate(prng0, prng1):
    '''Steps prng0 and prng1 as in relocate, and returns binary_base_address, stack_base_address'''
    fast_forward(prng0, prng1)
    binary_base_address = prng0.step_0()
    fast_forward(prng0, prng1)
    stack_base_address = prng0.step_0()
    fast_forward(prng0, prng1)
    prng0.step_raw()
    return binary_base_address, stack_base_address
