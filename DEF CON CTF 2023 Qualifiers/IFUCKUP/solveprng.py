#!/usr/bin/env python3

# "ifuckup" challenge at DEF CON CTF 2023 Qualifiers
# Script to recover PRNG internal state from observed data
# By shalaamum

import sys
import struct
import json
import itertools
from tqdm import tqdm
import sage.all
import pwnlib.tubes.server

from prng import PRNG
import prng

F = sage.all.GF(2)
V = F**512

def tuple_to_uint512(input_tuple):
    """Convert tuple of 16 32-bit integers to 512-bit wide integer."""
    bytes_repr = b"".join(struct.pack('>I', i) for i in input_tuple[::-1])
    return int.from_bytes(bytes_repr, byteorder='big')


def uint512_to_tuple(input_int):
    """Convert 512-bit wide integer to tuple of 16 32-bit integers."""
    bytes_repr = input_int.to_bytes(64, byteorder='big')
    return tuple(struct.unpack('>' + 'I'*16, bytes_repr))[::-1]


def uint32_to_bits(n):
    return [(n >> i) & 1 for i in range(32)]


def bits_to_uint(bits):
    return sum(x*(1<<i) for i, x in enumerate(bits))


def prng0_gap_matrices():
    # Each column contains the contribution of a particular bit of the original state
    # to the output bits.
    print('Calculating PRNG0 bit dependencies on original state...')
    columns = []
    for original_state_bit_num in tqdm(range(512)):
        prng = PRNG(uint512_to_tuple(1 << original_state_bit_num))
        column = []
        for prng0_step_num in range(16 + 256):
            output_raw = prng.step_raw()
            output_raw = uint32_to_bits(output_raw)
            column.extend(output_raw)
        columns.append(column)

    # This is the matrix A such that A * s will give successive bits of output,
    # if the original state was s (as a vector of bits).
    print('Constructing full matrix...')
    full_matrix = sage.all.matrix(F, columns).transpose()

    print('Constructing gap matrices')
    gap_matrices = []
    for gap in tqdm(range(256)):
        # We do not get gap many 32 bit outputs in the middle - gap is the output of the second
        # PRNG, and the first one is advanced that often without printing the value.
        gap_matrix = full_matrix.matrix_from_rows(list(range(32*8)) + list(range(32*(8+gap), 32*(8+gap+8))))
        gap_matrices.append(gap_matrix)

    print('Calculating kernels')
    kernels = []
    for gap_matrix in tqdm(gap_matrices):
        kernels.append(gap_matrix.right_kernel().list())

    return gap_matrices, kernels


def prng1_full_matrix():
    '''Matrix in which each row is one bit of raw output in terms of the original 512 bits of state.
    For each output only the top 8 bits get rows, ordered from least significant to most significant.
    After 8 outputs, there is a gap of 3 outputs that are not represented at all.
    On average, we expect to use 6 bits out of the 8 bits. So we need on average about
    8/6 * 512, so about 683 rows. We thus prepare 2048, which is very unlikly to be insufficient.
    '''
    print('Calculating PRNG1 bit dependencies on original state...')
    columns = []
    for original_state_bit_num in tqdm(range(512)):
        prng = PRNG(uint512_to_tuple(1 << original_state_bit_num))
        column = []
        gap_num = 0
        while len(column) < 2048:
            output_raw = prng.step_raw()
            output_raw = uint32_to_bits(output_raw)[-8:]
            column.extend(output_raw)
            gap_num += 1
            if gap_num % 8 == 0:
                for _ in range(3):
                    prng.step_raw()
        columns.append(column)

    # This is the matrix A such that A * s will give successive bits of output,
    # if the original state was s (as a vector of bits).
    print('Constructing full matrix...')
    full_matrix = sage.all.matrix(F, columns).transpose()
    return full_matrix


def extract_last_state_and_gaps(state, data):
    prng = PRNG(state)
    gaps = []
    for get_random_block in data:
        gaps.append([])
        for j, between_gaps_block in enumerate(get_random_block):
            for i, value in enumerate(between_gaps_block):
                found = False
                for gap_length in range(256 + (3*256 if j == 0 else 0)):
                    if value == prng.step_raw():
                        found = True
                        break
                if not found:
                    return None
                if (gap_length != 0) and (i != 0):
                    return None
                if j != 0 and i == 0:
                    gaps[-1].append(gap_length)
    return (prng.state, gaps)


def solve_prng0(data):
    print('Trying to recover PRNG0 state...')
    result = None
    for gap in tqdm(range(256)):
        matrix = GAP_MATRICES[gap]
        bits_known = sum([uint32_to_bits(x) for x in data[0][0] + data[0][1]], [])
        v = sage.all.vector(F, bits_known)
        try:
            original_state_vector_ = matrix.solve_right(v)
        except ValueError as e:
            if 'matrix equation has no solutions' in str(e):
                continue
            raise
        for kernel_vector in GAP_MATRIX_KERNELS[gap]:
            original_state_vector = original_state_vector_ + kernel_vector
            original_state_bits = [int(b) for b in original_state_vector]
            original_state = [int(bits_to_uint(original_state_bits[32*i:32*(i+1)])) for i in range(0x10)]
            ret = extract_last_state_and_gaps(original_state, data)
            if ret is not None:
                return ret

    return result


def make_data_raw(output_data):
    data = []
    # We increase the values by one to undo the floating point stuff
    for get_random_block in output_data:
        data.append([])
        for between_gap_block in get_random_block:
            data[-1].append([])
            for value in between_gap_block:
                if value == 0:
                    raise ValueError("Can't recover raw value from 0!")
                data[-1][-1].append(value + 1)
    return data


def solve_prng1(gaps):
    # First we collect the bits information we have
    row_indices = []
    target = []
    row_index = 0
    for gap_block in gaps:
        for gap in gap_block:
            for bit_index in range(8):
                if ((gap >> bit_index) & 1) == (((gap + 1) >> bit_index) & 1):
                    row_indices.append(row_index)
                    target.append(F((gap >> bit_index) & 1))
                row_index += 1
    
    # We need at least 512 to recover the state. But the 512x512 matrix we
    # get might not have full rank. So let us go to 520.
    if len(target) < 520:
        return None

    print()
    print('Solving for original PRNG1 state vector...')
    target = sage.all.vector(target)
    matrix = PRNG1_MATRIX.matrix_from_rows(row_indices)
    original_state_bits = [int(b) for b in matrix.solve_right(target)]
    original_state = [int(bits_to_uint(original_state_bits[32*i:32*(i+1)])) for i in range(0x10)]
    print('Recovered original PRNG1 state')
    state = prng1_advance_state_checking_gaps(original_state, gaps)
    return state


def prng1_advance_state_checking_gaps(original_state, gaps, skip_start=False):
    print('Checking recovered PRNG1 original state correctly predicts seen gaps')
    prng1 = PRNG(original_state)
    for gap_block_index, gap_block in enumerate(gaps):
        if gap_block_index != 0 or skip_start:
            for _ in range(3):
                prng1.step_raw()
        for gap_index, gap in enumerate(gap_block):
            value = prng1.step_1()
            if value != gap:
                raise ValueError(f'Mispredicted gap: {value} != {gap} at index {gap_index} in gap block {gap_block_index}')
    print('Correct prediction of gaps by recovered PRNG1 original state')
    return prng1.state


def get_new_gaps(io, prng0_state_now):
    output_data_new = [json.loads(io.recvline().strip().decode())]
    data_new = make_data_raw(output_data_new)
    prng0_state_now, gaps_new = extract_last_state_and_gaps(prng0_state_now, data_new)
    return prng0_state_now, gaps_new


def solver(io):
    print('\nGot connection.')
    io.sendline(b'NEXT')
    output_data_new = [json.loads(io.recvline().strip().decode())]
    data_new = make_data_raw(output_data_new)
    # Because we want to start with a 8 int block, not a 4 int block:
    data_new[0] = data_new[0][1:]
    prng0_state_now, _ = solve_prng0(data_new)
    print(f'Recovered PRNG0 state')

    gaps = []
    num_received = 1
    while True:
        io.sendline(b'NEXT')
        prng0_state_now, gaps_new = get_new_gaps(io, prng0_state_now)
        gaps += gaps_new
        num_received += 1
        print(f'\rNumber of get_random blocks received: {num_received:02}', end="")
        prng1_state_now = solve_prng1(gaps)
        if prng1_state_now is not None:
            break

    io.sendline(b'FINISH')
    output_data_new = [json.loads(io.recvline().strip().decode())]
    prng0 = PRNG(prng0_state_now)
    prng1 = PRNG(prng1_state_now)
    prng.relocate(prng0, prng1)
    for block_num, block in enumerate(output_data_new[0]):
        if block_num != 0:
            prng.fast_forward(prng0, prng1)
        for value in block:
            if prng0.step_0() != value:
                raise Exception('Prediction incorrect!')
    print('Fully cought up, know the state of the PRNGs in the running binary')

    for _ in range(3):
        prng.relocate(prng0, prng1)
    for _ in range(99):
        prng0.step_raw()
    prng.relocate(prng0, prng1)
    binary_base_address, stack_base_address = prng.relocate(prng0, prng1)
    binary_base_address &= 0xfffff000
    stack_base_address &= 0xfffff000

    print(f'Predicting binary at 0x{binary_base_address:08x}   stack at 0x{stack_base_address:08x}')
    io.sendline(json.dumps((binary_base_address, stack_base_address)).encode())
    io.close()
    print('Closed connection.\n')


def main():
    if len(sys.argv) != 2:
        print(f'{sys.argv[0]} PORT')
        sys.exit(1)

    print('===== PRECOMPUTATIONS =====')
    global GAP_MATRICES, GAP_MATRIX_KERNELS, PRNG1_MATRIX
    GAP_MATRICES, GAP_MATRIX_KERNELS = prng0_gap_matrices()
    PRNG1_MATRIX = prng1_full_matrix()
    print()

    print('===== SOLVER =====')
    server = pwnlib.tubes.server.server(int(sys.argv[1]))
    while True:
        io = server.next_connection()
        try:
            solver(io)
        except Exception as e:
            print(e)
    
 
if __name__ == '__main__':
    main()
