#!/usr/bin/env python3

# Solution by shalaamum (kalmarunionen) for the challenge "Collisions" by mr96 at m0leCon Teaser 2023

import os
import pwnlib.tubes.process
import pwnlib.tubes.remote
from Cryptodome.Util.number import bytes_to_long, long_to_bytes
import server

# Observations:
#
# preprocess is a bijection on 16 byte blocks
#
# ks:
# the rk1 and kr2 seem to be of the form XYY.
# but ok the xoring with rc is a bijection.
# the first rk1 and rk2 is just k1 and k2, respectively.
# so ks is injective

# ks is just rearranging bits, so if we flip all, then all roundkeys will
# have all bits flipped.
# if we flip all bits in l and r in the feistel part, then at the start of f
# the flipping of all bits of m and k will cancel out, so the outputs of f
# stay the same, but because of the adding of the left part afterwards
# the state in the feistel cipher part stays bitflipped.
# as in the hash we xor with one of the blocks afterwards again, this cancels
# the bitflip.

# so only need to make the first two blocks so that after preprocessing
# they are the bitflipped versions of the original ones after preprocessing


def invert_preprocess(b, cnt):
    state = [int(x) for x in bin(bytes_to_long(b))[2:].rjust(cnt, '0')]
    
    for i in range(cnt):
        first = state[-1] ^ state[46] ^ (1 - (state[69] & state[84])) ^ state[90]
        state = [first] + state[:-1]
    
    b = long_to_bytes(int(''.join(str(x) for x in state), 2)).rjust(cnt//8, b"\x00")
    return b

for i in range(100):
    m = os.urandom(16)
    assert m == invert_preprocess(server.preprocess(m, 8*16), 8*16)
    assert m == server.preprocess(invert_preprocess(m, 8*16), 8*16)

def construct_collision(m):
    m0 = m[0:16]
    m1 = m[16:32]
    mr = m[32:]
    mc0 = invert_preprocess(server.xor(server.preprocess(m0, 8*16), b'\xff'*16), 8*16)
    mc1 = invert_preprocess(server.xor(server.preprocess(m1, 8*16), b'\xff'*16), 8*16)
    mc = mc0 + mc1 + mr
    assert server.hash(m) == server.hash(mc)
    return mc

#r = pwnlib.tubes.process.process('./server.py')
r = pwnlib.tubes.remote.remote('collisions.challs.m0lecon.it', 5632)
for _ in range(10):
    data = r.recvline().strip().decode().split("'")
    m = bytes.fromhex(data[1])
    h = bytes.fromhex(data[3])
    r.sendline(construct_collision(m).hex().encode())

print(r.recvline().decode().strip())
# ptm{Fl1pp1n6_b175_1n_2023}
