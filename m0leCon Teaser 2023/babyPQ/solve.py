#!/usr/bin/env python3

# Solution by shalaamum (kalmarunionen) for babyPQ challenge by mr96 at m0leCon Teaser 2023

import random
import math
import itertools
from tqdm import tqdm
import os
from Cryptodome.Cipher import ChaCha20
from Cryptodome.Util.Padding import pad, unpad
from Cryptodome.Util.number import long_to_bytes, bytes_to_long
import sage.all
import chall

def recover_private_key(sboxes):
    F = sage.all.GF(2)
    Bs = list()
    ts = list()
    for sbox in sboxes:
        rows = []
        t = []
        for i in range(256):
            rows.append([int(bool(sbox[i] & (1 << j))) for j in range(47)])
            t.append(int(bool(sbox[i] & (1 << 47))))
        Bs.append(sage.all.matrix(rows))
        ts.append(sage.all.vector(t))

    all_good = False
    while not all_good:
        rows_used = random.sample(range(256), 47)
        #print(f'guessing using: {rows_used}')
        #print(f'of those wrong: {set(rows_used) & set(wrong_i)}')
        M = sage.all.matrix([Bs[0][i] for i in rows_used]).change_ring(F)
        if M.rank() != 47:
            #print('Not full rank.')
            continue
        rhs = sage.all.vector([ts[0][i] for i in rows_used])
        private_key_candidate = M.solve_right(rhs)
        #print(private_key_candidate)
        #num_correct = sum(private_key[i] == private_key_candidate[i] for i in range(47))
        #print(f'Correct: {num_correct} / 47')
        all_good = True
        print('Correct: ', end='')
        for sbox_index in range(len(Bs)):
            t = ts[sbox_index]
            B = Bs[sbox_index]
            t_from_candidate = sage.all.vector(int(x) % 2 for x in B*private_key_candidate)
            num_correct = sum(t_from_candidate[i] == t[i] for i in range(256))
            print(f'{sbox_index}: {num_correct / 2.56:.2f}%', end='   ')
            if num_correct < 0.9 * 256:
                all_good = False
                break
        print()
    return [int(x) for x in private_key_candidate] + [1]

def relation_holds(num, private_key):
    F = sage.all.GF(2)
    v = sage.all.vector(F(bool(num & (1 << i))) for i in range(48))
    return not bool(v * sage.all.vector(private_key))

def probabilities_last_round_key(pt, ct, private_key, sboxes):
    sbox_relation_wrong_at = [[] for _ in range(6)]
    W = []
    for sbox_index in range(6):
        for v in range(256):
            if not relation_holds(sboxes[sbox_index][v], private_key):
                sbox_relation_wrong_at[sbox_index].append(v)
        W.append(len(sbox_relation_wrong_at[sbox_index]))
        print(f'sbox {sbox_index} does not satisfy relation at {sbox_relation_wrong_at[sbox_index]}')

    P_prev_rounds_holds = 0.0
    P_prev_rounds_holds_not = 0.0
    for num_wrong in itertools.product(range(4),repeat=6):
        p = math.prod(
                math.comb(3, num_wrong[i])
                * ((W[i]/256)**num_wrong[i])
                * (((256 - W[i]) / 256)**(3- num_wrong[i]))
                for i in range(6)
                )
        if sum(num_wrong) % 2 == 0:
            P_prev_rounds_holds += p
        else:
            P_prev_rounds_holds_not += p
    #print(f'P(prev rounds holds) = {P_prev_rounds_holds}') 
    #print(f'P(prev rounds holds not) = {P_prev_rounds_holds_not}') 

    last_round_key_ps = [[1.0/256.0]*256 for _ in range(6)]
    for plain, enc in tqdm(list(zip(pt, ct))):
        plain_l, plain_r = bytes_to_long(plain[:6]), bytes_to_long(plain[6:])
        enc_l, enc_r = bytes_to_long(enc[:6]), bytes_to_long(enc[6:])
        #print(f'plain_l: {relation_holds(plain_l, private_key)}  plain_r: {relation_holds(plain_r, private_key)}')
        #print(f'enc_l: {relation_holds(enc_l, private_key)}  enc_r: {relation_holds(enc_r, private_key)}')
        l_bytes = [(enc_l>>(8*i)) & 0xFF for i in range(6)]

        prior_input_wrong = [0.0]*6
        for i in range(6):
            for v in sbox_relation_wrong_at[i]:
                prior_input_wrong[i] += last_round_key_ps[i][v ^ l_bytes[i]]
        
        P_last_holds_abs = 0.0
        P_last_holds_not_abs = 0.0
        for num_wrong in itertools.product(range(2),repeat=6):
            p = math.prod(
                    ((prior_input_wrong[i])**num_wrong[i])
                    * ((1 - prior_input_wrong[i])**(1 - num_wrong[i]))
                    for i in range(6)
                    )
            if sum(num_wrong) % 2 == 0:
                P_last_holds_abs += p
            else:
                P_last_holds_not_abs += p
        #print(f'P(last round holds abs) = {P_last_holds_abs}') 
        #print(f'P(last round holds not abs) = {P_last_holds_not_abs}') 

        rel_holds = relation_holds(plain_r, private_key) == relation_holds(enc_r, private_key)

        P_total_holds_abs = P_last_holds_abs * P_prev_rounds_holds + P_last_holds_not_abs * P_prev_rounds_holds_not
        P_total_holds_not_abs = P_last_holds_not_abs * P_prev_rounds_holds + P_last_holds_abs * P_prev_rounds_holds_not

        for byte_index in range(6):
            for k in range(256):
                P_last_cond_on_key = 0.0
                P_last_not_cond_on_key = 0.0
                for num_wrong in itertools.product(range(2),repeat=6):
                    if (num_wrong[byte_index] == 1) and ((l_bytes[byte_index] ^ k) not in sbox_relation_wrong_at[byte_index]):
                        continue
                    if (num_wrong[byte_index] == 0) and ((l_bytes[byte_index] ^ k) in sbox_relation_wrong_at[byte_index]):
                        continue
                    p = 1.0
                    for i in range(6):
                        if i == byte_index:
                            continue
                        p *= ((prior_input_wrong[i])**num_wrong[i]) * ((1 - prior_input_wrong[i])**(1 - num_wrong[i]))

                    if sum(num_wrong) % 2 == 0:
                        P_last_cond_on_key += p
                    else:
                        P_last_not_cond_on_key += p

                P_total_holds_cond_on_key = (
                        P_last_cond_on_key * P_prev_rounds_holds
                        + P_last_not_cond_on_key * P_prev_rounds_holds_not
                        )
                P_total_holds_not_cond_on_key = (
                        P_last_not_cond_on_key * P_prev_rounds_holds
                        + P_last_cond_on_key * P_prev_rounds_holds_not
                        )
                assert(-0.1 < P_total_holds_cond_on_key < 1.1)
                assert(abs(1 - P_total_holds_cond_on_key - P_total_holds_not_cond_on_key) < 0.01)
                
                if rel_holds:
                    last_round_key_ps[byte_index][k] *= P_total_holds_cond_on_key / P_total_holds_abs
                else:
                    last_round_key_ps[byte_index][k] *= P_total_holds_not_cond_on_key / P_total_holds_not_abs
    return last_round_key_ps


def round_key_candidates(pt, ct, private_key, sboxes, confidence=0.95):
    last_round_key_ps = probabilities_last_round_key(pt, ct, private_key, sboxes)
    key_candidates = list()
    for byte_num in range(6):
        cand = []
        keys_with_p = list(enumerate(last_round_key_ps[byte_num]))
        keys_with_p = sorted(keys_with_p, key=lambda x: -x[1])
        #print(keys_with_p)
        s = 0.0
        i = 0
        while s < confidence:
            c, p = keys_with_p[i]
            i += 1
            cand.append(c)
            s += p
        print(f'{len(cand)} candidates for byte {byte_num}: {cand}')
        print(keys_with_p[:i])
        key_candidates.append(cand)
        #print(f'Stats for last round key byte {i}: {sorted(last_round_key_ps[i])[::-1][:30]}')
        #key_candidates.append(sorted(range(256), key=lambda x: -last_round_key_stats[i][x]))
    print(f'Total number of candidate tuples: {math.prod(len(key_candidates[i]) for i in range(6))}')
    return key_candidates

def recover_last_key(pt, ct, private_key, sboxes):
    key_candidates = round_key_candidates(pt, ct, private_key, sboxes)

    for depth, key_indices in tqdm(
            itertools.product(
                range(6*256),
                itertools.product(*[range(len(key_candidates[i])) for i in range(5)])
                )
            ):
        last_index = depth - sum(key_indices)
        if last_index < 0:
            continue
        key_indices += (last_index,)
        key_bytes = [key_candidates[i][key_indices[i]] for i in range(6)]
        key_guess = sum(key_bytes[i]*(256**i) for i in range(6))
        key_rec = key_guess.to_bytes(8, "big")
        for i in range(6, -1, -1):
            cipher = ChaCha20.new(key = bytes([i])*32, nonce = b"\x00"*8)
            key_rec = cipher.decrypt(key_rec)

        found = True
        for plain, enc in zip(pt, ct):
            if chall.feistel_encrypt(plain, key_rec, sboxes) != enc:
                found = False
                break
        if found:
            print('\nFound the key!')
            print(f'Indices: {[key_candidates[i].index(key_bytes[i]) for i in range(6)]}')
            return key_rec

    if not found:
        print('Could not find key!')
        quit()

def feistel_decrypt(ct, key, public_key):
    ks = chall.expand_key(key, 8)
    l, r = bytes_to_long(ct[:6]), bytes_to_long(ct[6:])
    
    for i in range(7, -1, -1):
        l, r = r ^ chall.f(l ^ ks[i], public_key), l
    
    return long_to_bytes(l) + long_to_bytes(r)

def decrypt(ct, key, public_key):
    ct = [ct[i:i+12] for i in range(0, len(ct), 12)]
    pt = b''.join([feistel_decrypt(c, key, public_key) for c in ct])
    return unpad(pt, 12)



from output import *

private_key = recover_private_key(public_key)
print(f'{private_key}')
enc, ptct = enc
enc = bytes.fromhex(enc)
pt = []
ct = []
for p, c in ptct:
    pt.append(bytes.fromhex(p))
    ct.append(bytes.fromhex(c))
key = recover_last_key(pt, ct, private_key, public_key)
#key = b'\xf8\xf6\xa3\xe9\xf5\xd4\xe1\xfa'
print(f'{key=}')
flag = decrypt(enc, key, public_key)
print(flag)
# ptm{17_533m5_7h47_w3_4r3_n07_r34dy_f0r_qu4n7um_c0mpu73r5_y37}
