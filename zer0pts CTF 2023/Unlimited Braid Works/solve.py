#!/usr/bin/env python3

# Solve script by shalaamum (Kalmarunionen) for the "Unlimited Braid Works" challenge by mitsu at zer0pts CTF 2023.

import pwnlib.tubes.remote
import pwnlib.tubes.process
import hashlib
import sage.all

flag = ''
while 'pts{' not in flag:
    Bn = sage.all.BraidGroup(16)
    gs = Bn.gens()
    
    def str_to_bn(s, basis_str):
        #print(s)
        for i in range(16, -1, -1):
            s = s.replace(f's{i}', f'{basis_str}[{i}]')
        #print(s)
        s = s.replace('^', '**')
        return eval(s)
    
    #io = pwnlib.tubes.process.process(['sage', './server.sage'])
    io = pwnlib.tubes.remote.remote('crypto.2023.zer0pts.com', 10555)
    io.recvuntil(b'parameter> ')
    io.sendline(b'16')
    io.recvuntil(b'u: ')
    data = io.recvuntil(b'v: ')
    data = data[:-4]
    u = str_to_bn(data.decode(), 'gs')
    
    data = io.recvuntil(b'w: ')
    data = data[:-4]
    v = str_to_bn(data.decode(), 'gs')
    
    data = io.recvuntil(b'd: ')
    data = data[:-4]
    w = str_to_bn(data.decode(), 'gs')
    
    d = io.recvline().strip().decode()
    io.close()
    
    Bsmall = sage.all.BraidGroup(9)
    gsmall = list(Bsmall.gens()) + [Bsmall.one()]*20
    
    vsmall = str_to_bn(str(v), 'gsmall')
    usmall = str_to_bn(str(u), 'gsmall')
    
    
    #print(f'{v=}')
    #print(f'{vsmall=}')
    #print(f'{u=}')
    #print(f'{usmall=}')
    
    asmall = vsmall.conjugating_braid(usmall)
    #print(f'{asmall=}')
    #print(vsmall == asmall * usmall * asmall**-1)
    
    a = str_to_bn(str(asmall), 'gs')
    #print(c == a * w * a^-1)
    
    c = a * w * (a**(-1))
    
    def hash(b):
    	return hashlib.sha512(str(b).encode("utf-8")).digest()
    
    h = hash(sage.all.prod(c.right_normal_form()))
    
    enc = int(d).to_bytes(int(d).bit_length() // 8 + 1, "big")
    #print(enc)
    #print(len(enc))
    enc = enc.decode('utf-8')
    enc = list(enc)
    if len(enc) == 65:
      enc = enc[1:]
    #print(list(enc))
    #print(len(enc))
    
    flag = []
    for i in range(len(h)):
    	flag.append(chr(ord(enc[i]) ^ h[i]))
    flag = ''.join(flag)
    print(flag)


# zer0pts{I_4m_th3_b0n3_0f_Art1n_g3n3r4t0rs}
