#!/usr/bin/env python3

# Solution by shalaamum (kalmarunionen) for "snap circuits" challenge by defund at ångstromCTF 2023

import pwnlib.tubes.process
from tqdm import tqdm
import snapcircuits as sc

def extract_bits(masked_input_bits, table_and, table_xor):
    # Let us denote masking bits by x with appropriate index.
    # So if input bit is b0, then the masked version is b0 + x_b0.
    xored_table = [x ^ y for x, y in zip(table_and, table_xor)]
    # Due to the same key being used for generating the keystream,
    # the keystream cancels in the xor. So the xored table now has:
    # b0 + x_b0, b1 + x_b1 --> x_and + x_xor + b0 + b1 + b0*b1
    # Here the output has the same value for three inputs (b0,b1),
    # and a unique output produced by (0,0).
    if xored_table.count(0) == 1:
        index = xored_table.index(0)
    elif xored_table.count(1) == 1:
        index = xored_table.index(1)
    else:
        raise Exception()

    # index is now the index that corresponds to (b0,b1) = (0,0)
    i = index // 2
    j = index % 2
    # So now we have (i,j) = (b0 + x_b0, b1 + x_b1) = (x_b0, x_b1)
    masks = [i, j]
    unmasked_input_bits = [x ^ y for x, y in zip(masked_input_bits, masks)]
    return(unmasked_input_bits)


#remote = pwnlib.tubes.process.process('./snapcircuits.py')
remote = pwnlib.tubes.remote.remote("challs.actf.co", 32511)

has_pow = True
if has_pow:
    print(remote.recvline().strip().decode())
    remote.recvuntil(b'solution: ')
    remote.sendline(input().encode())

num_bits = int(remote.recvline().strip().decode().split('You have ')[1].split(' inputs to work')[0])
print(f'Number of bits: {num_bits}')
print(f'Sending gates...')
for index in tqdm(range(0, num_bits, 2)):
    remote.recvuntil(b'gate: ')
    inputs = str(index) + ' ' + str(index + 1)
    to_send = 'and ' + inputs
    remote.sendline(to_send.encode())
    remote.recvuntil(b'gate: ')
    to_send = 'xor ' + inputs
    remote.sendline(to_send.encode())
remote.sendline()
remote.recvline()

print(f'Receiving masked inputs...')
masked_inputs = []
for i in tqdm(range(num_bits)):
    remote.recvuntil(f'wire {i}: '.encode())
    remote.recvuntil(b' ')
    bit = int(remote.recvline().strip().decode())
    masked_inputs.append(bit)

print(f'Receiving tables...')
remote.recvuntil(b'table data:\n')
tables_and = []
tables_xor = []
for index in tqdm(range(0, num_bits, 2)):
    table = []
    for _ in range(4):
        remote.recvuntil(b' ')
        table.append(int(remote.recvline().strip().decode()))
    tables_and.append(table)
    table = []
    for _ in range(4):
        remote.recvuntil(b' ')
        table.append(int(remote.recvline().strip().decode()))
    tables_xor.append(table)

#print(f'masked inputs: {masked_inputs}')
#print(f'tables for and: {tables_and}')
#print(f'tables for xor: {tables_xor}')

bits = []
for index in range(0, num_bits, 2):
    bits += extract_bits(masked_inputs[index:index+2], tables_and[index // 2], tables_xor[index // 2])
flag = ""
for index in range(0, num_bits, 8):
    byte = sum([b*2**(7-i) for i, b in enumerate(bits[index:index+8])])
    flag += chr(byte)
print(flag)
# actf{L3akY_g@rbl1ng}
