#!/usr/bin/env python3

HOST = "round-n-round.ctf"
#HOST = "localhost"
PORT = 1337

import pwnlib.tubes.remote
from pwnlib.util.packing import p64, u64
import time

mul_const = 0x1337
C = 0x1b
rounds = 1000000

def add(a, b):
    return (a ^ b) % 2**64

def mul(a, b):
    #print(f'Calc {a} * {b}')
    smaller, bigger = a, b
    if smaller > bigger:
        smaller, bigger = bigger, smaller
    result = 0
    while smaller > 0:
        if smaller & 1 != 0:
            result ^= bigger
        carry = 0
        if bigger & 2**63:
            carry = C
        bigger = (bigger << 1) % (2**64)
        bigger ^= carry
        smaller = smaller >> 1
    return result

def power(a, n):
    result = 1
    #print(f'Power')
    while n > 0:
        #print(n)
        if n & 1:
            result = mul(result, a)
        n = n >> 1
        a = mul(a,a)
    #print(f'Power end')
    return result

def enc_number(number):
    if number >= 2**64:
        raise Exception
    s = pwnlib.tubes.remote.remote(HOST, PORT)
    to_send = p64(number, endian="little")
    print(f'Sending {to_send}')
    s.send(to_send)
    s.shutdown("write")
    data = s.recv()
    result = u64(data, endian="little")
    print(f'Received {data} --> {result}')
    return result


def obtain_key_part(a):
    enc_a = enc_number(a)
    a_part = mul(a, power(mul_const, rounds))
    key_part = add(a_part, enc_a)
    return key_part


# E(a) = a * mul_const^rounds + key_part
key_part = obtain_key_part(123)
print(f'Obtained key_part, it is {key_part}')


xbar = 2
mul_const_inv = xbar**58 + xbar**55 + xbar**53 + xbar**52 + xbar**48 + xbar**44 + xbar**40 + xbar**38 + xbar**37 + xbar**36 + xbar**35 + xbar**34 + xbar**31 + xbar**29 + xbar**28 + xbar**27 + xbar**26 + xbar**25 + xbar**23 + xbar**19 + xbar**16 + xbar**15 + xbar**13 + xbar**11 + xbar**10 + xbar**9 + xbar**4 + xbar**2 + 1
mul_const_inv_power_rounds = power(mul_const_inv, rounds)

print(f'mul_const_inv = {mul_const_inv}')
print(f'mul_const * mul_const_inv = {mul(mul_const, mul_const_inv)}')

with open("flag.enc", "rb") as fh:
    ciphertext = fh.read()

print(f'Full ciphertext is {ciphertext}')
flag = b''
for i in range(0, len(ciphertext), 8):
    ciphertext_block = ciphertext[i:i+8]
    ciphertext_block_number = u64(ciphertext_block)
    print(f'Decrypting {ciphertext_block} = {ciphertext_block_number}')
    no_key_part = add(ciphertext_block_number, key_part)
    cleartext_number = mul(no_key_part, mul_const_inv_power_rounds)
    cleartext = p64(cleartext_number)
    print(f'Got {cleartext_number} -> {cleartext}')
    flag += cleartext

print(f'\n\n\n{flag}')

#flag{90% of crypto: division is hard}


## how to get the inverse of mul_const:
#sage: F = FiniteField(2)
#sage: P = F[x]
#sage: C = 1 + x + x**3 + x**4
#sage: Q = P.quotient(x**64 - C)
#sage: xbar = Q.gen()
#
#>>> n = 0x1337
#>>> i = 0
#>>> while n > 0:
#...     if n & 1:
#...             print(f' + xbar^({i})', end="")
#...     n = n >> 1
#...     i += 1
#... 
# + xbar^(0) + xbar^(1) + xbar^(2) + xbar^(4) + xbar^(5) + xbar^(8) + xbar^(9) + xbar^(12)>>> 
#
#sage: m = xbar^(0) + xbar^(1) + xbar^(2) + xbar^(4) + xbar^(5) + xbar^(8) + xbar
#....: ^(9) + xbar^(12)
#
#sage: m^(-1)
#xbar^58 + xbar^55 + xbar^53 + xbar^52 + xbar^48 + xbar^44 + xbar^40 + xbar^38 + xbar^37 + xbar^36 + xbar^35 + xbar^34 + xbar^31 + xbar^29 + xbar^28 + xbar^27 + xbar^26 + xbar^25 + xbar^23 + xbar^19 + xbar^16 + xbar^15 + xbar^13 + xbar^11 + xbar^10 + xbar^9 + xbar^4 + xbar^2 + 1
#
