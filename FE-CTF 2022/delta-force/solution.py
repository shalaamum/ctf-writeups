#!/usr/bin/env python3

with open('news.txt.enc', 'br') as f:
    data = f.read()

# assume the cleartext consists only of spaces and count how often
# a certain value fits with that
Sinv_statistics = [[0 for _ in range(256)] for _ in range(256)]
previous = 0
for cipher_char in data:
    Sinv_statistics[cipher_char][(ord(' ') + previous) % 256] += 1
    previous = cipher_char

# Determine Sinv
Sinv = [None for _ in range(256)]
improved = True
while improved:
    print(Sinv)
    improved = False
    for cipher_char in range(256):
        if Sinv[cipher_char] is not None:
            continue
        max_val = max(Sinv_statistics[cipher_char])
        num_bigger = sum([1 if i > max_val//2 else 0 for i in Sinv_statistics[cipher_char]])
        if num_bigger == 1:
            # ok, we are pretty sure this is the correct one!
            improved = True
            Sinv[cipher_char] = Sinv_statistics[cipher_char].index(max_val)
            # Sinv[cipher_char] is now taken as a value, so remove it from the rest
            for value in range(256):
                if value == cipher_char:
                    continue
                Sinv_statistics[value][Sinv[cipher_char]] = 0

print(f'\nSinv is:')
print(Sinv)

c1 = 0
i = 0
fout = open('news.txt', 'w')
for c2 in data:
    x = Sinv[c2]
    p = (x - c1) % 256
    c1 = c2
    i += 1
    fout.write(chr(p))
