# Summary

We are given a script `crypto.py` and a file `news.txt.enc`, that was
apparently encrypted with the former script.
Encryption is roughly done by something similar to CBC,
but with block length one byte and encryption of the "block" being done
by substitution, which easily allows a statistical attack to recover
the S-box, after which we can decrypt the file.

# The S-Box

If `crypto.py` is used for encyption, a variable `code` is defined first,
as `code = init(key)`, and looking at `init` we can see that `init`
returns a list with `256` components, with each element of
`{0, 1, ..., 255}` occurring exactly once. `code` can thus be interpreted
as a permutation of `{0, 1, ..., 255}`. How this permutation is derived
from the key does not matter for this solution though.

# Encryption

The encryption function is as follows:
```python
def encipher(code):
    c = '\0'
    while True:
        p = sys.stdin.read(1)
        if not p:
            break
        # Almost like CBC, so must be good
        x = (ord(c) + ord(p)) & 0xff
        c = chr(code[x])
        sys.stdout.write(c)
```
Thus, if we denote by `clear_n` the `n`-th byte of the cleartext
and by `cipher_n` the `n`-th byte of the ciphertext, and we write
`S` for the permutation called `code` in the code and that
was discussed above, then we can interpret this as the equation
```
cipher_n = S[clear_n + cipher_{n-1}]
```
where `cipher_{-1} = 0`, and addition is done modulo `256`.

There are four objects occurring in this equation. We know both
`cipher_n` and `cipher_{n-1}`. Assuming that the cleartext is actual
text, as suggested by the filename and the challenge description,
we also know something about which values for `clear_n` are
more likely than others.
If we rewrite the above equation as
```
S^{-1}[cipher_n] = clear_n + cipher_{n-1}
```
we can then interpret this as knowledge about which values are more likely
for `S^{-1}[cipher_n]` than others.
A single byte of information will not be sufficient to be confident of course,
hence the importance to have sufficient data.

# Solution

A very common character in text is the space. In this case it turned out
that counting how often a certain value for `S^{-1}[cipher_n]` would fit
with `clear_n` being a space was sufficient. If one had less data available
and really needed to make the most of it one could use actual occurrence
ratios of characters in comparison material.

So we begin with this count:
```python
with open('news.txt.enc', 'br') as f:
    data = f.read()

# assume the cleartext consists only of spaces and count how often
# a certain value fits with that
Sinv_statistics = [[0 for _ in range(256)] for _ in range(256)]
previous = 0
for cipher_char in data:
    Sinv_statistics[cipher_char][(ord(' ') + previous) % 256] += 1
    previous = cipher_char
```

If these statistics give us one possible value way more often than
the other ones, then it is very likely the correct one. What worked
out in this case was to be sure about a value if it appears twice
as often as the second most often one. However, this does not yet
determine all values of `Sinv`. But if we are e.g. sure that
`Sinv[0] = 0`, then we will be able to rule out `0` as the value
of `Sinv[c]` whenever `c` is not `0`. So after we found a couple
of values we are sure enough about we can remove those from consideration
from the rest and see if we can get further afterwards. This method worked
in this case, so an actual analysis of the probabilities was not
necessary.
```python
# Determine Sinv
Sinv = [None for _ in range(256)]
improved = True
while improved:
    print(Sinv)
    improved = False
    for cipher_char in range(256):
        if Sinv[cipher_char] is not None:
            continue
        max_val = max(Sinv_statistics[cipher_char])
        num_bigger = sum([1 if i > max_val//2 else 0 for i in Sinv_statistics[cipher_char]])
        if num_bigger == 1:
            # ok, we are pretty sure this is the correct one!
            improved = True
            Sinv[cipher_char] = Sinv_statistics[cipher_char].index(max_val)
            # Sinv[cipher_char] is now taken as a value, so remove it from the rest
            for value in range(256):
                if value == cipher_char:
                    continue
                Sinv_statistics[value][Sinv[cipher_char]] = 0
```

Finally, now that we have `Sinv` we can apply the decryption algorithm to
`news.txt.enc`. A simple `grep "flag{" news.txt` then finds the flag.
