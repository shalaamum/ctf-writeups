import sys,os
class X(object):
    def __init__(x,a=0,b=0,c=0):
        x.a=a
        x.b=b or ~-a
        x.c=c

    def mem_get(self, index, width):
        value = (self.b // (3**index)) % (3**width)
        output = f'Memory at {index} of width {width} accessed, value is 0x{value:02x}'
        if value < 127:
            output += f'="{chr(value)}"'
        print(output)
        return value

    def mem_set(self, index, width, value):
        if value >= 3**width:
            print(f'Trying to write {value} at {index} with only width {width}')
            raise Exception
        self.b = (self.b - ((self.b // (3**index)) % (3**width))*(3**index)) + value*(3**index)
        output = f'Memory at {index} of width {width} set to 0x{value:02x}'
        if value < 127:
            output += f'="{chr(value)}"'
        print(output)


    def run(x):
        if not x.c: return
        y=[0]*9
        while 3**y[8]<x.a:
            z = x.mem_get(y[8], 7)
            y[8]+=7
            a=z//3**4
            b=z//9%9
            c=z%9
            d = c + x.mem_get(y[8], 7) * 9
            if   a==0:
                os._exit(0)
            elif a==1:
                y[8]+=7
                y[b]=d
            elif a==2:
                y[b]=y[c]
            elif a in(3,8):
                y[b] = x.mem_get(y[c], 9)
                if a==8:
                    y[c]+=9
                    y[c]%=3**9
            elif a in(4,6,7):
                if a==6:
                    y[8]+=7
                    b,c,d=8,7,d or y[b]
                if a>4:
                    y[c]-=9
                    y[c]%=3**9
                x.mem_set(y[c], 9, y[b])
                if a==6:
                    y[8]=d
            elif a==5:
                if y[b]:
                    y[8]=d
                else:
                    y[8]+=7
                pass
            elif 9<=a<=12:
                y[b]={
                     9:lambda a,b:a<b,
                    10:lambda a,b:(a+b)%3**9,
                    11:lambda a,b:(a*b)%3**9,
                    12:lambda a,b:(a-b)%3**9,
                }[a](y[b],y[c])
            elif 13<=a<=15:
                e,f=0,y[c]
                for _ in range(9):
                    e*=3
                    e+={
                        13:lambda a,b:~(a+b)%3,
                        14:lambda a,b:min(a,b),
                        15:lambda a,b:max(a,b),
                    }[a](y[b]%3,f%3)
                    y[b]//=3
                    f//=3
                for _ in range(9):
                    y[b]*=3
                    y[b]+=e%3
                    e//=3
            elif a==16:
                y[b]=y[b]*3**c%3**9
            elif a==17:
                y[b]=y[b]//3**c
            elif a==18:
                y[b]=ord(sys.stdin.read(1))
            elif a==19:
                sys.stdout.write(chr(y[b]));sys.stdout.flush()
            else:
                0/0
