import os,sys
import math

REGNUM_RIP = 0

class Communicator(object):
    def __init__(self, input_buffer):
        self.input_buffer = input_buffer
    def __pos__(self):
        if len(self.input_buffer) == 0:
            raise Exception('Input buffer exhausted!')
        result = self.input_buffer[0]
        self.input_buffer = self.input_buffer[1:]
        return result
    def __add__(self, character_as_int):
        sys.stderr.buffer.write(bytes([character_as_int]))
        sys.stderr.buffer.flush()
    def __mul__(self, text):
        for character_as_int in text.encode('latin1'):
            self + character_as_int

class Emulator(object):
    def __init__(self, a=0, b=0, c=0):
        self.a, self.b, self.c = a, b, c
        self.rip_visited = set()
    def __getitem__(self, address):
        address %= 11**5
        address *= 5
        return (self.a // (11**address)) % (11**5)
    def __setitem__(self, address, value):
        if address in self.rip_visited:
            print(f'Memory at address {address} was changed, even though this address occurs as a value of RIP!')
        prev_value= self[address]
        address %= 11**5
        address *= 5
        self.a += (value - prev_value) * (11**address)
    def run(self, input_buffer):
        if not self.c:
            return
        _ = Communicator(input_buffer)
        REG = [0]*11
        while 10:
            self.rip_visited.add(REG[REGNUM_RIP])
            REG = [ r % (11**5) for r in REG]
            b = self[REG[REGNUM_RIP]]
            REG[REGNUM_RIP] += 1
            b,c=divmod(b,11)
            b,d=divmod(b,11)
            b,e=divmod(b,11)
            b,f=divmod(b,11)
            b,g=divmod(b,11)
            h=f+g*11
            i=e+h*11
            j=d+i*11
            if False:
                print(open('flag').read())
            elif c==5:
                REG[REGNUM_RIP] += j-7320
            elif c==2:
                REG[d] = self[REG[e]+f-5]
                REG[e] += g-5
            elif c==3:
                self[REG[e]+f-5] = REG[d]
                REG[e] += g-5
            elif c==4:
                REG[d] = i
            elif c==0:
                if not d|e|f|g:
                    break
                elif 1<=d<=8:
                    d -= 1
                    if[
                            lambda a,_:a==0,
                            lambda a,_:a!=0,
                            lambda a,b:a==b,
                            lambda a,b:a!=b,
                            lambda a,b:a>b,
                            lambda a,b:a>=b,
                            lambda a,b:a<b,
                            lambda a,b:a<=b,
                    ][d](REG[e],REG[f]):
                        REG[REGNUM_RIP] += 1
            elif c==7:
                REG[d] += i-665
            elif c==8:
                if not d:
                    REG[e]+=MCCCXXXI*h
                elif d==1:
                    self[REG[e]] = h-60
            elif c==6:
                if d<=7:
                    REG[e]=[
                        lambda a,b:a+b,
                        lambda a,b:a-b,
                        lambda a,b:a*b,
                        lambda a,b:a//b,
                        lambda a,b:a%b,
                        lambda a,b:min(a,b),
                        lambda a,b:max(a,b),
                        lambda a,b:~(a+b),
                    ][d](REG[f],REG[g])
            elif c==9:
                REG[9] = REG[REGNUM_RIP]
                REG[REGNUM_RIP] += j-7320
            elif c==10:
                memory_from_reg_e_of_len_reg_f = ( self.a // (11**(REG[e]*5)) ) % (11**(REG[f]*5))
                string_to_evaluate = memory_from_reg_e_of_len_reg_f.to_bytes(
                        REG[f]*3,'little').decode('latin1').strip('\0')
                eval_result = eval(string_to_evaluate)
                #print(f'\nEval of string "{string_to_evaluate}", from buffer starting at address {REG[e]} of length {REG[f]} resulted in {eval_result}')
                REG[d] = eval_result or (0)
            elif c == 1:
                REG[d] = REG[e]
                REG[f] = REG[g]

    def memory_length(self):
        # Returns the number of words held in memory
        return math.floor(math.log(self.a, 11**5))

    def disassemble_instruction(self, rip):
        res = f'{rip:04}:   '
        b = self[rip]
        rip += 1
        b,c=divmod(b, 11)
        b,d=divmod(b, 11)
        b,e=divmod(b, 11)
        b,f=divmod(b, 11)
        b,g=divmod(b, 11)
        h=f+g*11
        i=e+h*11
        j=d+i*11
    
        REG = [f'REG{i}' for i in range(11)]
        REG[REGNUM_RIP] = 'RIP'

        if c == 5:
            res += f'JMP {(rip + j - 7320) % 11**5}'
        elif c == 2:
            res += f'{REG[d]} = MEM[{REG[e]} + {f - 5}]'
            if g != 5:
                res += f'  ;  {REG[e]} += {g - 5}'
        elif c == 3:
            res += f'MEM[{REG[e]} + {f - 5}] = {REG[d]}'
            if g != 5:
                res += f'  ;  {REG[e]} += {g - 5}'
        elif c == 4:
            if d == REGNUM_RIP:
                res += f'JMP {i}'
            else:
                res += f'{REG[d]} = {i}'
        elif c == 0:
            if not d|e|f|g:
                res += 'HALT'
            elif 1 <= d <= 8:
                d -= 1
                if d == 0:
                    res += f'IF {REG[e]} == 0 THEN JMPNEXT'
                elif d == 1:
                    res += f'IF {REG[e]} != 0 THEN JMPNEXT'
                elif d == 2:
                    res += f'IF {REG[e]} == {REG[f]} THEN JMPNEXT'
                elif d == 3:
                    res += f'IF {REG[e]} != {REG[f]} THEN JMPNEXT'
                elif d == 4:
                    res += f'IF {REG[e]} > {REG[f]} THEN JMPNEXT'
                elif d == 5:
                    res += f'IF {REG[e]} >= {REG[f]} THEN JMPNEXT'
                elif d == 6:
                    res += f'IF {REG[e]} < {REG[f]} THEN JMPNEXT'
                elif d == 7:
                    res += f'IF {REG[e]} <= {REG[f]} THEN JMPNEXT'
                else:
                    res += 'NOP'
            else:
                res += 'NOP'

        elif c == 7:
            if d == REGNUM_RIP:
                res += f'JMP {rip + i - 665}'
            else:
                res += f'{REG[d]} += {i-665}'
        elif c == 8:
            if not d:
                res += f'WEIRDPANIC'
            elif d == 1:
                res += f'MEM[{REG[e]}] = {h - 60}'
            else:
                res += 'NOP'
        elif c == 6:
            if d <= 7:
                if d == 0:
                    res += f'{REG[e]} = {REG[f]} + {REG[g]}'
                elif d == 1:
                    res += f'{REG[e]} = {REG[f]} - {REG[g]}'
                elif d == 2:
                    res += f'{REG[e]} = {REG[f]} * {REG[g]}'
                elif d == 3:
                    res += f'{REG[e]} = {REG[f]} // {REG[g]}'
                elif d == 4:
                    res += f'{REG[e]} = {REG[f]} % {REG[g]}'
                elif d == 5:
                    res += f'{REG[e]} = min({REG[f]}, {REG[g]})'
                elif d == 6:
                    res += f'{REG[e]} = max({REG[f]}, {REG[g]})'
                elif d == 7:
                    res += f'{REG[e]} = ~({REG[f]} + {REG[g]})'
            else:
                res += 'NOP'
        elif c == 9:
            res += f'REG9 = RIP  ;  JMP {(rip + j - 7320) % 11**5}'
        elif c == 10:
            res += f'{REG[d]} = EVALUATEPY MEM[{REG[e]}:{REG[e]}+{REG[f]}]'
        elif c == 1:
            if d == REGNUM_RIP:
                res += f'JMP {REG[e]}'
            else:
                res += f'{REG[d]} = {REG[e]}'
            if f != g:
                res += f'  ;  {REG[f]} = {REG[g]}'
        else:
            res += f'BAD INSTRUCTION'
        return res

    def print_disassembly(self, rips=None):
        if rips is None:
            rips = range(self.memory_length())
        index_prev = None
        for index in rips:
            if index_prev is not None and index_prev + 1 != index:
                print(f'\n...jumped over {index - index_prev - 1} words...\n')
            print(self.disassemble_instruction(index))
            index_prev = index

