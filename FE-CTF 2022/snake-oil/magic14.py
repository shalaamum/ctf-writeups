import os,sys
import math
import string

REGNUM_RIP = 0
REGNUM_RETADDR = 9
REGNUM_RSP = 10

class Communicator(object):
    def __init__(self, input_buffer):
        self.input_buffer = input_buffer
    def __pos__(self):
        if len(self.input_buffer) == 0:
            raise Exception('Input buffer exhausted!')
        result = self.input_buffer[0]
        self.input_buffer = self.input_buffer[1:]
        return result
    def __add__(self, character_as_int):
        sys.stderr.buffer.write(bytes([character_as_int]))
        sys.stderr.buffer.flush()
    def __mul__(self, text):
        for character_as_int in text.encode('latin1'):
            self + character_as_int

class Emulator(object):
    def __init__(self, a=0, b=0, c=0):
        self.a, self.b, self.c = a, b, c
        self.rip_visited = set()
        self.rip_jumped_to = set()
        self.function_entries = set()
    def __getitem__(self, address):
        address %= 11**5
        address *= 5
        return (self.a // (11**address)) % (11**5)
    def __setitem__(self, address, value):
        #if address in self.rip_visited:
        #    print(f'Memory at address {address} was changed, even though this address occurs as a value of RIP!')
        prev_value= self[address]
        address %= 11**5
        address *= 5
        self.a += (value - prev_value) * (11**address)
    def run(self, input_buffer, registers=None):
        if not self.c:
            return
        _ = Communicator(input_buffer)
        if registers is None:
            REG = [0]*11
        else:
            REG = registers
        old_rip = -1
        just_reted = False
        while 10:
            self.rip_visited.add(REG[REGNUM_RIP])
            if REG[REGNUM_RIP] != old_rip + 1 and not just_reted:
                self.rip_jumped_to.add(REG[REGNUM_RIP])
            just_reted = False
            old_rip = REG[REGNUM_RIP]
            REG = [ r % (11**5) for r in REG]
            b = self[REG[REGNUM_RIP]]
            REG[REGNUM_RIP] += 1
            b,c=divmod(b,11)
            b,d=divmod(b,11)
            b,e=divmod(b,11)
            b,f=divmod(b,11)
            b,g=divmod(b,11)
            h=f+g*11
            i=e+h*11
            j=d+i*11
            if False:
                print(open('flag').read())
            elif c==5:
                REG[REGNUM_RIP] += j-7320
            elif c==2:
                REG[d] = self[REG[e]+f-5]
                REG[e] += g-5
            elif c==3:
                self[REG[e]+f-5] = REG[d]
                REG[e] += g-5
            elif c==4:
                REG[d] = i
            elif c==0:
                if not d|e|f|g:
                    break
                elif 1<=d<=8:
                    d -= 1
                    if[
                            lambda a,_:a==0,
                            lambda a,_:a!=0,
                            lambda a,b:a==b,
                            lambda a,b:a!=b,
                            lambda a,b:a>b,
                            lambda a,b:a>=b,
                            lambda a,b:a<b,
                            lambda a,b:a<=b,
                    ][d](REG[e],REG[f]):
                        REG[REGNUM_RIP] += 1
            elif c==7:
                REG[d] += i-665
            elif c==8:
                if not d:
                    REG[e]+=MCCCXXXI*h
                elif d==1:
                    self[REG[e]] = h-60
            elif c==6:
                if d<=7:
                    REG[e]=[
                        lambda a,b:a+b,
                        lambda a,b:a-b,
                        lambda a,b:a*b,
                        lambda a,b:a//b,
                        lambda a,b:a%b,
                        lambda a,b:min(a,b),
                        lambda a,b:max(a,b),
                        lambda a,b:~(a+b),
                    ][d](REG[f],REG[g])
            elif c==9:
                self.function_entries.add(REG[REGNUM_RIP] + j-7320)
                REG[9] = REG[REGNUM_RIP]
                REG[REGNUM_RIP] += j-7320
            elif c==10:
                memory_from_reg_e_of_len_reg_f = ( self.a // (11**(REG[e]*5)) ) % (11**(REG[f]*5))
                string_to_evaluate = memory_from_reg_e_of_len_reg_f.to_bytes(
                        REG[f]*3,'little').decode('latin1').strip('\0')
                #print(f'Am going to evaluate string "{string_to_evaluate}"')
                eval_result = eval(string_to_evaluate)
                #print(f'\nEval of string "{string_to_evaluate}", from buffer starting at address {REG[e]} of length {REG[f]} resulted in {eval_result}')
                REG[d] = eval_result or (0)
            elif c == 1:
                if d == REGNUM_RIP:
                    if e == REGNUM_RETADDR:
                        just_reted = True
                REG[d] = REG[e]
                REG[f] = REG[g]

    def memory_length(self):
        # Returns the number of words held in memory
        return math.floor(math.log(self.a, 11**5))

    def disassemble_instruction(self, rip, functions={}):
        res = (f'{rip:04}: {">" if rip in self.function_entries else " "}'
            + f'{">" if rip in self.rip_jumped_to else " "}  ')
        b = self[rip]
        rip += 1
        b,c=divmod(b, 11)
        b,d=divmod(b, 11)
        b,e=divmod(b, 11)
        b,f=divmod(b, 11)
        b,g=divmod(b, 11)
        h=f+g*11
        i=e+h*11
        j=d+i*11
    
        REG = [f'REG{i}' for i in range(11)]
        REG[REGNUM_RIP] = 'RIP'
        REG[REGNUM_RETADDR] = 'RETADDR'
        REG[REGNUM_RSP] = 'RSP'

        if c == 5:
            res += f'JMP {(rip + j - 7320) % 11**5}'
        elif c == 2:
            if e == REGNUM_RSP and f == 5 and g == 6:
                res += f'POP {REG[d]}'
            else:
                res += f'{REG[d]} = MEM[{REG[e]} + {f - 5}]'
                if g != 5:
                    res += f'  ;  {REG[e]} += {g - 5}'
        elif c == 3:
            if e == REGNUM_RSP and f == 4 and g == 4:
                res += f'PUSH {REG[d]}'
            else:
                res += f'MEM[{REG[e]} + {f - 5}] = {REG[d]}'
                if g != 5:
                    res += f'  ;  {REG[e]} += {g - 5}'
        elif c == 4:
            if d == REGNUM_RIP:
                res += f'JMP {i}'
            else:
                res += f'{REG[d]} = {i}'
                if 0 <= i < 256:
                    res += f' = {bytes([i])}'
        elif c == 0:
            if not d|e|f|g:
                res += 'HALT'
            elif 1 <= d <= 8:
                d -= 1
                if d == 0:
                    res += f'IF {REG[e]} == 0 THEN JMPNEXT'
                elif d == 1:
                    res += f'IF {REG[e]} != 0 THEN JMPNEXT'
                elif d == 2:
                    res += f'IF {REG[e]} == {REG[f]} THEN JMPNEXT'
                elif d == 3:
                    res += f'IF {REG[e]} != {REG[f]} THEN JMPNEXT'
                elif d == 4:
                    res += f'IF {REG[e]} > {REG[f]} THEN JMPNEXT'
                elif d == 5:
                    res += f'IF {REG[e]} >= {REG[f]} THEN JMPNEXT'
                elif d == 6:
                    res += f'IF {REG[e]} < {REG[f]} THEN JMPNEXT'
                elif d == 7:
                    res += f'IF {REG[e]} <= {REG[f]} THEN JMPNEXT'
                else:
                    res += 'NOP'
            else:
                res += 'NOP'

        elif c == 7:
            if d == REGNUM_RIP:
                res += f'JMP {rip + i - 665}'
            else:
                res += f'{REG[d]} += {i-665}'
        elif c == 8:
            if not d:
                res += f'WEIRDPANIC'
            elif d == 1:
                res += f'MEM[{REG[e]}] = {h - 60}'
            else:
                res += 'NOP'
        elif c == 6:
            if d <= 7:
                if d == 0:
                    res += f'{REG[e]} = {REG[f]} + {REG[g]}'
                elif d == 1:
                    res += f'{REG[e]} = {REG[f]} - {REG[g]}'
                elif d == 2:
                    res += f'{REG[e]} = {REG[f]} * {REG[g]}'
                elif d == 3:
                    res += f'{REG[e]} = {REG[f]} // {REG[g]}'
                elif d == 4:
                    res += f'{REG[e]} = {REG[f]} % {REG[g]}'
                elif d == 5:
                    res += f'{REG[e]} = min({REG[f]}, {REG[g]})'
                elif d == 6:
                    res += f'{REG[e]} = max({REG[f]}, {REG[g]})'
                elif d == 7:
                    res += f'{REG[e]} = ~({REG[f]} + {REG[g]})'
            else:
                res += 'NOP'
        elif c == 9:
            address_to_call = (rip + j - 7320) % 11**5
            res += 'CALL '
            if address_to_call in functions:
                res += functions[address_to_call]
            else:
                res += str(address_to_call)
        elif c == 10:
            res += f'{REG[d]} = EVALUATEPY MEM[{REG[e]}:{REG[e]}+{REG[f]}]'
        elif c == 1:
            if d == REGNUM_RIP:
                if e == REGNUM_RETADDR:
                    res += f'RET'
                else:
                    res += f'JMP {REG[e]}'
            else:
                res += f'{REG[d]} = {REG[e]}'
            if f != g:
                res += f'  ;  {REG[f]} = {REG[g]}'
        else:
            res += f'BAD INSTRUCTION'
        return res

    def print_disassembly(self, rips=None, functions={}):
        if rips is None:
            rips = range(self.memory_length())
        index_prev = None
        for index in rips:
            if index_prev is not None and index_prev + 1 != index:
                print(f'\n...jumped over {index - index_prev - 1} words...\n')
            if index in functions:
                print(f'\n      function {functions[index]}')
            print(self.disassemble_instruction(index, functions=functions))
            index_prev = index


def byte_list_to_base_11_list(buffer):
    number_value = 0
    for i in range(len(buffer)):
        number_value += 2**(8*i) * buffer[i]
    result = []
    while number_value > 0:
        remainder = number_value % (11**5)
        number_value //= 11**5
        result.append(remainder)
    return result

def base_11_list_to_byte_list(buffer):
    number_value = 0
    for i in range(len(buffer)):
        number_value += 11**(5*i) * buffer[i]
    result = []
    while number_value > 0:
        remainder = number_value % (2**8)
        number_value //= 2**8
        result.append(remainder)
    return result
