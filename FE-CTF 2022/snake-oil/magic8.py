import os,sys

REGNUM_RIP = 0

class A(object):
    def __pos__(self):
        return sys.stdin.buffer.read(1)[0]
    def __add__(self, character_as_int):
        sys.stdout.buffer.write(bytes([character_as_int]))
        sys.stdout.buffer.flush()
    def __mul__(self, text):
        for character_as_int in text.encode('latin1'):
            self + character_as_int

class B(object):
    def __init__(self, a=0, b=0, c=0):
        self.a, self.b, self.c = a, b, c
    def __getitem__(self, address):
        address %= 11**5
        address *= 5
        return (self.a // (11**address)) % (11**5)
    def __setitem__(self, address, value):
        prev_value= self[address]
        address %= 11**5
        address *= 5
        self.a += (value - prev_value) * (11**address)
    def run(self):
        if not self.c:
            return
        _ = A()
        REG = [0]*11
        while 10:
            REG = [ r % (11**5) for r in REG]
            b = self[REG[REGNUM_RIP]]
            REG[REGNUM_RIP] += 1
            b,c=divmod(b,11)
            b,d=divmod(b,11)
            b,e=divmod(b,11)
            b,f=divmod(b,11)
            b,g=divmod(b,11)
            h=f+g*11
            i=e+h*11
            j=d+i*11
            if False:
                print(open('flag').read())
            elif c==5:
                REG[REGNUM_RIP] += j-7320
            elif c==2:
                REG[d] = self[REG[e]+f-5]
                REG[e] += g-5
            elif c==3:
                self[REG[e]+f-5] = REG[d]
                REG[e] += g-5
            elif c==4:
                REG[d] = i
            elif c==0:
                if not d|e|f|g:
                    break
                elif 1<=d<=8:
                    d -= 1
                    if[
                            lambda a,_:a==0,
                            lambda a,_:a!=0,
                            lambda a,b:a==b,
                            lambda a,b:a!=b,
                            lambda a,b:a>b,
                            lambda a,b:a>=b,
                            lambda a,b:a<b,
                            lambda a,b:a<=b,
                    ][d](REG[e],REG[f]):
                        REG[REGNUM_RIP] += 1
            elif c==7:
                REG[d] += i-665
            elif c==8:
                if not d:
                    REG[e]+=MCCCXXXI*h
                elif d==1:
                    self[REG[e]] = h-60
            elif c==6:
                if d<=7:
                    REG[e]=[
                        lambda a,b:a+b,
                        lambda a,b:a-b,
                        lambda a,b:a*b,
                        lambda a,b:a//b,
                        lambda a,b:a%b,
                        lambda a,b:min(a,b),
                        lambda a,b:max(a,b),
                        lambda a,b:~(a+b),
                    ][d](REG[f],REG[g])
            elif c==9:
                REG[9] = REG[REGNUM_RIP]
                REG[REGNUM_RIP] += j-7320
            elif c==10:
                memory_from_reg_e_of_len_reg_f = ( self.a // (11**(REG[e]*5)) ) % (11**(REG[f]*5))
                string_to_evaluate = memory_from_reg_e_of_len_reg_f.to_bytes(
                        REG[f]*3,'little').decode('latin1').strip('\0')
                eval_result = eval(string_to_evaluate)
                print(f'\nEval of string "{string_to_evaluate}", from buffer starting at address {REG[e]} of length {REG[f]} resulted in {eval_result}')
                REG[d] = eval_result or (0)
            elif not ~-c:
                REG[d] = REG[e]
                REG[f] = REG[g]
