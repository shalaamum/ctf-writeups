## Summary

This is a writeup for the "Doctored Dobbertin" challenge by bit and newton at [Hack.lu CTF 2023](https://flu.xxx/) that I solved playing for [Kalmarunionen](https://www.kalmarunionen.dk/). This challenge involved AES with tweaks and a modified key schedule. We get the encrypted flag together with the tweak used, and can request further 7 ciphertexts encrypted with the same key, and where we choose plaintext and tweak. The challenge was solved using differential cryptanalysis. We were the first team to solve this challenge, with the only other team to solve it during the CTF being Black Bauhinia.

Writeup by: shalaamum

Solved by: shalaamum


## The AES variant used

The cipher used was 10-round AES, with two major changes: the addition of tweaks, and a completely different key schedule. The modified key schedule made it very easy to convert between bytes of the round keys and the key itself, but the main part of the cipher attacked was the tweak. This writeup assumes the reader is familiar with AES. We arrange encryption as 9 full rounds (0 through 8) that start with round key addition and end with mixing, followed by round 9, which leaves out mixing, followed by a final round key addition (round key 10).

### The key schedule

The key consists of 10 bytes. The round key bytes in the two right columns are always either one of the bytes of the key, or the S applied to one of the bytes of the key. For rounds 0 to 9, the two left columns are the same as the two right columns, whereas for the last round key the two left columns are zero.

### The tweak

The cipher takes a 128 bit tweak. The tweak is expanded to round tweaks that are then added to the state after each round key addition in rounds 0 to 9. The two left columns of the round tweaks are always zero, and the remaining 64 bits for the two right columns are taken from 64 cyclically consecutive bits of the tweak. This 64 bit wide window is shifted by 11 each round.

The important thing we need about this is the following: If all bits of the tweak except possibly bits 13 through 21 are zero, then round tweaks 1 through 6 will be zero.


## The solution

### Related tweaks

Suppose we already have a plaintext-tweak-ciphertexts triple `(plain_1, tweak_1, ciphertext_1)`. We want to find a related triple where the state is the same as for this triple for as long as possible. To do this, let `tweak_delta` be a nonzero 128 bit value such that all bits except possibly bit 13 through 21 are zero. We then choose `tweak_2 = tweak_1 xor tweak_delta`. With this tweak, as mentioned before, the round tweaks expanded from `tweak_1` and `tweak_2` will be the same for rounds 1 through 6. Note however that round tweak 0 is added to the plaintext right at the start (well, after adding the first round key, but these two operations commute with each other). Thus we can calculate the first round tweak for `tweak_2` and add that to `plaintext_1` to obtain `plaintext_2`. Now these additions will cancel each other out. Thus the state we obtain at the start of round 7 for the two encryptions will be identical.

Let us now look at how the difference that we can observe at specific times during the encryption between these two inputs. Note: As the window the round tweaks are taken from shifts by 11 bits each round, while a byte has only 8 bits, there is sometimes a "jump" regarding in which entry of the matrix the difference from the round tweak appears. The below is thus just one example, the other cases work exactly the same otherwise.

### Round 7
At the start of round 7 this will look like this, as we just mentioned: the difference is zero.
```
0, 0, 0, 0
0, 0, 0, 0
0, 0, 0, 0
0, 0, 0, 0
```

After adding the round tweak for round 7, a known difference is introduced in the bottom right.
```
0, 0, 0, 0
0, 0, 0, 0
0, 0, 0, 0
0, 0, 0, known non-zero
```

After application of the S-box, we can not anymore predict the precise difference, but it must be nonzero.
```
0, 0, 0, 0
0, 0, 0, 0
0, 0, 0, 0
0, 0, 0, X
```

This is then shifted.
```
0, 0, 0, 0
0, 0, 0, 0
0, 0, 0, 0
X, 0, 0, 0
```

Now mixing happens, which causes differences in the entire first column.
```
X, 0, 0, 0
Y, 0, 0, 0
Z, 0, 0, 0
W, 0, 0, 0
```
Note that each of the four differences in the first row must be non-zero, as X was (this follows from the entries of the MixColumns matrix all being non-zero and hence invertible in `GF(2^8)`).

### Round 8

After adding the tweak, a new difference is introduced that we know.
```
X, 0, 0, 0
Y, 0, 0, 0
Z, 0, 0, known non-zero
W, 0, 0, 0
```

After substituting, we lose knowledge of the precise difference. All differences must still be non-zero.
```
X, 0, 0, 0
Y, 0, 0, 0
Z, 0, 0, V
W, 0, 0, 0
```

After shifting, we then get:
```
X, 0, 0, 0
0, 0, 0, Y
0, V, Z, 0
0, W, 0, 0
```

Now mixing happens. This will cause differences in each entry. Let us focus on the three columns in which there is only one difference that is nonzero so far. The mixing operation is linear, so the difference for the entire column will be the application of the mixing operation to a vector where three entries are zero. There are only 255 possible vectors of the form `(X, 0, 0, 0)` with `X` nonzero, hence `MixColumns(X, 0, 0, 0)` must lie in a subset of at most 255 elements in `GF(2^8)^4`. The differences we can observe in the four entries of these columns are thus not independent. We note this below by using the same letter. In the column in which there were already two entries with nonzero differences, a similar argument shows that the difference vector for the column must lie in a subset of at most `255^2` elements, but this is less useful to us, so we just consider that column difference to be unknown now.
```
X_1, ?, Z_1, Y_1
X_2, ?, Z_2, Y_2
X_3, ?, Z_3, Y_3
X_4, ?, Z_4, Y_4
```

By simulation with example keys/tweaks/plaintexts we can quickly find the 255 possible difference-vectors that are possible for the three columns. We thus found a strong structural property the states of the two encryptions must be related by at the start of round 9.


### Guessing key bytes one byte at a time

We now guess one byte of the last round key and work backwards. In the pictures below we mark with `K` if we know something without having to guess, and `x` if we know it after guessing a byte of the last round key. We mark with `?` if we don't know it.

We know the state of both encryptions at the very end, as we get the ciphertext.
```
K, K, K, K
K, K, K, K
K, K, K, K
K, K, K, K
```

We know the two leftmost columns of the last ciphertext, as they are just zero, and let us assume we also guess a value for the round key in the bottom right. Then we can undo the addition of the last round key to get this state of knowledge:
```
K, K, ?, ?
K, K, ?, ?
K, K, ?, ?
K, K, ?, x
```

After inverting the shift, the picture looks like this:
```
K, K, ?, ?
?, K, K, ?
?, ?, K, K
K, ?, x, K
```

We can also undo addition of constants as well as the round tweak, as they are known, still giving us the same picture.
We can not undo addition of round key 9, so let us now switch to considering the differences of the states in the two encryptions instead. Then the round key addition cancels, so we have knowledge of these parts of the difference of the state at the start of round 9.
```
K, K, ?, ?
?, K, K, ?
?, ?, K, K
K, ?, x, K
```

This means, conditional on our guess for that one byte of the last round key being correct, we know three out of four entries of the third column! In the previous section, we arrived at the conclusion that we can calculate a set of only 255 possibilities for the difference vector in the third column. Already the two entries that are known independent of our guess likely pin down which vector this must be, thereby determining what the difference in the entry marked with x must be. Wrong guesses for the byte of the last round key that we guessed will thus likely cause the difference we obtain in the entry marked with x to be incompatible, and hence we can throw out that guess. In practice there usually seem to be only 2 choices for that round key byte that are compatible.


### Bruteforcing the rest

Repeating the above with a couple of different values for `tweak_delta` the possible values of the last round key can quickly be reduced - in practice there were usually 2 bytes with 2 choices left, and the other round key bytes were determined completely. The key itself has 10 bytes, so there are 2 more bytes left. To attack those one could continue in a similar manner as before, but use knowledge about the last round key already gained to roll back the state further, guessing one byte of the key that occurs in round key 9 but not round key 10. However, the remaining space of `2*2*256*256 = 262 144` possible keys is small enough that a simple bruteforce suffices. As 6 bytes of the plaintext (prefix `flag{` and suffix `}`) are known and `256^6` is significantly larger then the size of our search space, it is very unlikely that there is more than one choice decrypting to something matching the expected pattern. Decrypting the given ciphertext with all the possible keys took less than 5 minutes, yielding the flag.


## Attachments

- [Challenge script](./app.py) and [modified AES implementation](./oracle.py) provided by the challenge.
- [Copy of the AES implementation with some extra functionality needed for the solution (e.g. decryption, encrypting only up to a specific round, etc.)](./myoracle.py)
- [Solve script](./solve.py)

