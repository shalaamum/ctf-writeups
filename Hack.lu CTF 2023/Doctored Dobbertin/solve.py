#!/usr/bin/env python3

# Solve script by shalaamum (Kalmarunionen) for the "Doctored Dobbertin" challenge by bit and newton at Hack.lu CTF 2023.

import copy
from myoracle import TAES_Oracle as MyOracle
from tqdm import tqdm
import random
import collections
import itertools
import functools
import pwnlib.tubes.process
import pwnlib.tubes.remote


cipher = MyOracle()
oracle = MyOracle()


def bit_one_place(n, i):
    s = ''.join(['0'] * i + ['1'] + ['0']*(n-i-1))
    i = int(s, 2)
    return i.to_bytes(n // 8, 'big')


def num_entries_nonzero(matrix):
    nonzero = 0
    for row in range(4):
        for column in range(4):
            if matrix[column][row] != 0:
                nonzero += 1
    return nonzero


def generate_deltas(tweak_delta):
    '''Generates the delta for the plaintext to get same state until round 7, and the deltas
    in the tweak in round 7, 8 and 9.
    Takes input as bytes'''
    tweak_delta = f'{int.from_bytes(tweak_delta, "big"):0128b}'
    #print(tweak_delta)
    round_tweak_deltas = cipher._expand_tweak(tweak_delta)

    # First we check and handle round 0
    if num_entries_nonzero(round_tweak_deltas[0]) > 1:
        raise ValueError
    plaintext_delta = b''.join(round_tweak_deltas[0])

    # Next we check round 8 is the first nonzero
    for i in range(1, 7):
        if num_entries_nonzero(round_tweak_deltas[i]) > 0:
            raise ValueError

    return plaintext_delta, round_tweak_deltas[7], round_tweak_deltas[8], round_tweak_deltas[9]


def xor(a, b):
    return bytes([x ^ y for x, y in zip(a,b)])


def xor_matrix(mat1, mat2):
    return [[mat1[col][row] ^ mat2[col][row] for row in range(4)] for col in range(4)]


def simulate_differential_columns(tweak_delta):
    '''Simulate the difference of column vectors after the last mix
    that are possible with this tweak difference.
    There should be 255 possible (not 256, because difference zero is not possible).
    Instead of doing this symbolically and tracing things through, we just try with 5000 random
    plaintexts, keys, and tweaks, and afterwards check that we found all 255.
    '''
    print('Simulating possible column deltas before last mix...')
    plaintext_delta, _, tweak_delta_8, _ = generate_deltas(tweak_delta)
    for col in range(4):
        for row in range(4):
            if tweak_delta_8[col][row] != 0:
                break
    col_with_two_deltas = (col - row) % 4
    #print(f'Column with two deltas: {col_with_two_deltas}')

    deltas_possible = [set() for column in range(4)]
    deltas_possible[col_with_two_deltas] = None

    for _ in tqdm(range(5000)):
        c = MyOracle()
        plaintext_1 = random.randbytes(16)
        plaintext_2 = xor(plaintext_1, plaintext_delta)
        tweak_1 = random.randbytes(16)
        tweak_2 = xor(tweak_1, tweak_delta)
        enc_1 = c.encrypt_to_after_last_mix(plaintext_1, tweak_1.hex())
        enc_2 = c.encrypt_to_after_last_mix(plaintext_2, tweak_2.hex())
        output_deltas = xor_matrix(enc_1, enc_2)
        #print(enc_1)
        #print(enc_2)
        #print(output_deltas)
        for col in range(4):
            if col == col_with_two_deltas:
                continue
            col_diff_vec = tuple(output_deltas[col])
            deltas_possible[col].add(col_diff_vec)
    for col in range(4):
        if col == col_with_two_deltas:
            continue
        #print(len(deltas_possible[col]))
        assert len(deltas_possible[col]) == 255
    return deltas_possible


def round_key_sym(r, col, row):
    '''Returns how the round key in the specified row and column in round r depends on the key.
    0 if always zero. Otherwise (True, i) if it is S(key[i]) and (False, i) if it is key[i]'''
    if r == 10 and col < 2:
        return 0
    perm = [4, 3, 6, 2, 5, 8, 7, 0, 9, 1]
    key_bytes = [(False, i) for i in range(10)]
    for rnd in range(r):
        p_key_bytes = [0] * 10
        for p in range(10):
            p_key_bytes[perm[p]] = key_bytes[p]
        key_bytes = p_key_bytes
        b, i = key_bytes[0]
        assert not b
        key_bytes[0] = (True, i)
    return key_bytes[(col%2)*4 + row]


Sinv = [None] * 0x100
for x in range(256):
    Sinv[cipher.s_box[x]] = x
S = list(cipher.s_box)


def partial_add_round_key(r, key, state):
    '''Add a round key and deal with some entries of key or state being None because we don't know them.'''
    for col, row in itertools.product(range(4), repeat=2):
        rnd_key = round_key_sym(r, col, row)
        if rnd_key == 0:
            continue
        b, i = rnd_key
        if i not in key.keys():
            state[col][row] = None
            continue
        k = key[i]
        if b:
            k = S[k]
        if state[col][row] is None:
            continue
        state[col][row] ^= k


def print_matrix(matrix):
    for row in range(4):
        print('[ ', end='')
        for column in range(4):
            print(f'{matrix[column][row]:02x}\t' if matrix[column][row] is not None else '--\t', end='')
        print(' ]')
    print()


def shift(s):
    s[0][1], s[1][1], s[2][1], s[3][1] = s[1][1], s[2][1], s[3][1], s[0][1]
    s[0][2], s[1][2], s[2][2], s[3][2] = s[2][2], s[3][2], s[0][2], s[1][2]
    s[0][3], s[1][3], s[2][3], s[3][3] = s[3][3], s[0][3], s[1][3], s[2][3]


def unshift(s):
    shift(s)
    shift(s)
    shift(s)


def partial_sinv(s):
    '''Invert substitute in the matrix and handle unknown entries'''
    for col, row in itertools.product(range(4), repeat=2):
        if s[col][row] is None:
            continue
        s[col][row] = Sinv[s[col][row]]


def partial_xor(s, t):
    '''xor for matrix with possibly unknown entries'''
    r = [[None]*4 for _ in range(4)]
    for col, row in itertools.product(range(4), repeat=2):
        if s[col][row] is None or t[col][row] is None:
            continue
        r[col][row] = s[col][row] ^ t[col][row]
    return r


def key_part_sure(key_possibilities):
    '''Extracts the key part we are already certain about.'''
    key = dict()
    for i in range(10):
        if len(key_possibilities[i]) == 1:
            key[i] = list(key_possibilities[i])[0]
    return key


def solve_key_for_tweak(key_possibilities, plaintext_1, tweak_1, enc_1, tweak_delta):
    column_differentials = simulate_differential_columns(tweak_delta) 
    plaintext_delta, _, tweak_delta_8, tweak_delta_9 = generate_deltas(tweak_delta)

    plaintext_2 = xor(plaintext_1, plaintext_delta)
    tweak_2 = xor(tweak_1, tweak_delta)
    enc_2 = get_ciphertext(plaintext_2, tweak_2.hex())

    enc_1_matrix = cipher._bytes2matrix(enc_1)
    enc_2_matrix = cipher._bytes2matrix(enc_2)

    for column_unknown in range(4):
        if column_differentials[column_unknown] is None:
            break

    for key_index in range(10):
        key = key_part_sure(key_possibilities)
        for key_guess in list(key_possibilities[key_index]):
            key[key_index] = key_guess
            state_1 = copy.deepcopy(enc_1_matrix)
            state_2 = copy.deepcopy(enc_2_matrix)
            partial_invert_until_after_last_mix(key, state_1, debug=False)
            partial_invert_until_after_last_mix(key, state_2, debug=False)
            delta = partial_xor(state_1, state_2)
            delta = partial_xor(delta, tweak_delta_9)
            #print('Delta:')
            #print_matrix(delta)
            if not check_delta_compatible(column_differentials, delta):
                key_possibilities[key_index].remove(key_guess)
        print(f'Key possibilities: {[len(x) for x in key_possibilities]}')


def check_delta_compatible(column_differentials, delta):
    '''Checks whether the delta for the whole matrix is in the possibilities for the column deltas'''
    result = True
    for col in range(4):
        if column_differentials[col] is None:
            continue
        possible = False
        for column_delta in column_differentials[col]:
            this_one_possible = True
            for row in range(4):
                if (delta[col][row] is not None) and (delta[col][row] != column_delta[row]):
                    this_one_possible = False
                    break
            if this_one_possible:
                possible = True
                break
        result = result and possible
    return result


def partial_invert_until_after_last_mix(key, state, debug=False):
    if debug:
        print(f'key = {key}')
    if debug:
        print('End state:')
        print_matrix(state)
    partial_add_round_key(10, key, state)
    if debug:
        print('Before last round key addition')
        print_matrix(state)
    unshift(state)
    if debug:
        print('Before shifting:')
        print_matrix(state)
    partial_sinv(state)
    if debug:
        print('Before S')
        print_matrix(state)
    

def brute_flag(flag_tweak, flag_enc, key_possibilities):
    '''Brute force the remaining indeterminancy'''
    num_combos = 1 
    for x in key_possibilities:
        num_combos *= len(x)
    print(f'Number of combinations to bruteforce over: {num_combos}')
    for key in tqdm(itertools.product(*key_possibilities)):
        cipher = MyOracle(key=bytes(key))
        flag = cipher.decrypt(flag_enc, flag_tweak)
        if flag[:5] == b'flag{':
            print(f'\n{flag}\n')
            return flag


def get_flag(flag_tweak, flag_enc):
    key_possibilities = [set(range(256)) for _ in range(10)]
    plaintext_1 = random.randbytes(16)
    tweak_1 = random.randbytes(16)
    enc_1 = get_ciphertext(plaintext_1, tweak_1.hex())
    solve_key_for_tweak(key_possibilities, plaintext_1, tweak_1, enc_1, bit_one_place(128, 21))
    solve_key_for_tweak(key_possibilities, plaintext_1, tweak_1, enc_1, bit_one_place(128, 13))
    solve_key_for_tweak(key_possibilities, plaintext_1, tweak_1, enc_1, bit_one_place(128, 15))
    solve_key_for_tweak(key_possibilities, plaintext_1, tweak_1, enc_1, bit_one_place(128, 17))
    return brute_flag(flag_tweak, flag_enc, key_possibilities)


################################
#flag_secret = b'flag{testflag!!}'
#flag_tweak = random.randbytes(16).hex()
#flag_enc = oracle.encrypt(flag_secret, flag_tweak)
#del flag_secret
#def get_ciphertext(plaintext, tweak):
#    return oracle.encrypt(plaintext, tweak)
################################
io = pwnlib.tubes.process.process('./app.py')
#io = pwnlib.tubes.remote.remote('flu.xxx', 10040)
io.recvuntil(b'challenge: ')
flag_enc = bytes.fromhex(io.recvline().strip().decode())
io.recvuntil(b'used: ')
flag_tweak = io.recvline().strip().decode()
def get_ciphertext(plaintext, tweak):
    io.recvuntil(b'>')
    io.sendline(plaintext.hex().encode())
    io.recvuntil(b'>')
    io.sendline(tweak.encode())
    data = bytes.fromhex(io.recvline().strip().decode())
    return data
##############################################

get_flag(flag_tweak, flag_enc)
# flag{sp00kd0bb}

