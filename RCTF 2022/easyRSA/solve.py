#!/usr/bin/env python3

import sage.all

QQ = sage.all.QQ
ZZ = sage.all.ZZ

def v(e, r, modulus):
    result = sage.all.lucas_number2(e, r, 1)
    return result % modulus

v_dict = {}
def v_fast(n, r, modulus):
    n = int(n)
    r = int(r)
    modulus = int(modulus)
    if n == 0: return 2
    if n == 1: return r
    if (n,r,modulus) in v_dict.keys(): return v_dict[(n,r,modulus)]
    if(n % 2 == 0): ret = (pow(v_fast(n // 2, r, modulus), 2, modulus) - 2) % modulus
    else: ret = (r * pow(v_fast(n // 2, r, modulus), 2, modulus) -  v_fast(n // 2, r, modulus) * v_fast((n // 2) - 1, r, modulus) - r) % modulus
    v_dict[(n, r, modulus)] = ret
    return ret

#print('Testing fast v:')
#for i in range(100):
#    assert v(i, p, N) == v_fast(i, p, N)
#print('All correct!')
v = v_fast

def encrypt(m, e, N):
    c = (1 + m * N) * v(e, r, N*N) % (N * N)
    return c


print('Generating test values')
p = 7907
q = 7919
N = p*q
d = 7883
r = 7879
e = ZZ(d).inverse_mod((p * p - 1) * (q * q - 1))
m = int.from_bytes(b'RC', 'big')
c = encrypt(m, e, N)
print(f'p = {p}')
print(f'q = {q}')
print(f'N = {N}')
print(f'd = {d}')
print(f'r = {r}')
print(f'e = {e}')
print(f'm = {m}')
print(f'c = {c}')
print('\n\n\n')
del p
del q
del d
del r


# Values from the challenge
e = 3121363059746835628022404544403822724460605553641332612055010587129451973002475126644668174294955070747985002800863652917895939538596303356113483509581841527286351537287500304267975061675901109982875778527827742120878835367386538561039072391997357702421691095861694681707017921391244519593945584755632901987840338065879901115934561426583008838453244051629340056867760923894623105542463500022221236457852502822707466528439969484890601953615303609725566617126458934095119670087068752543521167517461730977044465374505011791902510131823556603316457085145886999220426746234986984619161299098173535371540923264898459106461
c = 3023313363629909506923927199426293187583112749147539699346723655095868214179291222441307436555352537055690155418715652987685459938250844145450675418187664719327350488160722838989675928696633353180233455017609936874014883975932740217672705286265535646106053294507962613498142617741362730709360885118905440314573392981528077265110441270212385951070591696827167771592664502652520790612367259434545169836933571343480057141790292296952743986731389468760364416344837575740236416472589700581583016227273449673820568427641136163703116276104550877191839851640920430919278802098196408637904780725723268371465670950321881886863
N = 101946888552605033726177837709738163930032970477361664394564134626639467843553634920510447339985842689387519517553714582991506722045078696771986052246306068257957261478416093188640437503481862825381241480405463985516598520453211217206308826779669980833596066677262549841524134539729279446910817169620871929289

e = ZZ(e)
c = ZZ(c)
N = ZZ(N)

Nprime = QQ(N)**2 - QQ(9)/QQ(4)*N + 1
print(f"N' = {Nprime}")
num_for_cont_frac = QQ(e) / Nprime
print(f'Taking the contiued fraction of\n{num_for_cont_frac}\n')
cont_frac = sage.all.continued_fraction(num_for_cont_frac)

k = None
d = None

for i in range(1000):
    denom = cont_frac.denominator(i)
    if not sage.all.is_prime(denom):
        continue
    bit_length = ZZ(denom).nbits() 
    if bit_length > (N.nbits() // 2): 
        continue
    print(f'{i}th convergent denominator of bit length {bit_length}: {denom}')
    if bit_length == (N.nbits() // 2):
        # just one, very likely the right one
        k = cont_frac.numerator(i)
        d = denom
        break

print(f'\nFound k and d:')
print(f'k = {k}')
print(f'd = {d}')

# we have (p^2 - 1) * (q^2 - 1) = (ed - 1) / k
# and N = p * q

p2q2expr = (e * d - 1) / k
assert p2q2expr.is_integer()
p2q2expr = ZZ(p2q2expr)
print(f'(p^2 - 1) * (q^2 - 1) = {p2q2expr}')

R = sage.all.PolynomialRing(QQ, ['p','q'])
p_var, q_var = R.gens()
I = R.ideal((p_var**2 - 1) * (q_var**2 - 1) - p2q2expr, p_var * q_var - ZZ(N))
print('\nSolving for p and q...')
for solution in I.variety():
    p = solution[p_var]
    q = solution[q_var]
    if p > 0 and q > 0:
        break

assert p.is_integer()
assert q.is_integer()
p = ZZ(p)
q = ZZ(q)
print(f'p = {p}')
print(f'q = {q}')
assert p * q == N
assert (p**2 -1) * (q**2 - 1) == p2q2expr


print('Trying to decrypt')
dp = {}
dq = {}
dp[1] = e.inverse_mod(p - 1)
dp[-1] = e.inverse_mod(p + 1)
dq[1] = e.inverse_mod(q - 1)
dq[-1] = e.inverse_mod(q + 1)
inv_q = p.inverse_mod(q)
inv_p = q.inverse_mod(p)
pre_crt = inv_q
print('finished pre-steps')

ip = sage.all.kronecker((c**2 - 4), p)
iq = sage.all.kronecker((c**2 - 4), q)
print('Got legendre symbols')
rp = v(dp[ip], c, p)
rq = v(dq[iq], c, q)
r = (rp + p*(rq - rp)*pre_crt) % N
print('Got rp, rq, and r')
print(f'rp = {rp}')
print(f'rq = {rq}')
print(f'r = {r}')
tp = c * ZZ(v(e, r, p**2)).inverse_mod(p**2)
assert (tp - 1) % p == 0
tp = (tp - 1) // p
mp = (tp * inv_p) % p
tq = c * ZZ(v(e, r, q**2)).inverse_mod(q**2)
assert (tq - 1) % q == 0
tq = (tq - 1) // q
mq = (tq * inv_q) % q
print('Got mp and mq')
m = (mp + p*(mq - mp)*pre_crt) % N
print(f'\n m = {m}')
flag = int(m).to_bytes(100, 'big').decode().strip()
print(f'Flag:\n{flag}')
#RCTF{eAsy_1uca5_se9uEnce_a6ea27d4177d}


#k = 3031573000062707898030894362725938491041699360277944387594564983294599235354770088455076703590872603970028587461242567727871743567112906403449368368295617
#d = 10094195116105570180837271176023347900714250947119553719973319575439865862403311283068450312080840091183844937565503062268315317458438840517976725273073141
#p = 9116233275131173602739752692268079943189703035300573582575042083296621527313939933344455320687281588438414528000594454794847729159565419506066306192152661
#q = 11183005686209595001928972121695860070092013480977374483129957127854844677143979460482157275466240702921372008061294251388860670887956597721784208073814949
