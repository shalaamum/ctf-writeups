# Summary

We are given a file `output.txt` containing values for `e`, `c`, and `N`,
as well as the script `chall.py` that was used to create it.
The cryptosystem used to encrypt the flag is the one described in the
paper "An efficient probabilistic public-key cryptosystem
over quadratic fields quotients" by Guilhem Castagnos, and the particular
way that `e` is constructed allows us to apply the attack from the paper
"Cryptanalysis of RSA-type cryptosystems based on
Lucas sequences, Gaussian integers and elliptic curves"
by Martin Bunder, Abderrahmane Nitaj, Willy Susilo, Joseph Tonien.
With their method we can recover the factors of `N`, and then it is
just a matter of implementing decryption for this cryptosystem.

Writeup by: shalaamum

Solved by: shalaamum, UnblvR 


# Identifying the cryptosystem and the relevant attack

The main content of `chall.py` is as follows.
```python
def v(k):
    if k == 0:
        return 2
    if k == 1:
        return r
    return (r * v(k - 1) - v(k - 2)) % (N * N)


def encrypt(m, e, N):
    c = (1 + m * N) * v(e) % (N * N)
    return c


p = getPrime(512)
q = getPrime(512)
N = p * q
d = getPrime(512)
r = getPrime(512)
e = inverse(d, (p * p - 1) * (q * q - 1))
c = encrypt(bytes_to_long(flag), e, N)
print(f"e = {e}")
print(f"c = {c}")
print(f"N = {N}")
```

The first thing to identify is what this function `v` is.
The definition looks like a variant of Fibonacci numbers, and
searching around a bit leads to identifying this as Lucas numbers.
Through this the above mentioned two papers were found.
The first describes this cryptosystem based on Lucas numbers, while
the second describes how, using that
`ed - 1 = k*((p * p - 1) * (q * q - 1))`,
we can recover `d` and `k`, as long as
`d < sqrt((2*N^3 - 18*N^2) / e)`.
In our case `N` is `1024` bits, so the numerator in the square
root is roughly `3*1024 + 1` bits. `e` is at most `4*512` bits, so
we take the square root of a number of roughly `1025` bits.
That square root should thus be a bit larger than
`d` which has `512` bits. Thus the method described in the paper
can be applied.


# Recovering d and k

By the mentioned paper, `k/d` is a convergent of the continued
fraction expression of
`e / (N^2 - (9/4)*N + 1)`.
We can thus just go through the denominators of the convergents
until we find one that is both a prime as well as of the right
size. It turns out that there is only one convergent were this is
the case, so this will be the right convergent.
The following code implements this.
```python
Nprime = QQ(N)**2 - QQ(9)/QQ(4)*N + 1
print(f"N' = {Nprime}")
num_for_cont_frac = QQ(e) / Nprime
print(f'Taking the contiued fraction of\n{num_for_cont_frac}\n')
cont_frac = sage.all.continued_fraction(num_for_cont_frac)

k = None
d = None

for i in range(1000):
    denom = cont_frac.denominator(i)
    if not sage.all.is_prime(denom):
        continue
    bit_length = ZZ(denom).nbits()
    if bit_length > (N.nbits() // 2):
        continue
    print(f'{i}th convergent denominator of bit length {bit_length}: {denom}')
    if bit_length == (N.nbits() // 2):
        # just one, very likely the right one
        k = cont_frac.numerator(i)
        d = denom
        break

print(f'\nFound k and d:')
print(f'k = {k}')
print(f'd = {d}')
```


# Recovering p and q

Next we need to use the two equations `N = p * q` and
`ed - 1 = k*((p * p - 1) * (q * q - 1))` to solve for `p` and `q`:
```python
p2q2expr = (e * d - 1) / k
assert p2q2expr.is_integer()
p2q2expr = ZZ(p2q2expr)
print(f'(p^2 - 1) * (q^2 - 1) = {p2q2expr}')

R = sage.all.PolynomialRing(QQ, ['p','q'])
p_var, q_var = R.gens()
I = R.ideal((p_var**2 - 1) * (q_var**2 - 1) - p2q2expr, p_var * q_var - ZZ(N))
print('\nSolving for p and q...')
for solution in I.variety():
    p = solution[p_var]
    q = solution[q_var]
    if p > 0 and q > 0:
        break

assert p.is_integer()
assert q.is_integer()
p = ZZ(p)
q = ZZ(q)
print(f'p = {p}')
print(f'q = {q}')
assert p * q == N
assert (p**2 -1) * (q**2 - 1) == p2q2expr
```


# Decrypting

Finally, now that we have the secret key, we need to decrypt.
As we didn't know an implementation of Castagnos' cryptosystem,
we implemented this based on the pseudocode in the original paper.
For this we needed a more performant implementation of Lucas
numbers mod `N**2` than the recursion based one in `chall.py`.
For this we adapted code found in 
[a writeup for another CTF](https://n00bcak.github.io/writeups/2021/03/27/UMass-CTF.html),
based on square-and-multiply.
Everything is put together in `solve.py`.
