#pylint: disable=line-too-long,trailing-whitespace,invalid-name,missing-module-docstring,missing-function-docstring

import time
import itertools
import multiprocessing
import queue
from magic import Magic
from magic import MagicElement as El

N = 137
magic = Magic(N)
def generator(x): #pylint: disable=useless-return
    U = [(7*(3*i+5)**17+11) % N for i in range(N)] #pylint: disable=redefined-outer-name
    for i in range(x):
        yield U[i % N]
        if N-i % N == 1:
            V = U[:]
            for j in range(N):
                U[j] = V[U[j]]
        i = i + 1
    return

U = [_ for _ in generator(N**2+1)] #pylint: disable=unnecessary-comprehension
index_easy = [True, True, True, True, False, True, True, True, True, True, True, False, True, True, True, True, True, True, False, True, True, True, True, True, False, True, False, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, False, True, True, False, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, False, True, False, True]
# We have 9 difficult indices that all interact and where mix is complicated.
# but there is a sub-quasi-group in the quasigroup:
subgroup = [1, 4, 5, 6]
subgroup_complement = [0, 2, 3, 7]
difficult_indices = []
easy_indices = []
for index_di in range(N):
    if not index_easy[index_di]:
        difficult_indices.append(index_di)
    else:
        easy_indices.append(index_di)
#print(f'Difficult indices: {difficult_indices}')

def solve_right(M, R, correct_solution=None):
    # Returns a solution X for M*X = R
    print(f'Trying to solve M*X = R for X with\nM = {M}\nR = {R}\n\n')

    S = solve_right_easy(M, R)
    print(f'\n\nSolved for the easy components\nS = {S}\n\n')
    LHS = M * S
    for index in easy_indices:
        assert LHS.lst[index] == R.lst[index]
    if correct_solution:
        assert M * correct_solution == R
        for index in easy_indices:
            correct_solution.lst[index] = S.lst[index]
        print('Checking that the modified correct solution with easy what we found still works: ', end='')
        assert M * correct_solution == R
        print('OK')
    
    S_residues = solve_right_get_residues(M, R, S)
    print(f'Found residues for S\n{S_residues}')
    print(f'Checking that the residues fit...')
    S_residues_prev = magic_list_to_residue(S)
    for index in easy_indices:
        assert S_residues[index] == S_residues_prev[index]

    if correct_solution:
        print('Checking that residues fit correct solution: ', end='')
        assert magic_list_to_residue(correct_solution) == S_residues
        print('OK')
        print(f'The solution we should find is: {correct_solution}')
        correct_solution_difficult_str = ''
        for index in difficult_indices:
            correct_solution_difficult_str += str(correct_solution.lst[index])
        print(f'The difficult part of the correct solution is {correct_solution_difficult_str}')

    processes = []
    q = multiprocessing.Queue()
    for first, second in itertools.product(range(4), repeat=2):
        p = multiprocessing.Process(target=bruteforce, args=(first, second, q, M, R, S, S_residues))
        processes.append(p)
        p.start()
    
    status_num = {(fst,snd): 0 for fst,snd in itertools.product(range(4), repeat=2)}
    last_status_time = time.perf_counter()
    start_time = last_status_time
    full_number_per_process = 4**7
    while True:
        if not any([p.is_alive() for p in processes]):
            print(f'Somehow did not find a solution!!!')
            break
        if time.perf_counter() - last_status_time > 1:
            last_status_time = time.perf_counter()
            status_str = f'Elapsed time: {time.perf_counter() - start_time:.1f}s   Status: '
            for first, second in itertools.product(range(4), repeat=2):
                status_str += f'{(100*(status_num[(first, second)] / full_number_per_process)):0.2f}% '
            print(status_str)
        try:
            msg = q.get(timeout=1)
            status_num[(msg['fst'], msg['snd'])] = msg['num']
            if msg['solution'] is not None:
                print('Got solution!')
                S = msg['solution']
                break
        except queue.Empty:
            pass
    
    print(f'ok, solution is: {S}')
    assert M * S == R
    print('The solution should work')
    for p in processes:
        if p.is_alive():
            p.kill()
    print('Terminated other processes.')
    return S

def solve_right_easy(M, R):
    # we need to find S_guess so that mix(P1, S_guess) = RHS_chal 
    # We first do the easy indices.
    S_guess, = M.magic.random_list(1)
    for index in range(N):
        if not index_easy[index]:
            continue
        for guess in range(8):
            # In this component mix(P1, S_guess) is just
            # (P1 + S_guess) + S_guess
            if (M.lst[index] + El(guess)) + El(guess) == R.lst[index]:
                S_guess.lst[index] = El(guess)
                break
    LHS_current_guess = M * S_guess
    where_correct = [LHS_current_guess.lst[i] == R.lst[i] for i in range(N)]
    for index in range(N):
        if index_easy[index]:
            assert where_correct[index]
    print(f'Solved for the easy part: {S_guess}')
    return S_guess

def magic_list_to_residue(lst):
    return [x.value in subgroup for x in lst.lst]

def solve_right_get_residues(M, R, S):
    # S is a partial solution for the easy part
    # returns residues for the whole of S
    
    # This sub-quasi-group is normal.
    # Hence we can first determine the residue class first.
    
    R_residue = magic_list_to_residue(R)
    residue_start_time = time.perf_counter()
    S_difficult_residues = None
    num_ops = 0
    for residues in itertools.product([0,1], repeat=len(difficult_indices)):
        # We interpret 0=in complement, 1=in subgroup.
        # luckily those numbers have that property themselves.
        #print(f'Trying {residues}')
        for residue_index, list_index in enumerate(difficult_indices):
            S.lst[list_index] = El(residues[residue_index])
        num_ops += 1
        LHS_current_guess = M * S
        if magic_list_to_residue(LHS_current_guess) == R_residue:
            residue_time = time.perf_counter() - residue_start_time
            ops_per_second = num_ops / residue_time
            print(f'{num_ops} in {residue_time}s, thus {ops_per_second:.2f}op/s')
            print(f'The difficult residues are {residues}')
            return magic_list_to_residue(S)

# now we need to bruteforce the solution, do this with 16 processes.
def bruteforce(first, second, q, M, R, S, S_residues):
    # Tries to bruteforce the difficult indices, assuming we know
    # the residues, and letting the first and second difficult
    # index be as in the arguments
    # q is a queue to report progress.
    print(f'Starting bruteforce with first two difficult components {first} and {second}')
    num_ops = 0
    for choices_last in itertools.product(range(4), repeat=(len(difficult_indices)-2)):
        choices = [first, second] + list(choices_last)
        choices = [subgroup[choice] if S_residues[difficult_indices[index]] else subgroup_complement[choice]
                for index, choice in enumerate(choices)]
        #print(f'Trying {choices}')
        for choice_index, list_index in enumerate(difficult_indices):
            S.lst[list_index] = El(choices[choice_index])
        num_ops += 1
        LHS_current_guess = M * S
        if LHS_current_guess == R:
            print(f'Found solution {S}!')
            q.put({'fst': first, 'snd': second, 'num': num_ops, 'solution': S})
            return
        if num_ops % 100 == 0:
            q.put({'fst': first, 'snd': second, 'num': num_ops, 'solution': None})
    q.put({'fst': first, 'snd': second, 'num': num_ops, 'solution': None})



