# Summary

The remote of this crypto challenge generates a secret key and gives us the
flag if we manage to sign a specific message for that secret key, using a
custom cryptosystem. Analysis of the cryptosystem reveals severe weaknesses
that reduce the number of tries required for bruteforcing a signature enough
that it is possible to find a correct signature in a couple of minutes.

Writeup by: shalaamum

Solved by: killerdog, shalaamum

# Task setup

We are given two files, `task.py`, and `magic.py`, with `task.py` running remotely.
The main part of `task.py` is as follows.
```python
magic = Magic(137)          # most magical number

C, K, Q = magic.random_list(3)
P1, P2 = C*K, K*Q
pk, sk = (C, P1, P2), (C, K, Q)
print('C =',  pk[0])
print('P1 =', pk[1])
print('P2 =', pk[2])

H = magic.shake(b'Gotta make you understand~')
S = H*Q                     # sign
assert P1*S == C*H*P2       # verify
print('S =', S)

H = magic.shake(b'Never gonna give you flag~')
try:
    S_ = magic(input('> ')[:magic.N])
    if P1*S_ == C*H*P2:
        print(flag)
    else:
        print('Ooh~~~give~you~up~')
except:
    print('You know the rules and so~do~I~')
```
The relevant `H` we can compute locally, and we also know `C`, `P1`, and `P2`,
so in the equation `P1*S_ == C*H*P2` we know everything but `S_`, and need to
solve for `S_`. All of these variables are of the type `MagicList` defined
in `magic.py`, and multiplication is something custom.

# Magic elements

The most basic entities used in this cryptosystem are object of the class
`MagicElement`, which encapsulates an integer from `0` to `7`. The special
thing is that `MagicElement`s are added using a special operation table
given by a Latin square.
```python
Magic_Latin_Square = [[5, 3, 1, 6, 7, 2, 0, 4],
                      [3, 5, 0, 2, 4, 6, 1, 7],
                      [6, 2, 4, 5, 0, 3, 7, 1],
                      [4, 7, 6, 1, 3, 0, 2, 5],
                      [0, 1, 3, 7, 6, 4, 5, 2],
                      [7, 4, 2, 0, 5, 1, 6, 3],
                      [2, 6, 7, 3, 1, 5, 4, 0],
                      [1, 0, 5, 4, 2, 7, 3, 6]]
# [...]
    def __add__(self, other):
        return MagicElement(Magic_Latin_Square[self.value][other.value])
```
We can thus interpret `MagicElement` as an element of a quasigroup of
order 8, see [Wikipedia](https://en.wikipedia.org/wiki/Quasigroup#Latin_squares).
This quasigroup turns out to not have a neutral element and is also not
associative or commutative.

# Magic lists

The `MagicList` class encapsulates lists of `N` (in our case `137`) many `MagicElement`s.
Both multiplication and addition is defined for `MagicList`s.
Addition is just componentwise addition in the quasigroup.
```python
    def __add__(self, other):
        R = []
        for i in range(self.N):
            R.append(self.lst[i] + other.lst[i])
        return MagicList(self.magic, R)
```
Multiplication is more interesting. The product of `self` and `other` is given by `self.mix(self,other)`,
and the `mix` function is defined as follows:
```python
    def mix(self, T, K):
        R = T+K
        e = self.U[0]
        for i in range(self.N ** 2):
            d = self.U[i+1]
            R.lst[d] = R.lst[d] + R.lst[e]
            e = d
        R = R+K
        return R
```
So there are two componentwise additions, but in between we have this loop depending on `self.U`, so we should understand that.

# The generator

`U` only depends on `N`, which in our case is `137`. The function is given as follows.
```python
    def generator(self, x):
        U = [(7*(3*i+5)**17+11) % self.N for i in range(self.N)]
        for i in range(x):
            yield U[i % self.N]
            if self.N-i % self.N == 1:
                V = U[:]
                for j in range(self.N):
                    U[j] = V[U[j]]
            i = i + 1
        return
```
We can interpret this as follows. The first line initializes a `U`, which we will here
also call `Uorig`, which we can interpret as a map from `{0,...,N-1}` to `{0,...,N-1}`.
Then the values `U(0), ..., U(N-1)` get yielded.
Then `U` is updated by composing it with itself, and then `U(0), ..., U(N-1)` is yielded
again, continuing like this as often as necessary.

This means all the values yielded by this generator will be in the image of `Uorig^{n}`
for some `n`. In particular, all values are in `Im(Uorig)`. Luckily for us, `Uorig` is
chosen very badly, its image has only `9` elements!
```
>>> N=137
>>> U = [(7*(3*i+5)**17+11) % N for i in range(N)]
>>> U
[135, 133, 26, 4, 133, 24, 81, 135, 135, 26, 81, 18, 24, 133, 135, 18, 135, 18, 18, 135, 4, 26, 78, 18, 4, 135, 78, 135, 81, 78, 78, 133, 26, 24, 26, 24, 135, 24, 18, 18, 78, 133, 24, 81, 11, 78, 135, 26, 81, 4, 4, 135, 24, 135, 133, 135, 133, 26, 81, 81, 78, 24, 81, 24, 18, 4, 81, 133, 18, 24, 4, 4, 24, 4, 24, 26, 135, 4, 78, 133, 24, 24, 78, 135, 26, 18, 133, 26, 24, 133, 18, 4, 26, 81, 78, 18, 133, 4, 133, 133, 78, 18, 133, 78, 78, 24, 4, 81, 81, 81, 133, 4, 135, 24, 18, 26, 78, 78, 78, 18, 135, 81, 81, 26, 4, 81, 26, 26, 18, 26, 4, 81, 78, 133, 18, 4, 26]
>>> set(U)
{4, 133, 135, 11, 78, 81, 18, 24, 26}
>>> len(set(U))
9
```
We will call the elements of `{4, 133, 135, 11, 78, 81, 18, 24, 26}` the *difficult indices* and
the elements of the complement in `{0,...,136}` the *easy indices*.

# Mixing

So let us get back to the mix function.
```python
    def mix(self, T, K):
        R = T+K
        e = self.U[0]
        for i in range(self.N ** 2):
            d = self.U[i+1]
            R.lst[d] = R.lst[d] + R.lst[e]
            e = d
        R = R+K
        return R
```
The loop in the middle updates some components of the list by mixing in (using the addition of
the quasigroup) another component. However, both of those components are determined by `U`,
so the loop only touches the components at difficult indices! If `i` is an easy index,
then we thus have, with `R` the result:
```
R.lst[i] = (T.lst[i] + K.lst[i]) + K.lst[i]
```
Recall that we need to solve an equation `mix(P1,S)=C*H*P2` for `S`. The above discussion
now means that for easy indices, what works for `S` in that component is independent
of the other components. We can thus just bruteforce this, as there are only `8` different
possible values in each component. In the worst case we need to just calculate
`(137 - 9)*8*2=2048` additions in the quasigroup for this (the factor `2` is because there are
two additions in the above formula).

The remaining `9` difficult components interact with each other. Furthermore, the loop
causes `137**2 = 18769` operations in the quasigroup during mixing. In practice, with
this unoptimized Python implementation, we got around `60` `mix` operations per second
on a single core. There are `8**9 = 134 217 728` different possibilities for the
difficult components, so that would take over `600` hours on a single core in the worst
case, likely too long to be feasible!

# Structure in the quasigroup

To reduce the number of operations necessary for the bruteforce, we now turn
towards the quasigroup and try to find helpful structure in it. A couple of
properties were tried that the quasigroup (let us call it `G`) turned out not
to have, but what was successful was finding a normal sub-quasigroup.

If we let us output what elements of the quasigroup we can reach by beginning
with a specific element of `G` and iteratively adding it to itself (while
taking into account that as `G` is not associative, the order of these
operations matters), we obtain the following.
```
Reachable from 0: [0, 1, 2, 3, 4, 5, 6, 7]
Reachable from 1: [1, 4, 5, 6]
Reachable from 2: [0, 1, 2, 3, 4, 5, 6, 7]
Reachable from 3: [0, 1, 2, 3, 4, 5, 6, 7]
Reachable from 4: [1, 4, 5, 6]
Reachable from 5: [1, 4, 5, 6]
Reachable from 6: [1, 4, 5, 6]
Reachable from 7: [0, 1, 2, 3, 4, 5, 6, 7]
```
This shows that `{1, 4, 5, 6}` is a sub-quasigroup of `G`.
We can check that this subgroup is even normal, i.e. if `N = {1, 4, 5, 6}`, then
`x + N = N + x` for every element `x` of `G`. This is done in `test.py`.
This means that we can form the quotient `G/N` and obtain an
induced well-defined quasigroup structure, which must be the cyclic group of
order `2`, with the neutral element being represented by elements of `N`.

The upshot of this is now, that instead of bruteforcing all the `9**8` possible
combinations for the `9` difficult components, we can first bruteforce the
residues, which only requires in the worst case to try `2**9` combinations.
Once we have this, we have reduced the number of possibilities for each of the
difficult components by half, by knowing whether to look in `N` or its
complement. This means we then only have an additional `4**9` possibilities.
In total that means we only need to carry out `2**9 + 4**9 = 262656` `mix`
operations instead of `8**9`, an improvement by a factor of roughly `511`. This
means that with `60` operations per second (unoptimized Python) this takes 73
minutes in the worst case. Running this in parallel in 16 processes the worst
case search time is reduced to under 5 minutes.

# Solution

The solution suggested above is implemented in `solvelib.py`, with `solve.py`
handling the interaction with the remote.
