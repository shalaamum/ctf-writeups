#!/usr/bin/env python3

import itertools
from magic import MagicElement as El

def reach_by_pow(x):
    if isinstance(x, int):
        x = El(x)
    reachable = [x]
    changed = True
    while changed:
        new_reachable = []
        changed = False
        for y in reachable:
            for z in (x+y, y+x):
                if z not in reachable and z not in new_reachable:
                    changed = True
                    new_reachable.append(z)
        reachable = reachable + new_reachable
    return sorted([y.value for y in reachable])

for x in range(8):
    print(f'Reachable from {x}: {reach_by_pow(x)}')

subgroup = [El(1), El(4), El(5), El(6)]

print('Checking whether the subgroup is normal...')
normal = True
for x in range(8):
    e = El(x)
    left_coset = []
    right_coset = []
    for n in subgroup:
        left_coset.append(e + n)
        right_coset.append(n + e)
    for n in left_coset:
        if n not in right_coset:
            normal = False

if normal:
    print('The subgroup is indeed normal!')

