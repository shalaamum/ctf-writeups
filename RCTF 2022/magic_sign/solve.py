#!/usr/bin/env python3

from magic import Magic
import solvelib
import pwnlib.tubes.remote

N = 137
magic = Magic(N)

rem = pwnlib.tubes.remote.remote("190.92.238.158", 42000)
rem.recvuntil(b'C = ')
C = magic(rem.readline().decode().strip())
rem.recvuntil(b'P1 = ')
P1 = magic(rem.readline().decode().strip())
rem.recvuntil(b'P2 = ')
P2 = magic(rem.readline().decode().strip())
rem.recvuntil(b'> ')
S_c = None

H = magic.shake(b'Never gonna give you flag~')

R = C * H * P2
M = P1 

#print(f'M = {M}')
#print(f'R = {R}')
if S_c is not None:
    print('Checking that the supposed solution actually works: ', end='')
    assert M * S_c == R
    print('OK')


S = solvelib.solve_right(M, R, correct_solution=S_c)
to_send = str(S).encode() + b'\n'
print(f'Sending: {to_send}')
rem.send(to_send)
rem.interactive()
# RCTF{you_w0uldn't_get_th1s_froM_@ny_other_guy}
