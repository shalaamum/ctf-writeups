import public_values_aux
from public_values_aux import *
from sage.schemes.elliptic_curves.hom import compare_via_evaluation

load('castryck_decru_shortcut.sage')

a = 43
b = 26

# Set the prime, finite fields and starting curve
# with known endomorphism
p = 2^a*3^b - 1
public_values_aux.p = p

Fp2.<i> = GF(p^2, modulus=x^2+1)
R.<x> = PolynomialRing(Fp2)

E_start = EllipticCurve(Fp2, [0,0,0,1,0])
print(f'The curve E_start is {E_start}')

# Generation of the endomorphism 2i
i_fp2 = sqrt(Fp2(-1))
#print(f'i_fp2 = {i_fp2}')
def two_i(P):
    x, y = P.xy()
    pt = E_start(-x, i_fp2*y)
    return 2*pt
#print(f'two_i is {two_i}')

P2 = E_start((20816113353953844596827139*i + 16418101434179547435831830 , 9782287231195084940947894*i + 8305288838066432045414923 , 1))
Q2 = E_start((13022786448801065009926908*i + 21396754486749480260181021 , 5027869541156315740937282*i + 8428382255806278677381816 , 1))
P3 = E_start((7582970089792232978539532*i + 6411668474015872447958400 , 15459880436272725660545115*i + 7977012527121440514383975 , 1))
Q3 = E_start((10341548384598782389107676*i + 12525908271709247355078632 , 6555843755802979256565190*i + 11595932163398809254591141 , 1))

check_torsion_points(E_start, a, b, P2, Q2, P3, Q3)

EA = EllipticCurve(Fp2, [(4926878008530427712778566*i+8053083788709808436490360), (18771446501040649196825847*i+16306438728950797793375410)])
PA = EA((2535790352220803985875373*i + 17699033710915047849396921, 2413558249712558899689063*i + 5157954648088691506046995, 1))
QA = EA((16568070039544280994803013*i + 21423138055383385576701886, 5040448698696125071219900*i + 6672798507142407841550817, 1))
#print(f'EA = {EA}')
#print(f'PA = {PA}')
#print(f'QA = {QA}')

EB = EllipticCurve(Fp2, [(18866222948911535725014127*i+21372353382532165741892023), (14780329017962693588095579*i+4731720677310255642021851)])
PB = EB((3413055427164626562463192*i + 5176875496413372729075617, 17919859745180152815219510*i + 18120119720358642060676362, 1))
QB = EB((18433160961475396600407402*i + 22312166252239187097449810, 10433258275941991434154560*i + 9029292514862239326241711, 1))
#print(f'EB = {EB}')
#print(f'PB = {PB}')
#print(f'QB = {QB}')

def RunAttack(num_cores):
    return CastryckDecruAttack(E_start, P2, Q2, EB, PB, QB, two_i, num_cores=num_cores)
  
bobs_key = RunAttack(1)
print(f'Got bobs key: {bobs_key}')
Rb = P3 + bobs_key * Q3
print(f'Rb = {Rb}')
psi_recovered = E_start.isogeny(Rb, algorithm="factored")
#print(f'psi = {psi_recovered}')
J_solv = EA.isogeny(PA + bobs_key*QA, algorithm='factored').codomain().j_invariant()
print(f'J_solv = {J_solv}')
flag_enc = int(243706092945144760206191226817331300960683091878992)
print(f'flac_enc = {flag_enc}')
flag_dec = flag_enc ^^ ((int(J_solv[1]) << 84) + int(J_solv[0])) 
flag = flag_dec.to_bytes(length=100, byteorder="big").decode()
flag = 'RCTF{' + flag + '}'
print(f'flag = {flag}')
# RCTF{SIDH_isBr0ken_in_2O22}
