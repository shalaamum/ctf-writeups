# Summary

This was the Castryck-Decru Key Recovery Attack on
SIDH / SIKE (Supersingular isogeny Diffie–Hellman key exchange).
There is a public sage implementation of the attack, but
as a different starting elliptic curve was used, a small
modification was needed.

Writeup by: shalaamum

Solved by: killerdog, shalaamum


# Setup

We are given a file `s2dh.ipynb` containing a sage notebook
where a supersingular isogeny key exchange is carried out.
The resulting j-invariant is used to encrypt the flag by xoring.
See for example
[the Wikipedia page](https://en.wikipedia.org/wiki/Supersingular_isogeny_key_exchange)
for an overview of this key exchange.

SIDH has been broken recently, in a
[paper by Wouter Castryck and Thomas Decru](https://eprint.iacr.org/2022/975.pdf).


# The Castryck-Decru attack

There is a public sage implementation of the Castryck-Decru attack,
available in
[this Github repository](https://github.com/jack4818/Castryck-Decru-SageMath).
There is also an accompanying
[paper](https://eprint.iacr.org/2022/1283.pdf).

We started with the example script `baby_SIDH.sage` from the repo
and modified it as needed for our case.


# The endomorphism 2i

The attack relies on an endomorphism `2i` of the starting
elliptic curve `E_start`. This property this endomorphism
is to have is that `(2i)^2 = -4` (that is, the morphism
that maps every point to its scalar multiple with `-4`).
See [Section 2.4](https://eprint.iacr.org/2022/1283.pdf) and
[Section 3.1](https://eprint.iacr.org/2022/975.pdf).
While the rest of the relevant code appears to work in greater generality, 
`2i` is only implemented for the special start elliptic curve
`y^2 = x^3 + 6x^2 + x`.
However, in our case the elliptic curve is `y^2 = x^3 + x`.
So we have to implement the morphism `2i`.
Luckily, the
[Castryck-Decru paper](https://eprint.iacr.org/2022/975.pdf)
contains a formula for this morphism for this elliptic curve
as well, so we can implement `2i` as follows.
```python
E_start = EllipticCurve(Fp2, [0,0,0,1,0])
i_fp2 = sqrt(Fp2(-1))
def two_i(P):
    x, y = P.xy()
    pt = E_start(-x, i_fp2*y)
    return 2*pt
```


# Putting everything together

With the various constants changed as necessary and `2i` implemented,
we can run the sage implementation of the Castryck-Decru attack
to recover Bob's key. Then it is just the matter of calculating
the shared secret j-invariant from Bob's key, and decrypting the
flag with it. All of this is done in the sage script `solve.sage`,
which should be placed in the top directory of the checked out
[repository](https://github.com/jack4818/Castryck-Decru-SageMath).
