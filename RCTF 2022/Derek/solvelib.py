from ctypes import c_uint64
import random

def multiply_GF264(x,y):
    smaller, bigger = x, y
    if smaller > bigger:
        smaller, bigger = bigger, smaller
    result = 0
    while smaller > 0:
        if smaller & 1 != 0:
            result ^= bigger
        carry = 0
        if bigger & 2**63:
            carry = 0x1b
        bigger = (bigger << 1) % (2**64)
        bigger ^= carry
        smaller = smaller >> 1
    return result

gf264_inverse_0x100 = 0xc70000000000000b
gf264_inverse_0x1b = 0x71c71c71c71c71c3
assert multiply_GF264(gf264_inverse_0x100, 2**8) == 1
assert multiply_GF264(gf264_inverse_0x1b, 0x1b) == 1

def l_to_t_obfuscated(l):
    magic = c_uint64(0xffffffffffffffff)
    for m in bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
                    for byte in l.to_bytes(8, 'big')]):
        magic.value ^= c_uint64(m << 56).value
        for j in range(8):
            if magic.value & 0x8000000000000000 != 0:
                magic.value = magic.value << 1 ^ 0x1b
            else:
                magic.value = magic.value << 1
    magic.value ^= 0xffffffffffffffff
    t = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in bytes(magic)])
    return int(t.hex(), 16)

def l_to_t(l):
    #print(f'l as bytes: {l.to_bytes(8, "big")}')
    l_reversed = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in l.to_bytes(8, "big")])
    #print(f'l reversed: {l_reversed}')
    l_reversed = int.from_bytes(l_reversed, "big")
    #print(f'l reversed as int: {l_reversed}')
    magic = c_uint64(multiply_GF264(l_reversed, 0x1b) ^ multiply_GF264(0xffffffffffffffff, 0x1b) ^ 0xffffffffffffffff)
    #print(f'last magic: {magic}')
    #print(f'last magic as bytes: {bytes(magic)}')
    t = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in bytes(magic)])
    #print(f'As bytes: {t}')
    return int(t.hex(), 16)

def t_to_l(t):
    t = t.to_bytes(8,"big")
    #print(f'As bytes: {t}')
    magic = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in t])
    magic = c_uint64(int.from_bytes(magic, "little"))
    #print(f'last magic: {magic}')
    magic = magic.value ^ multiply_GF264(0xffffffffffffffff, 0x1b) ^ 0xffffffffffffffff
    l_reversed = multiply_GF264(magic, gf264_inverse_0x1b)
    #print(f'l reversed: {l_reversed}')
    l_reversed = l_reversed.to_bytes(8, "big")
    #print(f'l reversed: {l_reversed}')
    l = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in l_reversed])
    #print(f'l: {l}')
    l = int.from_bytes(l, "big")
    return l

for i in range(100):
    test_value = random.randrange(0, 2**64)
    assert l_to_t_obfuscated(test_value) == l_to_t(test_value)
    assert l_to_t(t_to_l(test_value)) == test_value
    assert t_to_l(l_to_t(test_value)) == test_value

def construct_special_cleartext():
    l = t_to_l(0xbaadf00ddeadbeef)
    r = t_to_l(0xdeadbeefbaadf00d)
    l = l.to_bytes(8,"big")
    r = r.to_bytes(8,"big")
    return l+r

def recover_key(ciphertext):
    # only need first block
    ciphertext = ciphertext[:16]
    ciphertext = int.from_bytes(ciphertext, "big")
    ciphertext ^= int.from_bytes(construct_special_cleartext(), "big")

    print(f'ciphertext = {hex(ciphertext)}')
    l = ciphertext // (2**(8*8))
    r = ciphertext % (2**(8*8))
    print(f'l = {hex(l)}')
    print(f'r = {hex(r)}')
    key = l.to_bytes(8, "big") + r.to_bytes(8, "big")
    return key

