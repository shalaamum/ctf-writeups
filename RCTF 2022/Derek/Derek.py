from LFSR import LFSR
from ctypes import c_uint64
from util import aes, nsplit
from Cryptodome.Util.Padding import pad
from solvelib import l_to_t

class Derek():
    def __init__(self, key, rnd=10):
        self.key = key
        self.rnd = rnd
        self.keys = list()
        self.generatekeys(self.key)

    def generatekeys(self, key: bytes) -> None:
        lfsr = LFSR(int.from_bytes(key, 'big'))
        for i in range(self.rnd):
            b = 0
            for j in range(128):
                b = (b << 1) + lfsr.next()
            self.keys.append(b.to_bytes(16, 'big'))

    def enc_block(self, x: int) -> int:
        x_bin = bin(x)[2:].rjust(128, '0') # 0's added to the left
        l, r = int(x_bin[:64], 2), int(x_bin[64:], 2)
        # l is most significant half of x, r least
        for i in range(self.rnd):
            magic = c_uint64(0xffffffffffffffff)
            for m in bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
                            for byte in l.to_bytes(8, 'big')]):
                # we go through each of the bytes of l,
                # starting with the most significant one.
                # m is then that byte, but with the order of the bits *reversed*,
                # as in the most significant (bit 7) is now the least
                # significant (bit 0)
                magic.value ^= c_uint64(m << 56).value
                # we xored the highest byte of magic with m
                for j in range(8):
                    if magic.value & 0x8000000000000000 != 0:
                        magic.value = magic.value << 1 ^ 0x1b
                    else:
                        magic.value = magic.value << 1
                # we multiply magic by 2, but do this in
                # GF(2**64) probably, so carry wraps around by
                # xoring with 0x1b
            magic.value ^= 0xffffffffffffffff
            # fliped all bits of magic
            t = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
                      for byte in bytes(magic)])
            # t is like magic, but in each of the bytes the order of the bits
            # has been reversed
            #print(f'Derek-enc round {i}, t = {hex(int.from_bytes(t, "big"))}')
            t = aes(int(t.hex(), 16), self.keys[i]) & 0xffffffffffffffff
            # encrypted t with aes using the key for this round
            t ^= aes(0xdeadbeefbaadf00d if i % 2 else 0xbaadf00ddeadbeef,
                     self.keys[i]) & 0xffffffffffffffff
            # xored with something else, why not
            #print(f'Derek-enc round {i}, xor with = {hex(t)}')
            #print(f'Derek-enc round {i}, l = {hex(l)}')
            #print(f'Derek-enc round {i}, r = {hex(r)}')
            l, r = r ^ t, l
        l ^= int.from_bytes(self.key[:8], 'big')
        r ^= int.from_bytes(self.key[8:], 'big')
        # Here we xored with the original key (not the round keys)
        l, r = r, l
        y = (l + (r << 64)) & 0xffffffffffffffffffffffffffffffff
        return y

    def dec_block(self, x: int) -> int:
        x_bin = bin(x)[2:].rjust(128, '0') # 0's added to the left
        l, r = int(x_bin[:64], 2), int(x_bin[64:], 2)
        l ^= int.from_bytes(self.key[:8], 'big')
        r ^= int.from_bytes(self.key[8:], 'big')
        for i in range(self.rnd-1, -1, -1):
            # so we have
            # l, r = r_prev ^ t, l_prev
            # so
            l_prev = r
            t = l_to_t(l_prev).to_bytes(8, "big")
            t = aes(int(t.hex(), 16), self.keys[i]) & 0xffffffffffffffff
            # encrypted t with aes using the key for this round
            t ^= aes(0xdeadbeefbaadf00d if i % 2 else 0xbaadf00ddeadbeef,
                     self.keys[i]) & 0xffffffffffffffff
            r_prev = l ^ t
            l, r = l_prev, r_prev
        y = (r + (l << 64)) & 0xffffffffffffffffffffffffffffffff
        return y

    def encrypt(self, text: bytes) -> bytes:
        #print(f'Encrypting {text.hex()}')
        text_blocks = nsplit(pad(text, 16), 16)
        result = b''
        for block in text_blocks:
            block = int.from_bytes(block, 'big')
            result += self.enc_block(block).to_bytes(16, 'big')
        return result

    def decrypt(self, text: bytes) -> bytes:
        cipher_blocks = nsplit(text, 16)
        result = b''
        for block in cipher_blocks:
            block = int.from_bytes(block, "big")
            result += self.dec_block(block).to_bytes(16, "big")
        # TODO: perhaps undo padding
        return result
