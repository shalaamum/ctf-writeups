#!/usr/bin/env python3

import os
from ctypes import c_uint64
from Derek import Derek
from solvelib import multiply_GF264, gf264_inverse_0x100, gf264_inverse_0x1b, l_to_t, t_to_l, construct_special_cleartext, recover_key
from Cryptodome.Util.number import long_to_bytes
from pwn import *


rem = remote("94.74.90.243", 42000)
#rem = process('./taskc.py')
data = rem.recvuntil(b'> ')
print(data.decode())

cleartext = construct_special_cleartext()
print(f'Encrypting {cleartext} of length {len(cleartext)}')
print(f'cleartext = {cleartext.hex()}')
rem.send(b'E\n')
rem.recvuntil(b'> ')
rem.send(cleartext.hex().encode() + b'\n')
ciphertext = rem.recvuntil(b'\n', drop=True)
rem.recvuntil(b'> ')
print(f'Received {ciphertext}')
ciphertext = long_to_bytes(int(ciphertext, 16))
recovered_key = recover_key(ciphertext)
print(f'rec key = {recovered_key.hex()}')

print(f'Getting flag...')
rem.send(b'G\n')
flag_ciphertext = rem.recvuntil(b'\n', drop=True)
print(f'Received: {flag_ciphertext}')
flag_ciphertext = long_to_bytes(int(flag_ciphertext, 16))

derek = Derek(recovered_key, rnd=42)
print(derek.decrypt(flag_ciphertext))
# RCTF{3asy_backd0or_wiTh_CRC_r3ver3s1ng}
