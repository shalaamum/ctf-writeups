# Summary

As the description says, this is a homemade Feistel cipher.
We get the encrypted flag and can let the remote encrypt
our cleartexts with the same key. A carefully chosen
cleartext allows us to recover the key, with which we can
then decrypt the flag.

Writeup by: shalaamum

Solved by: killerdog, Lance Roy, shalaamum

# Task

We are given four files. `util.py` contains utility functions
that are not relevant here. `LFSR.py` contains the code used
to generate the round keys of the Feistel cipher from the key,
but this did not play a role in the solution presented here.
Then there is `task.py`, which is the program we interact
with on the remote, containing the following.
```python
key = os.urandom(16)
derek = Derek(key, rnd=42)
while True:
    print(
        '| Option:\n|\t[E]ncrypt\n|\t[D]ecrypt\n|\t[G]et encrypted flag\n|\t[Q]uit')
    option = input('> ')
    if option.lower() == 'e':
        print(derek.encrypt(bytes.fromhex(
            (input('msg you want to encrypt (in hex) > ')))).hex())
    elif option.lower() == 'd':
        print('unimplement')
    elif option.lower() == 'g':
        print(derek.encrypt(flag).hex())
    else:
        exit()
```
We can thus obtain the flag encrypted using the homemade "Derek" cryptosystem
with a 128 bit key and with 42 rounds. We can also encrypt our own cleartext.
Finally, `Derek.py` implements the Derek cryptosystem. Here we renamed
`Derek.py` to `Derek_original.py`, as we will change `Derek.py`.

# Derek

The relevant part here is really only the function that encrypts a single block,
as we are not going to attack the key scheduling.
That function is reproduced below.
```python
    def enc_block(self, x: int) -> int:
        x_bin = bin(x)[2:].rjust(128, '0')
        l, r = int(x_bin[:64], 2), int(x_bin[64:], 2)
        for i in range(self.rnd):
            magic = c_uint64(0xffffffffffffffff)
            for m in bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
                            for byte in l.to_bytes(8, 'big')]):
                magic.value ^= c_uint64(m << 56).value
                for j in range(8):
                    if magic.value & 0x8000000000000000 != 0:
                        magic.value = magic.value << 1 ^ 0x1b
                    else:
                        magic.value = magic.value << 1
            magic.value ^= 0xffffffffffffffff
            t = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
                      for byte in bytes(magic)])
            t = aes(int(t.hex(), 16), self.keys[i]) & 0xffffffffffffffff
            t ^= aes(0xdeadbeefbaadf00d if i % 2 else 0xbaadf00ddeadbeef,
                     self.keys[i]) & 0xffffffffffffffff
            l, r = r ^ t, l
        l ^= int.from_bytes(self.key[:8], 'big')
        r ^= int.from_bytes(self.key[8:], 'big')
        l, r = r, l
        y = (l + (r << 64)) & 0xffffffffffffffffffffffffffffffff
        return y
```
As usual for Feistel ciphers, the cleartext is split into two parts,
`l` and `r`.
In each round, a value `t` is then calculated out of `l`, and then
`l` and `r` are updated as follows.
```
l, r = (r ^ aes(t, round_key) ^ aes(constant, round_key)), l
```
At the very end, the result is xored with the key.

Note that we could refactor this code using a `l_to_t` function and write
it as
```python
    def enc_block(self, x: int) -> int:
        x_bin = bin(x)[2:].rjust(128, '0')
        l, r = int(x_bin[:64], 2), int(x_bin[64:], 2)
        for i in range(self.rnd):
            t = l_to_t(l)
            t = aes(int(t.hex(), 16), self.keys[i]) & 0xffffffffffffffff
            t ^= aes(0xdeadbeefbaadf00d if i % 2 else 0xbaadf00ddeadbeef,
                     self.keys[i]) & 0xffffffffffffffff
            l, r = r ^ t, l
        l ^= int.from_bytes(self.key[:8], 'big')
        r ^= int.from_bytes(self.key[8:], 'big')
        l, r = r, l
        y = (l + (r << 64)) & 0xffffffffffffffffffffffffffffffff
        return y
```

The crucial observation now is that the round key appears twice here
as the key of the two AES operations. If we can arrange that
`t=0xdeadbeefbaadf00d` on even rounds and
`t=0xbaadf00ddeadbeef` on odd rounds, then we we will calculate
AES of the same cleartext twice and xor that together, getting `0`.
Hence the round will just swap `l` and `r`.
With such carefully chosen initial `l` and `r` we can thus ensure
that the each pair of successive rounds are the identity, so
the `42` rounds in total do not change `l` and `r`.
As `l` and `r` are xored at the end with half of the key each,
we can thus recover the key from the ciphertext returned from the remote.
To implement this idea we will have to find an inverse to `l_to_t`.

# The bijection converting l to t

We have to understand the following code:
```python
def l_to_t_obfuscated(l):
    magic = c_uint64(0xffffffffffffffff)
    for m in bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
                    for byte in l.to_bytes(8, 'big')]):
        magic.value ^= c_uint64(m << 56).value
        for j in range(8):
            if magic.value & 0x8000000000000000 != 0:
                magic.value = magic.value << 1 ^ 0x1b
            else:
                magic.value = magic.value << 1
    magic.value ^= 0xffffffffffffffff
    t = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in bytes(magic)])
    return int(t.hex(), 16)
```
The part at the end obtains `t` from `magic` by just reversing
the order of the bits in each byte, so let us focus on
`magic`.
Let us begin by understanding the inner loop
```python
        for j in range(8):
            if magic.value & 0x8000000000000000 != 0:
                magic.value = magic.value << 1 ^ 0x1b
            else:
                magic.value = magic.value << 1
```
This looks like a multiplication, but with carry in bit
`64` wrapping around leading to a xor with `0x1b`.
The angle to interpret this best is to consider binary numbers
as encoding elements of the polynomial ring
`𝔽₂[x]`, with the least significant bit corresponding to `1`,
the next bit corresponding to `x`, etc..
The number `2**64 - 0x1b` then corresponds to an irreducible
polynomial of degree `64`, so that the quotient ring by
that polynomial is the field `GF(2^64)`.
Interpreting `64` bit binary numbers as elements of this field
in the manner just discussed, we can then see that the
above loop is corresponds to multiplying `magic` by `x**8`.

Xoring corresponds to addition in the field, and multiplication
distributes over addition. We can thus consider
what happens with the initial value as well as all the
bytes of `l` separately, and then add them up at the end.
Before `l` is used the bits in the bytes are actually reversed,
and in the paragraph below `l` refers to this new `l`.

The initial value `0xffffffffffffffff` is multiplied by `x**8`
exactly `8` times, so is multiplied by `x**64`, which however
in `GF(2^64)` is the same as multiplying with the element
corresponding to `0x1b`.
The byte of `l` that is leftmost, so highest significance,
is first shifted up to its previous position, and then added,
before the multiplication with `x**8` happens.
So it will also be multiplied with `x**8` exactly `8` times,
hence it will also be in total multiplied by the element
corresponding to `0x1b`.
The next byte of `l` will be shifted by too much, so the
shift before xoring into `magic` already corresponds
to a multiplication by `x**8`, making up for the fact
that there are now only `7` passes of the loop left, so
that this byte still ends up being multiplied by `x**8`
exactly `8` times. The argument extends to the other bytes
of `l` as well.

The upshot is that we can reimplement `l_to_t` as follows.
```python
def l_to_t(l):
    l_reversed = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in l.to_bytes(8, "big")])
    l_reversed = int.from_bytes(l_reversed, "big")
    magic = c_uint64(multiply_GF264(l_reversed, 0x1b) ^ multiply_GF264(0xffffffffffffffff, 0x1b) ^ 0xffffffffffffffff)
    t = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in bytes(magic)])
    return int(t.hex(), 16)
```

This has been done in `solvelib.py`.

We also require the inverse `t_to_l`, which can be implemented as follows:
```python
def t_to_l(t):
    t = t.to_bytes(8,"big")
    magic = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in t])
    magic = c_uint64(int.from_bytes(magic, "little"))
    magic = magic.value ^ multiply_GF264(0xffffffffffffffff, 0x1b) ^ 0xffffffffffffffff
    l_reversed = multiply_GF264(magic, gf264_inverse_0x1b)
    l_reversed = l_reversed.to_bytes(8, "big")
    l = bytes([int(bin(byte)[2::].zfill(8)[8::-1], 2)
              for byte in l_reversed])
    l = int.from_bytes(l, "big")
    return l
```
The only possible difficulty here is that we need to invert multiplication by the
element corresponding to `0x1b`. This can be done in sage as follows.
```
┌────────────────────────────────────────────────────────────────────┐
│ SageMath version 9.5, Release Date: 2022-01-30                     │
│ Using Python 3.10.8. Type "help()" for help.                       │
└────────────────────────────────────────────────────────────────────┘
sage: F = FiniteField(2)
sage: P.<x> = F[]
sage: bin(0x1b)
'0b11011'
sage: C = 1 + x + x**3 + x**4
sage: S.<xbar> = P.quotient(x**64 - C)
sage: hex((xbar**(-64)).lift().change_ring(ZZ)(x=2))
'0x71c71c71c71c71c3'
```

# Key recovery

If we construct a cleartext as follows
```python
def construct_special_cleartext():
    l = t_to_l(0xbaadf00ddeadbeef)
    r = t_to_l(0xdeadbeefbaadf00d)
    l = l.to_bytes(8,"big")
    r = r.to_bytes(8,"big")
    return l+r
```
we will now, as discussed before, get that the rounds of the Feistel cipher
only swap `l` and `r`. As encryption finishes by xoring with the key,
we can thus recover it from the ciphertext obtained from this special
cleartext as follows.
```python
def recover_key(ciphertext):
    # only need first block
    ciphertext = ciphertext[:16]
    ciphertext = int.from_bytes(ciphertext, "big")
    ciphertext ^= int.from_bytes(construct_special_cleartext(), "big")

    l = ciphertext // (2**(8*8))
    r = ciphertext % (2**(8*8))
    key = l.to_bytes(8, "big") + r.to_bytes(8, "big")
    return key
```

# Putting everything together

Decryption was not implemented in `Derek.py` that we were provided,
so this had to be added. As the flag can also be read off without
stripping and/or verifying the padding, this was left out.
The script `solve.py` interacts with the remote and mostly uses
the decryption function from `Derek.py` as well as the various
functions from `solvelib.py` that were discussed in the preceding
sections.
