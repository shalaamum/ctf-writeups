## Summary

This is a writeup for the "Fast calculator" challenge by Minh at [UIUCTF 2023](https://2023.uiuc.tf), where I participated as part of [Kalmarunionen](https://www.kalmarunionen.dk/). There were 36 solves in total for this challenge, which involved reversing a C binary and fixing incorrectly implemented functions that were used to decrypt the flag.

Solved by: shalaamum

Writeup by: shalaamum


## Running the binary

We are given an ELF executable `calc` (find the file [here](./calc)), that seems to implement some kind of simple calculator, but promises to decrypt the flag if we enter a "secret operation":
```
% ./calc
Welcome to the fastest, most optimized calculator ever!
Example usage:
  Add:       1 + 2
  Subtract:  10 - 24
  Multiply:  34 * 8
  Divide:    20 / 3
  Modulo:    60 % 9
  Exponent:  2 ^ 12

If you enter the correct secret operation, I might decrypt the flag for you! ^-^

Enter your operation: 42*47
Result: 1974.000000
Enter your operation:
```


## Finding the secret operation

To find out what the secret operation might be, we can open the binary in Ghidra. Symbols are not stripped, so we can go to the `main` function where we find the following code (I removed some parts that are not relevant at the moment):
```c
  puts("If you enter the correct secret operation, I might decrypt the flag for you! ^-^\n");
  do {
    while( true ) {
      // ...
      printf("Enter your operation: ");
      // ...
    }
    // ...
    dVar10 = (double)calculate();
    local_2330 = dVar10;
    // ...
    printf("Result: %lf\n",dVar10);
    if (local_2330 == 8573.8567) {
      // ...
      puts("\nCorrect! Attempting to decrypt the flag...");
      // ...
      puts("Here is your decrypted flag:\n");
      // ...
    }
  } while( true );
```
From this we can see there is a `calculate` function that seems to be used to calculate the result of the operation we input (and just judging from a couple of example inputs to the `calc` binary, the result seems to be correct). If the result is `8573.8567`, then we enter a branch that contains printouts that sound promising. So let us try entering the operation `8573.8567*1`:
```
% ./calc
Welcome to the fastest, most optimized calculator ever!
Example usage:
  Add:       1 + 2
  Subtract:  10 - 24
  Multiply:  34 * 8
  Divide:    20 / 3
  Modulo:    60 % 9
  Exponent:  2 ^ 12

If you enter the correct secret operation, I might decrypt the flag for you! ^-^

Enter your operation: 8573.8567*1
Result: 8573.856700

Correct! Attempting to decrypt the flag...
I calculated 368 operations, tested each result in the gauntlet, and flipped 119 bits in the encrypted flag!
Here is your decrypted flag:

uiuctf{This is a fake flag. You are too fast!}
```
We get something that has the correct format for a flag, but claims to be fake, and the CTFd instance agreed that this was not the correct flag.


## Analyzing how the flag gets decrypted

Let us look more closely at the code that claims to decrypt the flag. The code below is how it looks like out of the box in Ghidra, then I will go through individual snippets, and change variable names, etc. as we go along.
```c
puts("\nCorrect! Attempting to decrypt the flag...");
local_2328 = (long)local_2360 + -1;
uVar5 = (((long)local_2360 + 0xfU) / 0x10) * 0x10;
for (puVar8 = (undefined *)((long)puVar7 + -0xb80);
    puVar8 != (undefined *)((long)puVar7 + (-0xb80 - (uVar5 & 0xfffffffffffff000)));
    puVar8 = puVar8 + -0x1000) {
  *(undefined8 *)(puVar8 + -8) = *(undefined8 *)(puVar8 + -8);
}
lVar1 = -(ulong)((uint)uVar5 & 0xfff);
local_2320 = puVar8 + lVar1;
if ((uVar5 & 0xfff) != 0) {
  *(undefined8 *)(puVar8 + ((ulong)((uint)uVar5 & 0xfff) - 8) + lVar1) =
       *(undefined8 *)(puVar8 + ((ulong)((uint)uVar5 & 0xfff) - 8) + lVar1);
}
__n = (size_t)local_2360;
*(undefined8 *)(puVar8 + lVar1 + -8) = 0x402051;
memcpy(puVar8 + lVar1,&local_78,__n);
local_2368 = 0;
for (local_2364 = 0; local_2364 < (int)local_235c; local_2364 = local_2364 + 1) {
  lVar6 = (long)local_2364;
  *(undefined8 *)(puVar8 + lVar1 + -0x10) = local_22f8[lVar6 * 3 + 2];
  *(undefined8 *)(puVar8 + lVar1 + -0x18) = local_22f8[lVar6 * 3 + 1];
  *(undefined8 *)(puVar8 + lVar1 + -0x20) = local_22f8[lVar6 * 3];
  *(undefined8 *)(puVar8 + lVar1 + -0x28) = 0x40209e;
  uVar11 = calculate();
  *(undefined8 *)(puVar8 + lVar1 + -8) = 0x4020b1;
  cVar3 = gauntlet(uVar11);
  if (cVar3 != '\0') {
    local_2358 = local_2364;
    if (local_2364 < 0) {
      local_2358 = local_2364 + 7;
    }
    local_2358 = local_2358 >> 3;
    local_2354 = local_2364 % 8;
    local_2320[local_2358] =
         local_2320[local_2358] ^ (byte)(1 << (7U - (char)(local_2364 % 8) & 0x1f));
    local_2368 = local_2368 + 1;
  }
}
uVar5 = (ulong)local_2368;
uVar9 = (ulong)local_235c;
*(undefined8 *)(puVar8 + lVar1 + -8) = 0x402164;
printf("I calculated %d operations, tested each result in the gauntlet, and flipped %d bits in  the encrypted flag!\n"
       ,uVar9,uVar5);
*(undefined8 *)(puVar8 + lVar1 + -8) = 0x402173;
puts("Here is your decrypted flag:\n");
puVar2 = local_2320;
*(undefined8 *)(puVar8 + lVar1 + -8) = 0x402191;
printf("%s\n\n",puVar2);
```

Skimming this, we can get a rough overview over the different parts. Let us start at the very end, because that is where the flag is supposedly printed, which is what interests us:
```c
num_bitflips_ = (ulong)num_bitflips;
num_operations_ = (ulong)num_operations;
*(undefined8 *)(puVar6 + lVar1 + -8) = 0x402164;
printf("I calculated %d operations, tested each result in the gauntlet, and flipped %d bits in  the encrypted flag!\n"
       ,num_operations_,num_bitflips_);
*(undefined8 *)(puVar6 + lVar1 + -8) = 0x402173;
puts("Here is your decrypted flag:\n");
flag_ = flag;
*(undefined8 *)(puVar6 + lVar1 + -8) = 0x402191;
printf("%s\n\n",flag_);
```
The `printf`s indicate that the pointer I thus renamed `flag` contains the decrypted flag, and that it was obtained by "testing" some results and then flipping bits of the encrypted flag. I renamed and retyped the variables used for this to fit with the meaning indicated by the printout.

Let us now go through the other snippets from top to bottom.

```c
*(undefined8 *)((long)puVar5 + -0xb88) = 0x401f88;
puts("\nCorrect! Attempting to decrypt the flag...");
local_2328 = (long)local_2360 + -1;
num_bitflips_ = (((long)local_2360 + 0xfU) / 0x10) * 0x10;
for (puVar6 = (undefined *)((long)puVar5 + -0xb80);
    puVar6 != (undefined *)((long)puVar5 + (-0xb80 - (num_bitflips_ & 0xfffffffffffff000)));
    puVar6 = puVar6 + -0x1000) {
  *(undefined8 *)(puVar6 + -8) = *(undefined8 *)(puVar6 + -8);
}
lVar1 = -(ulong)((uint)num_bitflips_ & 0xfff);
flag = puVar6 + lVar1;
```
This first part seems to somehow calculate an address used for the `flag` buffer. What exactly the code before the last line in this snippet does did not turn out to be relevant to solving this challenge, so I never really looked at it.

```c
if ((num_bitflips_ & 0xfff) != 0) {
  *(undefined8 *)(puVar6 + ((ulong)((uint)num_bitflips_ & 0xfff) - 8) + lVar1) =
       *(undefined8 *)(puVar6 + ((ulong)((uint)num_bitflips_ & 0xfff) - 8) + lVar1);
}
__n = (size_t)local_2360;
*(undefined8 *)(puVar6 + lVar1 + -8) = 0x402051;
memcpy(puVar6 + lVar1,&local_78,__n);
```
This snippet copies some data to the `flag` buffer. The decryption seems happens in place, by bit-flipping as the output of the binary suggest (and as we will see confirmed in the next snippet), so the encrypted flag seems to be contained in `local_78`. The top of the main function defines this variable (as well as adjacent ones):
```c
local_78 = 0x10eeb90001e1c34b;
local_70 = 0xcb382178a4f04bee;
local_68 = 0xe84683ce6b212aea;
local_60 = 0xa0f5cf092c8ca741;
local_58 = 0x20a92860082772a1;
local_50 = 0x35abb366;
local_4c = 0xe9a4;
```
This data is thus likely the encrypted flag.

The remaining snippet is a loop that seems to do the actual decryption by flipping bits:
```c
num_bitflips = 0;
for (local_2364 = 0; local_2364 < (int)num_operations; local_2364 = local_2364 + 1) {
  lVar4 = (long)local_2364;
  *(undefined8 *)(puVar6 + lVar1 + -0x10) = local_22f8[lVar4 * 3 + 2];
  *(undefined8 *)(puVar6 + lVar1 + -0x18) = local_22f8[lVar4 * 3 + 1];
  *(undefined8 *)(puVar6 + lVar1 + -0x20) = local_22f8[lVar4 * 3];
  *(undefined8 *)(puVar6 + lVar1 + -0x28) = 0x40209e;
  uVar8 = calculate();
  *(undefined8 *)(puVar6 + lVar1 + -8) = 0x4020b1;
  cVar2 = gauntlet(uVar8);
  if (cVar2 != '\0') {
    local_2358 = local_2364;
    if (local_2364 < 0) {
      local_2358 = local_2364 + 7;
    }
    local_2358 = local_2358 >> 3;
    local_2354 = local_2364 % 8;
    flag[local_2358] = flag[local_2358] ^ (byte)(1 << (7U - (char)(local_2364 % 8) & 0x1f));
    num_bitflips = num_bitflips + 1;
  }
}
```
Going backwards, we can see near the end that a bit of `flag` is flipped if `cVar2` is nonzero. This seems to be the return value of a function `gauntlet`, that is called using the return value of the function `calculate` that were already identified as being the function that calculates the result of a binary operation on two floating point numbers, as this was used in the main loop in `main` as well. However, the decompile here is somewhat unsatisfactory, because we can't see what the arguments to `calculate` are. Before analyzing this crucial decryption loop further we should thus have a look at how arguments are passed to `calculate`, and what `gauntlet` does.


## Parameters of `calculate`

Out of the box Ghidra presents us with the following decompile for `calculate`:
```c
double calculate(void)

{
  uint param_7;
  double param_8;
  double param_9;
  double local_10;
  
  local_10 = 0.0;
  if (param_7 < 0x30) {
    if ((0x24 < param_7) && (true)) {
      switch(param_7) {
      case 0x25:
        local_10 = fmod(param_8,param_9);
        break;
      case 0x2a:
        local_10 = param_9 * param_8;
        break;
      case 0x2b:
        local_10 = param_9 + param_8;
        break;
      case 0x2d:
        local_10 = param_8 - param_9;
        break;
      case 0x2f:
        local_10 = param_8 / param_9;
      }
    }
  }
  else if (param_7 == 0x5e) {
    local_10 = powf64(param_8,param_9);
  }
  return local_10;
}
```
Clearly `param_8` and `param_9` are the two floating point arguments, so I renamed them `operand_1` and `operand_2`, and `param_7` is the operation, which should be a char judging from the cases, renamed `operation`.

Evidently `operation`, `operand_1`, and `operand_2` must be passed as arguments to `calculate`, yet Ghidra did not detect this. The listing view shows us where these variables are stored:
```
                **************************************************************
                *                          FUNCTION                          *
                **************************************************************
                undefined calculate(undefined param_1, undefined param_2
undefined         AL:1           <RETURN>
undefined         DIL:1          param_1
undefined         SIL:1          param_2
undefined         DL:1           param_3
undefined         CL:1           param_4
undefined         R8B:1          param_5
undefined         R9B:1          param_6
undefined8        Stack[0x18]:8  operand_2               XREF[6]:     004021fd(R), 
                                                                      00402215(R), 
                                                                      0040222a(R), 
                                                                      0040223f(R), 
                                                                      0040224f(R), 
                                                                      00402271(R)  
undefined8        Stack[0x10]:8  operand_1               XREF[6]:     004021f8(R), 
                                                                      00402210(R), 
                                                                      00402225(R), 
                                                                      0040223a(R), 
                                                                      00402254(R), 
                                                                      00402276(R)  
char              Stack[0x8]:1   operation               XREF[1]:     004021ae(R)  
undefined8        Stack[-0x10]:8 local_10                XREF[8]:     004021a9(W), 
                                                                      00402206(W), 
                                                                      0040221e(W), 
                                                                      00402233(W), 
                                                                      00402248(W), 
                                                                      0040226b(W), 
                                                                      0040228d(W), 
                                                                      00402292(R)  
                calculate                       XREF[4]:     Entry Point(*), main:00401f28(c), 
                                                             main:00402099(c), 004d96bc(*)  
```
So we see that the three arguments are passed on the stack, while the expected calling convention on x86-64 is passing arguments in registers. We can however tell Ghidra how arguments are passed in this particular case. I am recording how to do this here because I only learned how by doing this challenge:

1. Right click on the function name and select "Edit Function Signature"

![Image showing result of the previous step](./edit-signature-1.png)

2. Edit the signature at the top.

![Image showing result of the previous step](./edit-signature-2.png)

3. Select "Use Custom Storage".

4. Double click on the "Storage" field for a parameter.

![Image showing result of the previous step](./edit-signature-3.png)

5. Remove the previous storage location entry and add a new one, with the location on the stack obtained from the listing view.

![Image showing result of the previous step](./edit-signature-4.png)

![Image showing the end result of the previous five steps](./edit-signature-5.png)


## The flag decryption algorithm, a second look

Back in main, the loop in which the flag is decrypted now looks like this, after renaming some of the variables as well:
```c
num_bitflips = 0;
for (operation_index = 0; operation_index < (int)num_operations;
    operation_index = operation_index + 1) {
  operation_index_ = (long)operation_index;
  *(undefined8 *)(puVar4 + lVar1 + -0x10) = local_22f8[operation_index_ * 3 + 2];
  *(undefined8 *)(puVar4 + lVar1 + -0x18) = local_22f8[operation_index_ * 3 + 1];
  *(undefined8 *)(puVar4 + lVar1 + -0x20) = local_22f8[operation_index_ * 3];
  *(undefined8 *)(puVar4 + lVar1 + -0x28) = 0x40209e;
  operation_result =
       calculate(puVar4[lVar1 + -0x20],*(double *)(puVar4 + lVar1 + -0x18),
                 *(double *)(puVar4 + lVar1 + -0x10));
  *(undefined8 *)(puVar4 + lVar1 + -8) = 0x4020b1;
  gauntlet_result = gauntlet(operation_result);
  if (gauntlet_result != '\0') {
    flag_byte_index = operation_index;
    if (operation_index < 0) {
      flag_byte_index = operation_index + 7;
    }
    flag_byte_index = flag_byte_index >> 3;
    local_2354 = operation_index % 8;
    flag[flag_byte_index] =
         flag[flag_byte_index] ^ (byte)(1 << (7U - (char)(operation_index % 8) & 0x1f));
    num_bitflips = num_bitflips + 1;
  }
}
```
We can conclude that for each of the first `num_operations` bits of the flag, the array `local_22f8` seems to store a binary operation, and if `gauntlet` applied to the result of that operation returns something nonzero, then the corresponding bit of `flag` will get flipped.

If we look for `local_22f8` in the `main` function, we find that its contents are obtained by copying a global:
```c
memcpy(local_22f8,&DAT_004b8240,0x2280);
```

We can copy that global as a Python byte string and then print out the operation, getting something like the following (only the first 8):
```
+3.1423572239e+02 % -3.4380018153e+02 = +3.1423572239e+02
+4.4023960117e+02 - +9.7067350728e+01 = +3.4317225044e+02
-1.0185126481e+02 % +4.3061512281e+02 = -1.0185126481e+02
+2.6566683118e+01 / -1.7908100069e+02 = -1.4835009307e-01
-7.0145728961e+01 % -3.2011401672e+02 = -7.0145728961e+01
+4.6611421244e+02 / -1.7360461195e+02 = -2.6849183741e+00
-2.3380621927e+00 % -4.3522572283e+02 = -2.3380621927e+00
-2.1453669191e+02 - -3.0075547485e+02 = +8.6218782945e+01
```

### `gauntlet`

So now that we know where the argument to `gauntlet` comes from, we need to look at what that function is doing. Ghidra gives us the following short decompile:
```c
undefined4 gauntlet(undefined8 param_1)
{
  char cVar1;
  
  cVar1 = isNegative(param_1);
  if (((cVar1 == '\0') && (cVar1 = isNotNumber(param_1), cVar1 == '\0')) &&
     (cVar1 = isInfinity(param_1), cVar1 == '\0')) {
    return 0;
  }
  return 1;
}
```
From this it seems like `gauntlet` is supposed to return `1` if and only if the argument is negative, not a number, or infinity.

The decompile of the three function is as follows:
```c
bool isNegative(double param_1)
{
  return param_1 < 0.0;
}

undefined8 isNotNumber(void)
{
  return 0;
}

undefined8 isInfinity(void)
{
  return 0;
}
```

Weirdly, while `isNegative` seems to do what it claims, the other two functions just return `0`. Checking the assembly shows that this is not just a mistake in Ghidra's decompile. So that is a bit weird.


## Solution

At this point I was stuck for a while. What are we supposed to do? Are the operations wrong, and we need to find a different batch of operations in the binary that can be used to decrypt the encrypted flag to the correct one? Or perhaps the encrypted flag was wrong? Or should we fix `isNotNumber` and `isInfinity`? Implementing the calculation of the results of the operations in Python and printing them out, the first all seem to have pretty normal results. But then there are a bunch of results where `nan` occurs. Also, a suspicious number of results `-0.0` occur. This points towards us having to fix `isNotNumber` and possibly also `isNegative` -- the latter does not count `-0.0` as negative, but once we already think we need to fix `isNotNumber`, the amount of occurrences of `-0.0` and `0.0` seems unlikely to be a coincidence. That the first couple of results seem normal can be explained: this is because the fake and real flag have the same prefix `uiuctf{`. This turned out to be the correct solution, and yielded the flag. Infinity did not occur as a result, so that function did not need fixing / taking into account.


## Attachments

The solve script implementing the solution can be found in [solve.py](./solve.py) and [data.py](./data.py), and the [challenge binary](./calc) is available as well.
