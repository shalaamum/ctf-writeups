#!/usr/bin/env python3

# Solve script by shalaamum (Kalmarunionen) for the "Fast Calculator" challenge by Minh at UIUCTF 2023

import struct
from pwnlib.util.packing import u64, p64
from data import operation_list_raw
import math

# flag just before num_bit_flipped = 0, after memcpy:
flag_enc_nums = [
0x10eeb90001e1c34b,
0xcb382178a4f04bee,
0xe84683ce6b212aea,
0xa0f5cf092c8ca741,
0x20a92860082772a1,
0xe9a435abb366
]
flag_enc = list(b''.join([p64(x) for x in flag_enc_nums]))
flag = flag_enc.copy()
#print(flag_enc)


def unpack_operation(raw):
    op = raw[:1].decode()
    operand_1 = struct.unpack('d', raw[0x8: 0x10])[0]
    operand_2 = struct.unpack('d', raw[0x10: 0x18])[0]
    return (operand_1, op, operand_2)

def do_operation(op, operand_1, operand_2):
    if op == '%':
        return math.fmod(operand_1, operand_2)
    elif op == '+':
        return operand_1 + operand_2
    elif op == '-':
        return operand_1 - operand_2
    elif op == '*':
        return operand_1 * operand_2
    elif op == '/':
        return operand_1 / operand_2
    elif op == '^':
        return operand_1 ** operand_2

    else:
        raise NotImplementedError(f'Need to implement operation {op}')

operations_raw = [operation_list_raw[i:i+0x18] for i in range(0, len(operation_list_raw), 0x18)]

num_flipped = 0
for op_num, operation in enumerate(operations_raw):
    operand_1, op, operand_2 = unpack_operation(operation)
    result = do_operation(op, operand_1, operand_2)
    do_flip = False
    try:
        result = float(result)
    except Exception as e:
        print(e)
        result = float("NaN")

    if result < 0.0:
        do_flip = True
    if math.isnan(result): 
        do_flip = True
    if math.copysign(1, result) == -1.0:
        do_flip = True
    if do_flip:
        index_in_flag = op_num >> 3
        bit_num = op_num % 8
        flag[index_in_flag] ^= 1 << (7 - bit_num)
        num_flipped += 1

    print(f'{operand_1:+.10e} {op} {operand_2:+.10e} = {result:+.10e}  {do_flip}')
print(bytes(flag_enc))
print(f'{num_flipped=}   num_ops = {len(operations_raw)}')
print(bytes(flag))
# uiuctf{n0t_So_f45t_w1th_0bscur3_b1ts_of_MaThs}
