## Summary

This is a writeup for the challenge "vmwhere2" by richard at [UIUCTF 2023](https://2023.uiuc.tf/). The challenge involved a program for a small stack-based custom VM that needed to be reversed. This is the second part of a two-part-challenge, so this writeup assumes you read [my writeup for the first part](https://gitlab.com/shalaamum/ctf-writeups/-/blob/master/UIUCTF%202023/vmwhere1/writeup.md). 66 teams solved this challenge.

Solution by: shalaamum

Writeup by: shalaamum


## New instructions

The binary used for this challenge added two instructions `0x11` and `0x12` to the VM. Furthermore, instruction `0x10` is actually used this time (it wasn't in part 1), so we need to understand what those do.
```c
case 0x10:
  current_intruction = current_intruction + 2;
  offset = *next_instruction;
  if ((long)stack_pointer - (long)stack_buffer < (long)(ulong)offset) {
    printf("Stack underflow in reverse at 0x%04lx\n",(long)current_intruction - (long)param_1);
  }
  for (k = 0; (int)k < (int)(uint)(offset >> 1); k = k + 1) {
    tmp = stack_pointer[(int)(k - offset)];
    stack_pointer[(int)(k - offset)] = stack_pointer[(int)~k];
    stack_pointer[(int)~k] = tmp;
  }
  break;
case 0x11:
  value = stack_pointer[-1];
  for (i = 0; i < 8; i = i + 1) {
    (stack_pointer + -1)[i] = value & 1;
    value = value >> 1;
  }
  stack_pointer = stack_pointer + 7;
  current_intruction = next_instruction;
  break;
case 0x12:
  v = 0;
  for (j = 7; -1 < j; j = j + -1) {
    v = v << 1 | (stack_pointer + -8)[j] & 1;
  }
  stack_pointer[-8] = v;
  stack_pointer = stack_pointer + -7;
  current_intruction = next_instruction;
  break;
```
What `0x10` is doing is reversing the order of some bytes at the top of the stack. `0x11` pops a value from the stack, and pushes the individual bits, whereas `0x12` is the inverse of that operation, reassembling a byte from bits.

Adding these new instructions to the solve script from `vmwhere1`, we can disassemble the program. Attached you can find the [full disassemble](./disassembled.txt).


## Overview of the program

Looking at the bottom of the disassembly like last time, we find that the failure branch starts at `0x0bbc` and the success branch starts at `0x0ba0`. However this time both addresses are used in jumps only a single time, right before those two snippets:
```
0x0b9a: JMPZ 0x0ba0
0x0b9d: JMP 0x0bbc
```
So when execution arrives near the end of the program, we get the success branch if and only if the top of the stack is zero.

Looking through the rest of the program, it is obvious that there are three different parts:

1. Initially there is the printout that welcomes the user and asks for the password, ending at `0x0071`.

2. Then there is a `PUSH 0x00` (at `0x0074`), followed by very repetitive code blocks from `0x0076` to `0x0971`. The first block looks as follows, with the other blocks identical (with jumps targets adjusted):
```
0x0074: PUSH 0x00 
0x0076: READ
0x0077: BITEXTRACT 
0x0078: PUSH 0xff
0x007a: REVERSE 0x09
0x007c: REVERSE 0x08
0x007e: PUSH 0x00
0x0080: REVERSE 0x02
0x0082: DUP
0x0083: PUSH 0xff 
0x0085: XOR
0x0086: JMPZ 0x008d
0x0089: POP
0x008a: JMP 0x0091
0x008d: POP
0x008e: JMP 0x00a7
0x0091: REVERSE 0x02
0x0093: REVERSE 0x02
0x0095: JMPZ 0x009f
0x0098: POP
0x0099: PUSH 0x01 
0x009b: ADD
0x009c: JMP 0x00a0
0x009f: POP
0x00a0: DUP
0x00a1: DUP
0x00a2: ADD
0x00a3: ADD
0x00a4: JMP 0x0080
0x00a7: POP
```

3. From `0x0972` to `0x0b98` there are repetitive blocks that all look similar to the following first block from `0x0972` to `0x097c`:
```
0x0972: PUSH 0xc6
0x0974: XOR
0x0975: REVERSE 0x2e 
0x0977: REVERSE 0x2f
0x0979: OR
0x097a: REVERSE 0x2e
0x097c: REVERSE 0x2d
```

The first type of blocks seem to be where the individual characters of the password that the user inputs are read in, and then some processing happens that we will have to analyze closer. The second type of block does not have a loop, so we can easily count how the stack gets changed: Each of these blocks reduces the size of the stack by `1`. So we can guess that this consumes the converted password characters byte by byte, and will somehow fold it to a value that will have to be zero at the very end for success.


## The character-reading conversion

Let us start with the first type of repetitive block, where the password gets read in. Here is a commented version of the disassembly where I added how the top of the stack looks like:
```
0x0076: READ           # input
0x0077: BITEXTRACT     # input_bits_low_to_high
0x0078: PUSH 0xff      # input_bits_low_to_high 0xff
0x007a: REVERSE 0x09   # 0xff input_bits_high_to_low
0x007c: REVERSE 0x08   # 0xff input_bits_low_to_high
0x007e: PUSH 0x00      # 0xff input_bits_l_to_h acc
# the top of the stack is 0x00 currently, but further below we see this is an an accumulator

0x0080: REVERSE 0x02   # 0xff input_bits_l_to_h_except_last acc highest_bit
# next four: checks whether we reached the "guard" 0xff value, and if so break
0x0082: DUP
0x0083: PUSH 0xff 
0x0085: XOR             # 0xff input_bits_l_to_h_except_last acc highest_bit (highest_bit ^ 0xff)
0x0086: JMPZ 0x008d
# so if the "highest_bit" was 0xff (so in fact we reached the 0xff guard value after consuming all bits),
# we now jump out of the loop. the stack must have been:
# acc 0xff (highest_bit ^ 0xff)
# otherwise:
0x0089: POP             # 0xff input_bits_l_to_h_except_last acc highest_bit
0x008a: JMP 0x0091

0x008d: POP             # acc 0xff
0x008e: JMP 0x00a7

0x0091: REVERSE 0x02
0x0093: REVERSE 0x02    # 0xff remaining_bits acc current_bit
0x0095: JMPZ 0x009f     # if current_bit != 0:
  0x0098: POP           # 0xff remaining_bits acc
  0x0099: PUSH 0x01     # 0xff remaining_bits acc 1
  0x009b: ADD           # 0xff remaining_bits acc+1
  0x009c: JMP 0x00a0
                        # else:
  0x009f: POP           # 0xff remaining_bits acc
# so the above increments acc iff current_bit != 0
0x00a0: DUP             # 0xff remaining_bits acc acc
0x00a1: DUP             # 0xff remaining_bits acc acc acc
0x00a2: ADD             # 0xff remaining_bits acc 2*acc
0x00a3: ADD             # 0xff remaining_bits 3*acc
0x00a4: JMP 0x0080
0x00a7: POP             # acc
```
From this we can see that every input byte `b` gets converted in the following way:
If we write `b` in binary, as `b = sum_i b_i * 2^i`, then this byte gets converted to `(sum_i b_i * 3^(i+1)) mod 256`.

Let us call this function `f`. After all input characters have been read, the stack will then look like
```
0 f(input_0) f(input_1) ... f(input_45)
```


## The password-checking blocks

Here is the first block in commented form:
```
# 0 encoded_input[0:n]
0x0972: PUSH 0xc6     # 0 encoded_input[0:n] c6
0x0974: XOR           # 0 encoded_input[:n-1] encoded_input[-1]^0xc6 
0x0975: REVERSE 0x2e  
0x0977: REVERSE 0x2f  # encoded_input[:n-1] encoded_input[-1]^0xc6 0
0x0979: OR            # encoded_input[:n-1] (encoded_input[-1]^0xc6 | 0)
0x097a: REVERSE 0x2e  
0x097c: REVERSE 0x2d  # (encoded_input[-1]^0xc6 | 0) encoded_input[:n-1]
```
We already know that what remains on the stack at the very end has to be `0` for success. This implies that the last encoded input byte needs to be `0xc6`. The further blocks then check the encoded input bytes against constants in the same manner.


## Solution

We can extract the constants the encoded input is checked against from the `PUSH` instructions occurring between `0x0972` and `0x0b98`. What is left then is only to invert the function `f`. Unfortunately, `f` is not a bijection, so for some characters there is more than one possibility. I dealt with this by just printing out all possible passwords consisting only of printable ASCII characters, and then manually deciding which of multiple printable characters in some position made sense (in order to form an English word, etc.).


## Attachments

[Challenge binary](./chal2) and [program](./program). The solution strategy described above is implemented in the [solve script](./solve.py), which also produces [the disassembly](./disassembled.txt).
