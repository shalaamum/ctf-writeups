#!/usr/bin/env python3

# Solve script by shalaamum (Kalmarunionen) for the "vmwhere2" challenge by richard at UIUCTF 2023

import sys
import string

with open('./program', 'rb') as fh:
    program = fh.read()

def emulate(program, debug=False, inputs=None, silent=False, skipto=0):
    rip = 0
    stack = []
    rips_seen = set()
    stepping_active = False
    while True:
        if rip == skipto:
            stepping_active = True
        if debug:
            print(f'RIP = {rip:04x}')
            print(f'Stack: ' + " ".join(f'{x:02x}' for x in stack))
            if stepping_active:
                input()
            else:
                print()
        stack = [s & 0xff for s in stack]
        ci = program[rip]
        rips_seen.add(rip)
        rip += 1
        if ci == 0:
            break
        elif ci == 1:
            stack.append(stack.pop() + stack.pop())
        elif ci == 2:
            stack.append(-stack.pop() + stack.pop())
        elif ci == 3:
            stack.append(stack.pop() & stack.pop())
        elif ci == 4:
            stack.append(stack.pop() | stack.pop())
        elif ci == 5:
            stack.append(stack.pop() ^ stack.pop())
        elif ci == 6:
            fst = stack.pop()
            snd = stack.pop()
            stack.append(snd << (fst & 0x1f))
        elif ci == 7:
            fst = stack.pop()
            snd = stack.pop()
            stack.append(snd >> (fst & 0x1f))
        elif ci == 8:
            if inputs is None:
                c = ord(sys.stdin.read(1))
            else:
                c = inputs[0]
                inputs = inputs[1:]
            stack.append(c)
        elif ci == 9:
            c = stack.pop()
            if not silent:
                print(chr(c), end='', flush=True)
                if debug:
                    print()
        elif ci == 10:
            stack.append(program[rip])
            rip += 1
        elif ci == 11:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            if stack[-1] & 0x80:
                rip += offset
                if debug:
                    print(f'JMPL {rip+2}')
            rip += 2
        elif ci == 12:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            if stack[-1] == 0:
                rip += offset
                if debug:
                    print(f'JMPZ {rip+2}')
            rip += 2
        elif ci == 13:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            #if debug:
            #    print(f'{program[rip]=}')
            #    print(f'{program[rip+1]=}')
            #    print(f'{offset=}')
            rip += offset
            if debug:
                print(f'JMP {rip+2}')
            rip += 2
        elif ci == 14:
            stack.pop()
        elif ci == 15:
            stack.append(stack[-1])
        elif ci == 0x10:
            l = program[rip]
            stack = stack[:-l] + stack[-l:][::-1]
            rip += 1
        elif ci == 0x11:
            v = stack.pop()
            for i in range(8):
                stack.append(v & 1)
                v >>= 1
        elif ci == 0x28:
            raise Exception
        else:
            raise Exception(f'Unknown opcode {ci}')
        if len(stack) > 0x1000:
            raise Exception("Stack overflow")
    return rips_seen

def disassemble(program):
    rip = 0
    instruction_offsets = []
    jumps = {}
    while rip < len(program):
        ci = program[rip]
        print(f'0x{rip:04x}: ', end="")
        instruction_offsets.append(rip)
        rip += 1
        if ci == 0:
            print(f'HLT')
        elif ci == 1:
            print(f'ADD')
        elif ci == 2:
            print('SUB')
        elif ci == 3:
            print('AND')
        elif ci == 4:
            print('OR')
        elif ci == 5:
            print('XOR')
        elif ci == 6:
            print('SHL')
        elif ci == 7:
            print('SHR')
        elif ci == 8:
            print('READ')
        elif ci == 9:
            print('WRITE')
        elif ci == 10:
            print(f'''PUSH 0x{program[rip]:02x} {"('" + chr(program[rip]) + "')"  if chr(program[rip]) in string.printable else ""}''')
            rip += 1
        elif ci == 11:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            print(f'JMPL 0x{rip + offset + 2:04x}')
            jumps[rip - 1] = rip + offset + 2
            rip += 2
        elif ci == 12:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            print(f'JMPZ 0x{rip + offset + 2:04x}')
            jumps[rip - 1] = rip + offset + 2
            rip += 2
        elif ci == 13:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            print(f'JMP 0x{rip + offset + 2:04x}')
            jumps[rip - 1] = rip + offset + 2
            rip += 2
        elif ci == 14:
            print('POP')
        elif ci == 15:
            print('DUP')
        elif ci == 0x10:
            v = program[rip]
            print(f'REVERSE 0x{v:02x}')
            rip += 1
        elif ci == 0x11:
            print('BITEXTRACT')
            # Replaces on the stack X by
            # (X & 1) ((X >> 1)& 1)  ...
            # top of stack is highest bit
        elif ci == 0x12:
            print('BITASSEMBLE')
            # inverse to BITEXTRACT
            quit()
        elif ci == 0x28:
            print('weird op 0x28')
            quit()
        else:
            raise Exception(f'Unknown opcode: {ci:02x}')
    return instruction_offsets, jumps

def extract_ciphertext(program):
    rip = 0
    ciphertext = []
    while rip < len(program):
        ci = program[rip]
        #print(f'0x{rip:04x}: ', end="")
        rip += 1
        if ci == 0:
            pass
            #print(f'HLT')
        elif ci == 1:
            pass
            #print(f'ADD')
        elif ci == 2:
            pass
            #print('SUB')
        elif ci == 3:
            pass
            #print('AND')
        elif ci == 4:
            pass
            #print('OR')
        elif ci == 5:
            pass
            #print('XOR')
        elif ci == 6:
            pass
            #print('SHL')
        elif ci == 7:
            pass
            #print('SHR')
        elif ci == 8:
            pass
            #print('READ')
        elif ci == 9:
            pass
            #print('WRITE')
        elif ci == 10:
            #print(f'''PUSH 0x{program[rip]:02x} {"('" + chr(program[rip]) + "')"  if chr(program[rip]) in string.printable else ""}''')
            if 0x971 < rip < 0xb96:
                ciphertext.append(program[rip])
            rip += 1
        elif ci == 11:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            #print(f'JMPL 0x{rip + offset + 2:04x}')
            rip += 2
        elif ci == 12:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            #print(f'JMPZ 0x{rip + offset + 2:04x}')
            rip += 2
        elif ci == 13:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            #print(f'JMP 0x{rip + offset + 2:04x}')
            rip += 2
        elif ci == 14:
            pass
            #print('POP')
        elif ci == 15:
            pass
            #print('DUP')
        elif ci == 0x10:
            v = program[rip]
            #print(f'REVERSE 0x{v:02x}')
            rip += 1
        elif ci == 0x11:
            pass
            #print('BITEXTRACT')
            # Replaces on the stack X by
            # (X & 1) ((X >> 1)& 1)  ...
            # top of stack is highest bit
        elif ci == 0x12:
            #print('BITASSEMBLE')
            # inverse to BITEXTRACT
            quit()
        elif ci == 0x28:
            #print('weird op 0x28')
            quit()
        else:
            raise Exception(f'Unknown opcode: {ci:02x}')
    return  ciphertext

disassemble(program)

S = [list() for _ in range(256)]
Sinv = [list() for _ in range(256)]
for x in [ord(x) for x in string.printable]:
    v = 0
    xorig = x
    for i in range(8):
        if x & 1:
            v += 3**(i+1)
        x >>= 1
    v = v % 256
    #print(f'{xorig} --> {v}')
    #if Sinv[v] is not None:
    #    print(f'Duplicate! Sinv[{v}] = {Sinv[v]} =? {xorig}')
    #    quit()
    S[xorig].append(v)
    Sinv[v].append(xorig)

#print(S[0xb5])
print(Sinv)

ciphertext = extract_ciphertext(program)
print([hex(x) for x in ciphertext])
flags = [""]
for c in ciphertext:
    flags_orig = flags
    flags = []
    for flag in flags_orig:
        for v in Sinv[c]:
            flags.append(flag + chr(v))
flags = [flag[::-1] for flag in flags]
prefix = "uiuctf{b4s3_3_1s_b4s3d_just_l1k3_vm_r3v3rs1ng}"
for flag in flags:
    if flag[:len(prefix)] == prefix:
        print(flag)

# uiuctf{b4s3_3_Xs_b4sZd_just_lXkZ_vm_rZvZrsX @}
