## Summary

This is a writeup for the challenge "geoguesser" by richard at [UIUCTF 2023](https://2023.uiuc.tf/). The challenge involved a Janet image file, but instead of reversing it, I patched the interpreter instead. There were 38 teams that solved this challenge.

Solution by: shalaamum

Writeup by: shalaamum


## First look at the problem

We are given two files: [janet](./janet), an executable, and [program.jimage](./program.jimage). The challenge description told us that intended usage was `janet -i program.jimage`. Running `./janet` without the arguments first, we get some kind of repl, suggesting Janet may be some kind of scriping language, which a quick internet search [confirms](https://janet-lang.org/). It also shows us the version:
```
% ./janet
Janet 1.28.0-358f5a0 linux/x64/gcc - '(doc)' for help
repl:1:>
```
The version string helpfully includes what seems to be a short git commit hash, so that we should be able to obtain the exact code used to produce the executable from the [public Github repo](https://github.com/janet-lang/janet) -- this will be relevant later.

The documentation reveals that the `jimage` file we need to run is some kind of im "image file" that seems to contain some kind of compiled Janet program. Running it we get the following:
```
% ./janet -i ./program.jimage
Welcome to geoguesser!
Where am I? I don't know?
Not a valid coordinate. Try again.
Where am I? 0,0
Nope. You have 4 guesses left.
Where am I? 0,0
Nope. You have 3 guesses left.
Where am I? 1,2
Nope. You have 2 guesses left.
Where am I? 3,4
Nope. You have 1 guesses left.
Where am I? 1.23123,9
You lose!
The answer was: <tuple 0x5E683EA51BD0>
```
Apparantly we are to guess a coordinate in 5 guesses. At the end the program claims to give us what the answer was supposed to be, but seems to use a print function that prints type and address for the tuple, rather than pretty-printing its content.


## Can I disassemble Janet image files?

At first I looked for ways to somehow disassemble or decompile the `jimage` file. While I did find some references to [for example ppasm](https://janet.guide/testing-and-debugging/) online that indicated that Janet had some kind of disassembly builtin, the examples I saw required the program itself to somehow enable it with `(debug)`. But I only knew how to run `program.jimage`, not how to modify it or how to load it from the REPL. This is actually possible, and other teams solved the challenge by importing the program in the REPL with `(import ./program)`. As I did not quickly find this possibility, I went for a different path instead: Patching the interpreter itself.


## Prettier printing for Janet

I pulled the Janet interpreter from [the Github repo](https://github.com/janet-lang/janet) and checked out commit `358f5a03`, the exact one used to build the binary we were provided with. Building with `make` resulted in a binary `build/janet` that seemed to run `program.jimage` with no observable differences to the provided version. I then used GPT4 to generate a patch that would make Janet pretty print a tuple instead of just showing type and address[^1]:
```diff
diff --git a/src/core/pp.c b/src/core/pp.c
index d058cb1e..f58c1513 100644
--- a/src/core/pp.c
+++ b/src/core/pp.c
@@ -255,7 +255,11 @@ void janet_to_string_b(JanetBuffer *buffer, Janet x) {
         }
     fallthrough:
         default:
-            string_description_b(buffer, janet_type_names[janet_type(x)], janet_unwrap_pointer(x));
+            if (janet_type(x) == JANET_TUPLE) {
+                janet_pretty(buffer, 10, 0, x);  // pretty prints tuple, 10 is max depth
+            } else {
+                string_description_b(buffer, janet_type_names[janet_type(x)], janet_unwrap_pointer(x));
+            }
             break;
     }
 }
```

With this patch, we now see what the correct answer was after guessing incorrectly five times:
```
% ./janet/build/janet -i ./program.jimage
Welcome to geoguesser!
Where am I? 0,0
Nope. You have 4 guesses left.
Where am I? 0,0
Nope. You have 3 guesses left.
Where am I? 0,0
Nope. You have 2 guesses left.
Where am I? 0,0
Nope. You have 1 guesses left.
Where am I? 0,0
You lose!
The answer was: (33.8077 -101.218)
```


## Solution

Running this a second time quashed the hope that the answer may be a constant. My next hope was that the the answer would be time-based, and starting two copies at the same time confirmed this. Testing with `faketime` confirmed that only second-resolution was required. The suggested solution was thus to send five guesses to the remote that correspond to the answers for five seconds around the one we opened the connection at (using the exact one might not work, as the clocks may not be completely in sync, and there is some latency in starting the program on the remote). This succeeded in recovering the flag.


## Attachments

[Provided Janet interpreter](./janet), [the program](./program.jimage), the [patch for Janet](./janet.patch) and [solve script](./solve.py).


[^1]: While I had never heard about Janet before, GPT4 seemed to have sufficient knowledge of the Janet source code to suggest looking for a function called `janet_to_string_b` after the first suggestion did not pan out.
