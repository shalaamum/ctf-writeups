#!/usr/bin/env python3

# Solve script by shalaamum (Kalmarunionen) for the "geoguesser" challenge by richard at UIUCTF 2023


from datetime import datetime
import time
import pwnlib.tubes.process
import pwnlib.tubes.remote
from pwn import context

def get_answer_for_time(timestamp):
    dt_object = datetime.fromtimestamp(timestamp)
    iso_format = dt_object.strftime("%Y-%m-%dT%H:%M:%S")
    cmd = ['faketime', iso_format]
    cmd += ['/home/user/various/janet/build/janet', '-i', '/home/user/CTFs/2023/Q3/uiuc-ctf-2023/geoguesser/program.jimage']
    io = pwnlib.tubes.process.process(cmd)
    for _ in range(5):
        io.sendline(b'0,0')
    io.recvuntil(b'was: (')
    answer = io.recvline().strip().decode()[:-1]
    io.close()
    answer = answer.replace(' ', ',')
    return answer

timestamp = int(time.time())
answers = [get_answer_for_time(timestamp + offset) for offset in range(5)]

with context.local(log_level="debug"):
    io = pwnlib.tubes.remote.remote("geoguesser.chal.uiuc.tf", 1337, level="debug")
    for answer in answers:
        io.recvuntil(b'Where am I? ')
        io.sendline(answer.encode())
    io.interactive()

# uiuctf{i_se3_y0uv3_f0und_7h3_t1m3_t0_r3v_th15_b333b674c1365966}
