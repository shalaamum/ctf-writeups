#!/usr/bin/env python3

# Solve script by shalaamum (Kalmarunionen) for the "vmwhere1" challenge by richard at UIUCTF 2023

import sys
import string

with open('./program', 'rb') as fh:
    program = fh.read()

def emulate(program, debug=False, inputs=None, silent=False):
    rip = 0
    stack = []
    rips_seen = set()
    while True:
        if debug:
            print(f'RIP = {rip}')
        stack = [s & 0xff for s in stack]
        ci = program[rip]
        rips_seen.add(rip)
        rip += 1
        if ci == 0:
            break
        elif ci == 1:
            stack.append(stack.pop() + stack.pop())
        elif ci == 2:
            stack.append(-stack.pop() + stack.pop())
        elif ci == 3:
            stack.append(stack.pop() & stack.pop())
        elif ci == 4:
            stack.append(stack.pop() | stack.pop())
        elif ci == 5:
            stack.append(stack.pop() ^ stack.pop())
        elif ci == 6:
            fst = stack.pop()
            snd = stack.pop()
            stack.append(snd << (fst & 0x1f))
        elif ci == 7:
            fst = stack.pop()
            snd = stack.pop()
            stack.append(snd >> (fst & 0x1f))
        elif ci == 8:
            if inputs is None:
                c = ord(sys.stdin.read(1))
            else:
                c = inputs[0]
                inputs = inputs[1:]
            stack.append(c)
        elif ci == 9:
            c = stack.pop()
            if not silent:
                print(chr(c), end='', flush=True)
        elif ci == 10:
            stack.append(program[rip])
            rip += 1
        elif ci == 11:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            if stack[-1] & 0x80:
                rip += offset
                if debug:
                    print(f'JMPL {rip+2}')
            rip += 2
        elif ci == 12:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            if stack[-1] == 0:
                rip += offset
                if debug:
                    print(f'JMPZ {rip+2}')
            rip += 2
        elif ci == 13:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            if debug:
                print(f'{program[rip]=}')
                print(f'{program[rip+1]=}')
                print(f'{offset=}')
            rip += offset
            if debug:
                print(f'JMP {rip+2}')
            rip += 2
        elif ci == 14:
            stack.pop()
        elif ci == 15:
            stack.append(stack[-1])
        elif ci == 16:
            print('weird one')
            raise Exception
        elif ci == 0x28:
            print('second weird one')
            raise Exception
        else:
            raise Exception('Unknown opcode')
        if len(stack) > 0x1000:
            raise Exception("Stack overflow")
    return rips_seen

def disassemble(program):
    rip = 0
    instruction_offsets = []
    jumps = {}
    while rip < len(program):
        ci = program[rip]
        print(f'0x{rip:04x}: ', end="")
        instruction_offsets.append(rip)
        rip += 1
        if ci == 0:
            print(f'HLT')
        elif ci == 1:
            print(f'ADD')
        elif ci == 2:
            print('SUB')
        elif ci == 3:
            print('AND')
        elif ci == 4:
            print('OR')
        elif ci == 5:
            print('XOR')
        elif ci == 6:
            print('SHL')
        elif ci == 7:
            print('SHR')
        elif ci == 8:
            print('READ')
        elif ci == 9:
            print('WRITE')
        elif ci == 10:
            print(f'''PUSH 0x{program[rip]:02x} {"('" + chr(program[rip]) + "')"  if chr(program[rip]) in string.printable else ""}''')
            rip += 1
        elif ci == 11:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            print(f'JMPL 0x{rip + offset + 2:04x}')
            jumps[rip - 1] = rip + offset + 2
            rip += 2
        elif ci == 12:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            print(f'JMPZ 0x{rip + offset + 2:04x}')
            jumps[rip - 1] = rip + offset + 2
            rip += 2
        elif ci == 13:
            offset = program[rip+1] + program[rip]*0x100
            if offset & 0x8000:
                offset = offset - 0x10000
            print(f'JMP 0x{rip + offset + 2:04x}')
            jumps[rip - 1] = rip + offset + 2
            rip += 2
        elif ci == 14:
            print('POP')
        elif ci == 15:
            print('DUP')
        elif ci == 16:
            print('weird one')
        elif ci == 0x28:
            print('second weird one')
        else:
            raise Exception('Unknown opcode')
    return instruction_offsets, jumps



instruction_offsets, jumps = disassemble(program)

#target = 0x48f
#print(jumps)

inputs = [0]*100
for i in range(100):
    best_c = 0
    max_rips = 0
    for c in range(256):
        inputs[i] = c
        num_rips = len(emulate(program, debug=False, inputs=inputs, silent=True))
        if num_rips > max_rips:
            best_c = c
            max_rips = num_rips
    inputs[i] = best_c
    flag = "".join([chr(x) for x in inputs if x != 0])
    print(flag)
    if best_c == 0:
        break

# uiuctf{ar3_y0u_4_r3al_vm_wh3r3_(gpt_g3n3r4t3d_th1s_f14g)
