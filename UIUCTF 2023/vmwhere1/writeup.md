## Summary

This is a writeup for the challenge "vmwhere1" by richard at [UIUCTF 2023](https://2023.uiuc.tf/). The challenge involved a program for a small stack-based custom VM and could be solved by bruteforcing the flag byte-by-byte, by watching the number of operations verification took. 124 teams solved this challenge.

Solution by: shalaamum

Writeup by: shalaamum


## First look

We are given two files, an executable [chal](./chal) and a file [program](./program). The description tells us to run `./chal program`. Doing that we see the following:
```
% ./chal program
Welcome to VMWhere 1!
Please enter the password:
testpassword
Incorrect password!
```
So this seems like a typical VM reversing challenge where we need to figure out what password will pass the checks, with the password probably being the flag.


## Reversing the binary

The binary `chal` is stripped, but it is pretty easy to understand in Ghidra. The main function reads in the program from the file specified in the argument, and then a function `run_program` is called. That function's decompilation is reproduced below, after renaming and retyping some variables.
```c
undefined8 run_program(byte *program,int program_length)

{
  byte bVar1;
  byte bVar2;
  int iVar3;
  byte *stack_buffer;
  uint local_24;
  byte *current_instruction;
  byte *stack_pointer;
  byte *next_instruction;
  
  stack_buffer = (byte *)malloc(0x1000);
  current_instruction = program;
  stack_pointer = stack_buffer;
  while( true ) {
    if ((current_instruction < program) || (program + program_length <= current_instruction)) {
      printf("Program terminated unexpectedly. Last instruction: 0x%04lx\n",
             (long)current_instruction - (long)program);
      return 1;
    }
    next_instruction = current_instruction + 1;
    if (false) break;
    switch(*current_instruction) {
    case 0:
      return 0;
    case 1:
      stack_pointer[-2] = stack_pointer[-2] + stack_pointer[-1];
      stack_pointer = stack_pointer + -1;
      current_instruction = next_instruction;
      break;
    case 2:
      stack_pointer[-2] = stack_pointer[-2] - stack_pointer[-1];
      stack_pointer = stack_pointer + -1;
      current_instruction = next_instruction;
      break;
    case 3:
      stack_pointer[-2] = stack_pointer[-2] & stack_pointer[-1];
      stack_pointer = stack_pointer + -1;
      current_instruction = next_instruction;
      break;
    case 4:
      stack_pointer[-2] = stack_pointer[-2] | stack_pointer[-1];
      stack_pointer = stack_pointer + -1;
      current_instruction = next_instruction;
      break;
    case 5:
      stack_pointer[-2] = stack_pointer[-2] ^ stack_pointer[-1];
      stack_pointer = stack_pointer + -1;
      current_instruction = next_instruction;
      break;
    case 6:
      stack_pointer[-2] = stack_pointer[-2] << (stack_pointer[-1] & 0x1f);
      stack_pointer = stack_pointer + -1;
      current_instruction = next_instruction;
      break;
    case 7:
      stack_pointer[-2] = (byte)((int)(uint)stack_pointer[-2] >> (stack_pointer[-1] & 0x1f));
      stack_pointer = stack_pointer + -1;
      current_instruction = next_instruction;
      break;
    case 8:
      iVar3 = getchar();
      *stack_pointer = (byte)iVar3;
      stack_pointer = stack_pointer + 1;
      current_instruction = next_instruction;
      break;
    case 9:
      stack_pointer = stack_pointer + -1;
      putchar((uint)*stack_pointer);
      current_instruction = next_instruction;
      break;
    case 10:
      *stack_pointer = *next_instruction;
      stack_pointer = stack_pointer + 1;
      current_instruction = current_instruction + 2;
      break;
    case 0xb:
      if ((char)stack_pointer[-1] < '\0') {
        next_instruction = next_instruction + CONCAT11(*next_instruction,current_instruction[2]);
      }
      current_instruction = next_instruction;
      current_instruction = current_instruction + 2;
      break;
    case 0xc:
      if (stack_pointer[-1] == 0) {
        next_instruction = next_instruction + CONCAT11(*next_instruction,current_instruction[2]);
      }
      current_instruction = next_instruction;
      current_instruction = current_instruction + 2;
      break;
    case 0xd:
      current_instruction =
           next_instruction + (long)CONCAT11(*next_instruction,current_instruction[2]) + 2;
      break;
    case 0xe:
      stack_pointer = stack_pointer + -1;
      current_instruction = next_instruction;
      break;
    case 0xf:
      *stack_pointer = stack_pointer[-1];
      stack_pointer = stack_pointer + 1;
      current_instruction = next_instruction;
      break;
    case 0x10:
      current_instruction = current_instruction + 2;
      bVar1 = *next_instruction;
      if ((long)stack_pointer - (long)stack_buffer < (long)(ulong)bVar1) {
        printf("Stack underflow in reverse at 0x%04lx\n",(long)current_instruction - (long)program);
      }
      for (local_24 = 0; (int)local_24 < (int)(uint)(bVar1 >> 1); local_24 = local_24 + 1) {
        bVar2 = stack_pointer[(int)(local_24 - bVar1)];
        stack_pointer[(int)(local_24 - bVar1)] = stack_pointer[(int)~local_24];
        stack_pointer[(int)~local_24] = bVar2;
      }
      break;
    default:
      goto switchD_001014bd_caseD_11;
    case 0x28:
      FUN_00101370(program,stack_buffer,stack_pointer,(long)next_instruction - (long)program);
      current_instruction = next_instruction;
    }
    if (stack_pointer < stack_buffer) {
      printf("Stack underflow at 0x%04lx\n",(long)current_instruction - (long)program);
      return 1;
    }
    if (stack_buffer + 0x1000 < stack_pointer) {
      printf("Stack overflow at 0x%04lx\n",(long)current_instruction - (long)program);
      return 1;
    }
  }
switchD_001014bd_caseD_11:
  printf("Unknown opcode: 0x%02x at 0x%04lx\n",(ulong)*current_instruction,
         (long)current_instruction - (long)program);
  return 1;
}
```
Identifying the different parts here was straightforward. There is a variable that needs to point to somewhere in the `program` buffer and usually gets incremented by one in each iteration of the loop, so that must be the instruction pointer. A lot of operations take two bytes directly below another pointer, and then write the result of a binary operation in place of the first of these two operands, and decrement the pointer, so that must be a stack pointer. It is thus clear we have a stack-based VM here, with a pretty standard set of instructions.


## Reversing the program

We can reimplement an emulator for this VM in Python, but also disassemble the program into something more readable. The full disassembly is [available here](./disassembled.txt). Let us start by looking at the very end, to find out where the winning branch is.
```
0x0494: PUSH 0x00 
0x0496: PUSH 0x0a
0x0498: PUSH 0x21 ('!')
0x049a: PUSH 0x64 ('d')
0x049c: PUSH 0x72 ('r')
0x049e: PUSH 0x6f ('o')
0x04a0: PUSH 0x77 ('w')
0x04a2: PUSH 0x73 ('s')
0x04a4: PUSH 0x73 ('s')
0x04a6: PUSH 0x61 ('a')
0x04a8: PUSH 0x70 ('p')
0x04aa: PUSH 0x20 (' ')
0x04ac: PUSH 0x74 ('t')
0x04ae: PUSH 0x63 ('c')
0x04b0: PUSH 0x65 ('e')
0x04b2: PUSH 0x72 ('r')
0x04b4: PUSH 0x72 ('r')
0x04b6: PUSH 0x6f ('o')
0x04b8: PUSH 0x63 ('c')
0x04ba: PUSH 0x6e ('n')
0x04bc: PUSH 0x49 ('I')
0x04be: JMPZ 0x04c5
0x04c1: WRITE
0x04c2: JMP 0x04be
0x04c5: HLT
```
This snippet pushes the string "Incorrect password!\n" (null-terminated) to the stack (in reversed order), and then prints it using a loop (WRITE pops the byte at the top of the stack and prints it to stdout, the loop then prints character-by-character until the null-byte is hit). The immediately preceding snippet is the following:
```
0x0478: PUSH 0x00 
0x047a: PUSH 0x0a ('
')
0x047c: PUSH 0x21 ('!')
0x047e: PUSH 0x74 ('t')
0x0480: PUSH 0x63 ('c')
0x0482: PUSH 0x65 ('e')
0x0484: PUSH 0x72 ('r')
0x0486: PUSH 0x72 ('r')
0x0488: PUSH 0x6f ('o')
0x048a: PUSH 0x43 ('C')
0x048c: JMPZ 0x0493
0x048f: WRITE
0x0490: JMP 0x048c
0x0493: HLT
```
This prints `Correct!`, so this is what we want to end up. So jumps to `0x0494` would be the failure-branch and jumps to `0x0478` would be the success-branch. Searching for those we see that the success branch actually starts with a `POP` right before, so the success branch starts at `0x0477`, and we jump there only at the very end, while `0x0494` occurs 58 times in the disassembly.


## Solution

This above suggests the program may check the input byte-by-byte, and jumps to the failure branch immediately if one character is wrong. Under this hypothesis, we can now easily produce a solution by bruteforcing the password byte-by-byte, detecting the correct one by the number of operations the program used in total. This strategy turned out to work!


## Attachments

[Challenge binary](./chal) and [program](./program), [solve script](./solve.py) and [generated disassembly](./disassembled.txt).

